FROM node:18-alpine AS modules
ENV NODE_ENV=development
WORKDIR /usr/src/app
COPY --chown=node:node package*.json .
RUN npm i

FROM node:18-alpine AS production-modules
ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY --chown=node:node package*.json .
RUN npm ci --only=production

FROM node:18-alpine AS development
ENV NODE_ENV=development
WORKDIR /usr/src/app
COPY --chown=node:node --from=modules /usr/src/app/node_modules ./node_modules
COPY --chown=node:node . .
#RUN apk add --update curl && \
#    rm -rf /var/cache/apk/* && \
#    apk add --no-cache bash
#RUN curl -fsSL https://bun.sh/install | bash
#RUN bun install
#RUN apk --no-cache add ca-certificates wget
#RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub
#RUN wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.28-r0/glibc-2.28-r0.apk
#RUN apk add --no-cache --force-overwrite glibc-2.28-r0.apk
#RUN npm install -g bun
#CMD npm run db:migrations:do:all && npm run db:seeders:do:all && bun start:dev
CMD npm run db:migrations:do:all && npm run db:seeders:do:all && npm run start:dev

FROM node:18-alpine AS test-e2e
ENV NODE_ENV=test
WORKDIR /usr/src/app
COPY --chown=node:node --from=modules /usr/src/app/node_modules ./node_modules
COPY --chown=node:node . .
CMD npm run db:migrations:do:all && npm run test:e2e

FROM node:18-alpine AS build
ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY --chown=node:node --from=modules /usr/src/app/node_modules ./node_modules
COPY --chown=node:node . .
RUN npm run build

FROM node:18-alpine AS production
ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY --chown=node:node --from=production-modules /usr/src/app/node_modules ./node_modules
COPY --chown=node:node --from=build /usr/src/app/dist ./dist
COPY --chown=node:node package*.json .sequelizerc ./
COPY --chown=node:node ./db ./db
CMD npm run db:migrations:do:all && npm run db:seeders:do:all && node dist/main.js
