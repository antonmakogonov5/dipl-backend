'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('users', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      address: {
        type: Sequelize.STRING(255),
        allowNull: false,
        unique: true,
      },
      nonce: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      roleName: {
        type: Sequelize.STRING(255),
        allowNull: false,
        references: {
          model: 'roles',
          key: 'name',
        },
        onUpdate: 'cascade',
        onDelete: 'restrict',
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: queryInterface.sequelize.fn('NOW'),
      },
      updatedAt: {
        type: Sequelize.DATE,
      },
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('users');
  },
};
