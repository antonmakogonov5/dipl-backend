'use strict';

const dotenv = require('dotenv');

dotenv.config();

const config = {
  host: process.env.POSTGRES_HOST,
  port: process.env.POSTGRES_PORT,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  timezone: process.env.POSTGRES_TIMEZONE,
  dialect: 'postgres',
  migrationStorage: 'sequelize',
  seederStorage: 'sequelize',
};

module.exports = {
  development: config,
  test: config,
  production: config,
};
