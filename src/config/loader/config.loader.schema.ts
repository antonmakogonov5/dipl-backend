import { SequelizeModuleOptions } from '@nestjs/sequelize';
import { EnvTypes } from 'src/config/enums/config.enums.env-types';
import { AppConfig } from 'src/app/app.config';
import { AuthConfig } from 'src/auth/auth.config';
import { MailerConfig } from 'src/utils/config/utils.config.mailer';

export interface ConfigLoaderSchema {
  envType: EnvTypes;
  app: AppConfig;
  db: SequelizeModuleOptions;
  auth: AuthConfig;
  mailer: MailerConfig;
  frontendUrl: string;
}
