import { ConfigLoaderSchema } from 'src/config/loader/config.loader.schema';
import { EnvTypes } from 'src/config/enums/config.enums.env-types';
import { User } from 'src/user/user.model';
import { RefreshToken } from 'src/auth/models/auth.models.refresh-token';
import { AccessToken } from 'src/auth/models/auth.models.access-token';
import { ResetPasswordToken } from 'src/auth/models/auth.models.reset-password-token';
import { Setting } from '../../setting/models/setting.model';
import {Product} from "../../product/models/product.model";
import {Cart} from "../../cart/models/cart.model";
import {Order} from "../../order/models/order.model";
import {OrderProduct} from "../../order/models/order-product.model";

export function loadConfig(): ConfigLoaderSchema {
  return {
    envType: process.env.NODE_ENV as EnvTypes,
    app: {
      port: parseInt(process.env.APP_PORT),
      url: process.env.APP_URL,
      storage: process.env.STORAGE_FOLDER,
    },
    db: {
      host: process.env.POSTGRES_HOST,
      port: parseInt(process.env.POSTGRES_PORT),
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
      timezone: process.env.POSTGRES_TIMEZONE,
      dialect: 'postgres',
      logging: false,
      pool: {
        max: parseInt(process.env.POSTGRES_POOL_MAX),
      },
      models: [
        User,
        RefreshToken,
        AccessToken,
        ResetPasswordToken,
        Setting,
        Product,
        Cart,
        Order,
        OrderProduct,
      ],
      synchronize: false,
    },
    auth: {
      jwt: {
        secret: process.env.AUTH_JWT_SECRET,
      },
      ttl: {
        access: process.env.AUTH_JWT_TTL_ACCESS,
        refresh: process.env.AUTH_JWT_TTL_REFRESH,
        resetPassword: process.env.AUTH_JWT_TTL_RESET_PASSWORD,
      },
    },
    mailer: {
      options: {
        host: process.env.MAILER_HOST,
        port: parseInt(process.env.MAILER_PORT),
        secure: true,
        auth: {
          user: process.env.MAILER_USER,
          pass: process.env.MAILER_PASSWORD,
        },
        pool: true,
      },
      defaults: {
        from: process.env.MAILER_FROM,
      },
    },
    frontendUrl: process.env.FRONTEND_URL,
  }
}
