import { IsEnum, IsPort, IsNotEmpty, IsInt, Min, Max } from 'class-validator';
import { Type } from 'class-transformer';

import { EnvTypes } from 'src/config/enums/config.enums.env-types';

export class ConfigValidatorSchema {
  @IsEnum(EnvTypes)
  NODE_ENV: string;

  @IsPort()
  APP_PORT: string;

  @IsNotEmpty()
  APP_URL: string;

  @IsNotEmpty()
  STORAGE_FOLDER: string;

  @IsNotEmpty()
  POSTGRES_HOST: string;

  @IsPort()
  POSTGRES_PORT: string;

  @IsNotEmpty()
  POSTGRES_USER: string;

  @IsNotEmpty()
  POSTGRES_PASSWORD: string;

  @IsNotEmpty()
  POSTGRES_DB: string;

  @IsNotEmpty()
  POSTGRES_TIMEZONE: string;

  @Type(() => Number)
  @Max(20)
  @Min(1)
  @IsInt()
  POSTGRES_POOL_MAX: number;

  @IsNotEmpty()
  AUTH_JWT_SECRET: string;

  @IsNotEmpty()
  AUTH_JWT_TTL_ACCESS: string;

  @IsNotEmpty()
  AUTH_JWT_TTL_REFRESH: string;

  @IsNotEmpty()
  AUTH_JWT_TTL_RESET_PASSWORD: string;

  @IsNotEmpty()
  MAILER_HOST: string;

  @IsPort()
  MAILER_PORT: string;

  @IsNotEmpty()
  MAILER_USER: string;

  @IsNotEmpty()
  MAILER_PASSWORD: string;

  @IsNotEmpty()
  MAILER_FROM: string;

  @IsNotEmpty()
  FRONTEND_URL: string;
}
