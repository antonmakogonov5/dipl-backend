import { plainToInstance } from 'class-transformer';
import { ValidationError, validateSync } from 'class-validator';

import { ConfigValidatorSchema } from 'src/config/validator/config.validator.schema';

export function validateConfig(
  rawConfig: Record<string, string>,
): ConfigValidatorSchema {
  const config: ConfigValidatorSchema = plainToInstance(
    ConfigValidatorSchema,
    rawConfig,
  );
  const validationErrors: ValidationError[] = validateSync(config, {
    skipMissingProperties: false,
  });

  if (validationErrors.length > 0) {
    throw new Error(validationErrors.toString());
  }

  return config;
}
