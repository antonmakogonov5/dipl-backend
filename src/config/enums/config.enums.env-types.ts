export enum EnvTypes {
  development = 'development',
  test = 'test',
  production = 'production',
}
