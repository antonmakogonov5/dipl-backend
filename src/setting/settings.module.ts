import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { SettingController } from './setting.controller';
import { SettingService } from './setting.service';
import { Setting } from './models/setting.model';
import { AccessToken } from '../auth/models/auth.models.access-token';

@Module({
  imports: [SequelizeModule.forFeature([Setting, AccessToken])],
  controllers: [SettingController],
  providers: [SettingService],
  exports: [SettingService],
})
export class SettingsModule {}
