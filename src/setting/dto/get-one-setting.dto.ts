import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class GetOneSettingDto {
  @ApiProperty({
    example: 'key',
  })
  @IsString()
  key: string;
}
