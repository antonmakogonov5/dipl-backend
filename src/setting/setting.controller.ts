import {
  Body,
  Controller,
  Get,
  Param,
  Put,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiForbiddenResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { SettingService } from './setting.service';
import { InternalServerErrorResponse } from '../utils/responses/utils.responses.internal-error';
import { UnauthorizedErrorResponse } from '../auth/responses/auth.responses.unauthorized-error';
import { ResourceNotFoundErrorResponse } from '../utils/responses/utils.responses.not-found-error';
import { ForbiddenResponse } from '../utils/responses/forbidden-response';
import { JwtInterceptor } from '../auth/interceptors/auth.interceptors.jwt';
import { RolesInterceptor } from '../auth/interceptors/auth.interceptors.roles';

@Controller('settings')
@ApiTags('settings')
@ApiInternalServerErrorResponse({
  description: 'Internal error',
  type: InternalServerErrorResponse,
})
@ApiUnauthorizedResponse({
  description: 'Access token validation failed',
  type: UnauthorizedErrorResponse,
})
@ApiNotFoundResponse({
  description: 'Not found',
  type: ResourceNotFoundErrorResponse,
})
@ApiForbiddenResponse({
  description: 'Forbidden',
  type: ForbiddenResponse,
})
@UseInterceptors(JwtInterceptor, RolesInterceptor)
export class SettingController {
  constructor(private readonly settingService: SettingService) {}
}
