import { Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class SettingOutput {
  @Expose()
  @ApiProperty({ example: 1 })
  id: number;
  @Expose()
  @ApiProperty({ example: 'default_merchant_commission' })
  key: string;
  @Expose()
  @ApiProperty({ example: '5' })
  value: string;
  @Expose()
  @ApiProperty({ example: 'integer' })
  type: string;
}
