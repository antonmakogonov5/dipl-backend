import {
  Column,
  DataType,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript';
import { InferAttributes, InferCreationAttributes } from 'sequelize';

@Table({
  tableName: 'settings',
  timestamps: false,
  createdAt: false,
  updatedAt: false,
})
export class Setting extends Model<
  InferAttributes<Setting>,
  InferCreationAttributes<Setting>
> {
  @PrimaryKey
  @Column({
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
    type: DataType.INTEGER,
  })
  id: number;
  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  key: string;
  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  value: string;
  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  type: string;
}
