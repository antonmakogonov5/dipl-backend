import { Transaction } from 'sequelize';
import { Roles } from '../../auth/enums/auth.enums.roles';

export interface GetDefaultRateInput {
  role: Roles;
  transaction: Transaction;
}
