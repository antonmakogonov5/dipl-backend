export const SettingKeys = {
  DEFAULT_MERCHANT_COMMISSION: 'default_merchant_commission',
  DEFAULT_TRADER_COMMISSION: 'default_trader_commission',
} as const;

export type SettingKeys = (typeof SettingKeys)[keyof typeof SettingKeys];
