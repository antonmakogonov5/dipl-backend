import { Injectable } from '@nestjs/common';
import { SettingOutput } from './outputs/setting.output';
import { Setting } from './models/setting.model';
import { Transaction } from 'sequelize';
import { TranslatableValidationException } from '../utils/exceptions/translatable-validation-exception';
import { Roles } from '../auth/enums/auth.enums.roles';
import { InjectModel } from '@nestjs/sequelize';
import { isInt } from 'class-validator';
import { GetDefaultRateInput } from './input/get-default-rate.input';

@Injectable()
export class SettingService {
  constructor(
    @InjectModel(Setting)
    private readonly settingModel: typeof Setting,
  ) {}

}
