import {
  Injectable,
  Scope,
  PipeTransform,
  Inject,
  ArgumentMetadata,
  NotFoundException,
} from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/sequelize';
import { Transaction } from 'sequelize';
import { systemErrorMessages } from '../../utils/constants/utils.constants.system-error-messages';
import { Setting } from '../models/setting.model';

@Injectable({
  scope: Scope.REQUEST,
})
export class GetSettingByIdPipe implements PipeTransform {
  constructor(
    @Inject(REQUEST)
    private request: any,

    @InjectModel(Setting)
    private settingModel: typeof Setting,
  ) {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async transform(id: string, metadata: ArgumentMetadata): Promise<Setting> {
    const transaction: Transaction = this.request.transaction;
    const setting: Setting = await this.settingModel.findOne({
      transaction,
      where: {
        key: id,
      },
    });
    if (!setting) {
      throw new NotFoundException(systemErrorMessages.paydeskNotFound);
    }

    return setting;
  }
}
