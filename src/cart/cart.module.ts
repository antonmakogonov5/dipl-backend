import {Module} from "@nestjs/common";
import {SequelizeModule} from "@nestjs/sequelize";
import {AccessToken} from "../auth/models/auth.models.access-token";
import {CartController} from "./cart.controller";
import {CartService} from "./cart.service";
import {Cart} from "./models/cart.model";

@Module({
  imports: [
    SequelizeModule.forFeature([Cart,  AccessToken]),
  ],
  controllers: [CartController],
  providers: [CartService],
  exports: [CartService]
})
export class CartModule {
}