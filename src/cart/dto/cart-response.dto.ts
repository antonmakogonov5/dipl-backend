import {ProductCartDto} from "../../product/dto/product-cart.dto";

export class CartResponseDto {
    id: number;
    product: ProductCartDto;
    quantity: number;
    totalSum: number;
}