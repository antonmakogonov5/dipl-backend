import {IsDefined, IsNumber} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class AddToCartDto {
    @ApiProperty(
        {
            description: 'Product id',
            example: 1
        }
    )
    @IsNumber()
    @IsDefined()
    productId: number;
    @ApiProperty(
        {
            description: 'Quantity',
            example: 1
        }
    )
    @IsNumber()
    @IsDefined()
    quantity: number;
}