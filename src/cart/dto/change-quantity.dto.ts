import {IsDefined, IsInt} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class ChangeQuantityDto {
    @ApiProperty({example: 1})
    @IsInt()
    @IsDefined()
    quantity;
}