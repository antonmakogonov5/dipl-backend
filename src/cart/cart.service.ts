import {AddToCartDto} from "./dto/add-to-cart.dto";
import {User} from "../user/user.model";
import {Cart} from "./models/cart.model";
import {InjectModel} from "@nestjs/sequelize";
import {Product} from "../product/models/product.model";
import {AppConfig} from "../app/app.config";
import {ConfigService} from "@nestjs/config";
import {ConfigNames} from "../config/enums/config.enums.config-names";
import {ChangeQuantityDto} from "./dto/change-quantity.dto";
import {ForbiddenException, UnprocessableEntityException} from "@nestjs/common";
import {I18nContext, I18nService} from "nestjs-i18n";
import {EmptyResponse} from "../utils/responses/utils.responses.empty-response";

export class CartService {
  private appConfig: AppConfig;
  constructor(
    @InjectModel(Cart)
    private readonly cartModel: typeof Cart,
    private configService: ConfigService,
    private i18nService: I18nService,
  ) {
    this.appConfig = configService.get<AppConfig>(ConfigNames.app);
  }

  async create(addToCartDto: AddToCartDto, user: User) {
    const cart = await this.cartModel.findOne({
      where: {userId: user.id, productId: addToCartDto.productId},
    });
    if (cart) {
      const newQuantity = cart.quantity + addToCartDto.quantity;
      await this.cartModel.update({
        quantity: newQuantity
      }, {
        where: {userId: user.id, productId: addToCartDto.productId},
        returning: true
      });
    } else {
      await this.cartModel.create({
        userId: user.id,
        productId: addToCartDto.productId,
        quantity: addToCartDto.quantity
      });
    }
    const productCount = await this.cartModel.count({
      where: {userId: user.id}
    });

    return productCount;
  }

  async getCount(user: User) {
    return await this.cartModel.count({
      where: {userId: user.id}
    });
  }

  async getCart(user: User) {
    let cart = await this.cartModel.findAll({
      where: {userId: user.id},
      include: [Product]
    });
    return  cart.map((cartItem) => {
      return {
        id: cartItem.id,
        product: {
          id: cartItem.product.id,
          name: cartItem.product.name,
          price: cartItem.product.price,
          image: this.appConfig.url + '/public/' + cartItem.product.image,
          blockchainId: cartItem.product.blockchainId,
        },
        quantity: cartItem.quantity,
        totalSum: cartItem.quantity * cartItem.product.price
      }
    });
  }

  async updateQuantity(id: number,changeQuantityDto: ChangeQuantityDto, user: User) {
    const cart = await this.cartModel.findByPk(id);
    if (!cart) {
      throw new UnprocessableEntityException(
        this.i18nService.t('validation.PRODUCT_NOT_FOUND', {
          lang: I18nContext.current().lang,
        }),
      );
    }

    if(cart.userId !== user.id) {
      throw new ForbiddenException(
        this.i18nService.t('validation.FORBIDDEN', {
          lang: I18nContext.current().lang,
        }),
      );
    }

    if (changeQuantityDto.quantity === 0) {
      await this.cartModel.destroy({
        where: {id: id},
      });
    } else {
      await this.cartModel.update({
        quantity: changeQuantityDto.quantity
      }, {
        where: {id: id},
        returning: true
      });
    }
    return new EmptyResponse();
  }

  async delete(id: number, user: User) {
    const cart = await this.cartModel.findByPk(id);
    if (!cart) {
      throw new UnprocessableEntityException(
        this.i18nService.t('validation.PRODUCT_NOT_FOUND', {
          lang: I18nContext.current().lang,
        }),
      );
    }

    if(cart.userId !== user.id) {
      throw new ForbiddenException(
        this.i18nService.t('validation.FORBIDDEN', {
          lang: I18nContext.current().lang,
        }),
      );
    }

    await this.cartModel.destroy({
      where: {id: id},
    });
    return new EmptyResponse();
  }
}