import {Body, Controller, Delete, Get, Param, Post, Put, UseGuards, UseInterceptors} from "@nestjs/common";
import {CartService} from "./cart.service";
import {JwtInterceptor} from "../auth/interceptors/auth.interceptors.jwt";
import {AddToCartDto} from "./dto/add-to-cart.dto";
import {CurrentAccessToken} from "../auth/decorators/auth.decorators.current-access-token";
import {AccessToken} from "../auth/models/auth.models.access-token";
import {EmptyResponse} from "../utils/responses/utils.responses.empty-response";
import {CartResponseDto} from "./dto/cart-response.dto";
import {ChangeQuantityDto} from "./dto/change-quantity.dto";
import {IdDto} from "../product/dto/id.dto";

@Controller('cart')
export class CartController {
  constructor(
    public readonly cartService: CartService,
  ) {
  }

  @Post()
  @UseInterceptors(JwtInterceptor)
  async create(
    @CurrentAccessToken() accessToken: AccessToken,
    @Body() createCartDto: AddToCartDto,
  ): Promise<number> {
    return await this.cartService.create(
      createCartDto,
      accessToken.user,
    );
  }

  @Get('/count')
  @UseInterceptors(JwtInterceptor)
  async getCount(
    @CurrentAccessToken() accessToken: AccessToken,
  ): Promise<number> {
    return await this.cartService.getCount(accessToken.user);
  }

  @Get()
  @UseInterceptors(JwtInterceptor)
  async getCart(
    @CurrentAccessToken() accessToken: AccessToken,
  ): Promise<CartResponseDto[]> {
    return await this.cartService.getCart(accessToken.user);
  }

  @Put('/:id')
  @UseInterceptors(JwtInterceptor)
  async changeQuantity(
    @CurrentAccessToken() accessToken: AccessToken,
    @Param() params: IdDto,
    @Body() changeQuantityDto: ChangeQuantityDto,
  ): Promise<EmptyResponse> {
    return await this.cartService.updateQuantity(
      params.id,
      changeQuantityDto,
      accessToken.user,
    );
  }

  @Delete('/:id')
  @UseInterceptors(JwtInterceptor)
  async delete(
    @CurrentAccessToken() accessToken: AccessToken,
    @Param() params: IdDto,
  ): Promise<EmptyResponse> {
    return await this.cartService.delete(
      params.id,
      accessToken.user,
    );
  }
}