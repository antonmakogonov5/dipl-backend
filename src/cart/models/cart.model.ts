import {BelongsTo, Column, DataType, ForeignKey, Model, PrimaryKey, Table} from "sequelize-typescript";
import {InferAttributes, InferCreationAttributes} from "sequelize";
import {Product} from "../../product/models/product.model";

@Table({
  tableName: 'cart',
  timestamps: false,
})
export class Cart extends Model<
  InferAttributes<Cart>,
  InferCreationAttributes<Cart>
> {
  @PrimaryKey
  @Column({
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
    type: DataType.INTEGER,
  })
  id: number;
  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  userId: number;
  @ForeignKey(() => Product)
  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  productId: number;
  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  quantity: number;
  @BelongsTo(() => Product)
  product: Product;
}