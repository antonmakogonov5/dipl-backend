import {IsInt, IsOptional} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";
import {Transform} from "class-transformer";

export class GetAllOrdersDto {
  @ApiProperty({example: 1, description: 'Page'})
  @IsInt()
  @Transform(({value}) => parseInt(value))
  @IsOptional()
  page: number;
  @ApiProperty({example: 10, description: 'Limit'})
  @IsInt()
  @Transform(({value}) => parseInt(value))
  @IsOptional()
  limit: number;
}