import {IsEmail, IsInt, IsNotEmpty, IsString} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";
import {Optional} from "@nestjs/common";

export class CreateOrderDto {
  @ApiProperty({example: "James", description: 'First name'})
  @IsString()
  @IsNotEmpty()
  firstName: string;
  @ApiProperty({example: "James", description: 'Last name'})
  @IsString()
  @IsNotEmpty()
  lastName: string;
  @ApiProperty({example: "example@email", description: 'Email'})
  @IsEmail()
  @IsNotEmpty()
  email:  string;
  @ApiProperty({example: "+380123456789", description: 'Phone'})
  @IsString()
  @IsNotEmpty()
  phone: string;
  @ApiProperty({example: "USA", description: 'Country'})
  @IsString()
  @IsNotEmpty()
  country: string;
  @ApiProperty({example: "Address", description: 'Address'})
  @IsString()
  @IsNotEmpty()
  address: string;
  @ApiProperty({example: "City", description: 'City'})
  @IsString()
  @IsNotEmpty()
  city: string;
  @ApiProperty({example: "State", description: 'State'})
  @IsString()
  @IsNotEmpty()
  state: string;
  @ApiProperty({example: "Zip", description: 'Zip'})
  @IsString()
  @IsNotEmpty()
  zip:  string;
  @ApiProperty({example: "Comment", description: 'Comment'})
  @IsString()
  @Optional()
  comment: string;
  @ApiProperty({example: "0x123456789", description: 'Transaction hash'})
  @IsString()
  @IsNotEmpty()
  txHash: string;
  @ApiProperty({example: [1, 2, 3], description: 'Product ids'})
  @IsInt({each: true})
  @IsNotEmpty()
  productIds: number[];
}