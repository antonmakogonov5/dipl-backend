import {Body, Controller, Get, Param, Post, Query, UseInterceptors} from "@nestjs/common";
import {OrderService} from "./order.service";
import {JwtInterceptor} from "../auth/interceptors/auth.interceptors.jwt";
import {CreateOrderDto} from "./dto/create-order.dto";
import {AccessToken} from "../auth/models/auth.models.access-token";
import {CurrentAccessToken} from "../auth/decorators/auth.decorators.current-access-token";
import {RequestTransaction} from "../utils/decorators/utils.decorators.request-transaction";
import {Transaction} from "sequelize";
import {Order} from "./models/order.model";
import {GetAllOrdersDto} from "./dto/get-all-orders.dto";
import {IdDto} from "../product/dto/id.dto";

@Controller('orders')
export class OrderController {
  constructor(
    public readonly orderService: OrderService,
  ) {
  }

  @Post()
  @UseInterceptors(JwtInterceptor)
  async createOrder(
     @Body() createOrderDto: CreateOrderDto,
     @CurrentAccessToken() accessToken: AccessToken,
     @RequestTransaction() transaction: Transaction,
  ): Promise<number> {
    return await this.orderService.createOrder(createOrderDto, accessToken.user, transaction);
  }

  @Get()
  @UseInterceptors(JwtInterceptor)
  async findAll(
    @Query() query: GetAllOrdersDto,
    @CurrentAccessToken() accessToken: AccessToken,
  ): Promise<any> {
    return this.orderService.findAll(accessToken.user, query);
  }

  @Get(':id')
  @UseInterceptors(JwtInterceptor)
  async findOne(
    @CurrentAccessToken() accessToken: AccessToken,
    @Param() param: IdDto,
  ): Promise<Order> {
    return this.orderService.findOne(accessToken.user, param.id);
  }
}