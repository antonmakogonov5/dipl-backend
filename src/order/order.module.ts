import {Module} from "@nestjs/common";
import {SequelizeModule} from "@nestjs/sequelize";
import {Order} from "./models/order.model";
import {AccessToken} from "../auth/models/auth.models.access-token";
import {OrderController} from "./order.controller";
import {OrderService} from "./order.service";
import {OrderProduct} from "./models/order-product.model";
import {Cart} from "../cart/models/cart.model";

@Module({
  imports: [
    SequelizeModule.forFeature([Order, OrderProduct, Cart,  AccessToken]),],
  controllers: [OrderController],
  providers: [OrderService],
  exports: [OrderService],
})
export class OrderModule {
}