import {InjectModel} from "@nestjs/sequelize";
import {Order} from "./models/order.model";
import {CreateOrderDto} from "./dto/create-order.dto";
import {User} from "../user/user.model";
import {OrderProduct} from "./models/order-product.model";
import {Cart} from "../cart/models/cart.model";
import {Op, Transaction} from "sequelize";
import {GetAllOrdersDto} from "./dto/get-all-orders.dto";

export class OrderService {
  constructor(
    @InjectModel(Order)
    private readonly orderModel: typeof Order,
    @InjectModel(OrderProduct)
    private readonly orderProductModel: typeof OrderProduct,
    @InjectModel(Cart)
    private readonly cartModel: typeof Cart,
  ) {
  }

  async createOrder(createOrderDto: CreateOrderDto, user: User, transaction: Transaction): Promise<number> {
    const order = await this.orderModel.create({
      ...createOrderDto,
      userId: user.id,
    }, {transaction});
    console.log(createOrderDto)
    const orderProducts = await this.cartModel.findAll({
      where: {
        userId: user.id,
         productId: {
          [Op.in]: createOrderDto.productIds,
         }
      },
      // include: [Cart.associations.product],
      include: ["product"],
      transaction,
      logging: true,
    });
   console.log(orderProducts.map((orderProduct) => ({
     orderId: order.id,
     productId: orderProduct.productId,
     quantity: orderProduct.quantity,
     price: orderProduct.product.price,
   })));
    await this.orderProductModel.bulkCreate(
      orderProducts.map((orderProduct) => ({
        orderId: order.id,
        productId: orderProduct.productId,
        quantity: orderProduct.quantity,
        price: orderProduct.product.price,
      })),
      {transaction},);

    return order.id;
  }

  async findAll(user: User, query: GetAllOrdersDto): Promise<any> {
    if(user.role === "admin") {
      return this.orderModel.findAndCountAll({
        attributes: ["id", "firstName", "lastName", "createdAt"],
        include: ["user"],
        limit: query.limit,
        offset: query.limit * (query.page - 1),
        order: [["id", "DESC"]],
      });
    } else {
      return this.orderModel.findAndCountAll({
        attributes: ["id", "firstName", "lastName", "createdAt"],
        where: {
          userId: user.id,
        },
        include: ["user"],
        limit: query.limit,
        offset: query.limit * (query.page - 1),
        order: [["id", "DESC"]],
      });
    }
  }

  async findOne(user: User, id: number): Promise<Order> {
    const order = await this.orderModel.findByPk(id, {
      include: ["products"],
    });
    if (user.role !== "admin" || order.userId !== user.id) {
      throw new Error("You don't have access to this order");
    }

    return order;
  }
}