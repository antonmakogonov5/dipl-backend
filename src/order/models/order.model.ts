import {BelongsTo, BelongsToMany, Column, DataType, ForeignKey, Model, PrimaryKey, Table} from "sequelize-typescript";
import {InferAttributes, InferCreationAttributes} from "sequelize";
import {User} from "../../user/user.model";
import {Product} from "../../product/models/product.model";
import {OrderProduct} from "./order-product.model";

@Table({
  tableName: 'orders',
  timestamps: true,
})
export class Order extends Model<
  InferAttributes<Order>,
  InferCreationAttributes<Order>
> {
  @PrimaryKey
  @Column({
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
    type: DataType.INTEGER,
  })
  id: number;
  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  firstName: string;
  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  lastName: string;
  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  email: string;
  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  phone: string;
  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  country: string;
  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  address: string;
  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  city: string;
  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  state: string;
  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  zip: string;
  @Column({
    allowNull: false,
    type: DataType.STRING,
  })
  comment: string;
  @Column({
    allowNull: true,
    type: DataType.STRING,
  })
  txHash: string;
  @ForeignKey(() => User)
  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  userId: number;
  @BelongsTo(() => User)
  user: User;
  @BelongsToMany(() => Product, () => OrderProduct)
  products: Product[];
}