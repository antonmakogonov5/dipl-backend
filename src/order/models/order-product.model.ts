import {Column, DataType, ForeignKey, Model, PrimaryKey, Table} from "sequelize-typescript";
import {InferAttributes, InferCreationAttributes} from "sequelize";
import {Order} from "./order.model";
import {Product} from "../../product/models/product.model";

@Table({
  tableName: 'order_product',
  timestamps: false,
})
export class OrderProduct extends Model<
  InferAttributes<OrderProduct>,
  InferCreationAttributes<OrderProduct>
>{
  @PrimaryKey
  @Column({
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
    type: DataType.INTEGER,
  })
  id: number;
  @ForeignKey(() => Order)
  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  orderId: number;
  @ForeignKey(() => Product)
  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  productId: number;
  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  quantity: number;
  @Column({
    allowNull: false,
    type: DataType.INTEGER,
  })
  price: number;
}