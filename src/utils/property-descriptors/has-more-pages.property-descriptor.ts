import { ApiPropertyOptions } from '@nestjs/swagger';

import { examples } from 'src/utils/constants/examples.constant';

export const hasMorePagesDescriptor: ApiPropertyOptions = {
  description: 'Indicates there are more pages or no more',
  example: examples.hasMorePages,
};
