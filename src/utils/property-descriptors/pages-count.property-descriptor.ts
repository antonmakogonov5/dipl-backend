import { ApiPropertyOptions } from '@nestjs/swagger';

import { validationConstraints } from 'src/utils/constants/validation-constraints.constant';
import { examples } from 'src/utils/constants/examples.constant';

export const pagesCountDescriptor: ApiPropertyOptions = {
  description: 'Total pages count (integer)',
  minimum: validationConstraints.pagesCount.min,
  example: examples.pagesCount,
};
