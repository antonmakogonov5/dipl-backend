import { ApiPropertyOptions } from '@nestjs/swagger';

import { validationConstraints } from 'src/utils/constants/validation-constraints.constant';
import { defaults } from 'src/utils/constants/defaults.constant';

export const pageDescriptor: ApiPropertyOptions = {
  description: 'Page number to load (integer)',
  required: false,
  minimum: validationConstraints.page.min,
  default: defaults.page,
  example: defaults.page,
};
