import { ApiPropertyOptions } from '@nestjs/swagger';

import { examples } from 'src/utils/constants/examples.constant';

export const updatedAtDescriptor: ApiPropertyOptions = {
  description: 'Last update in `YYYY-MM-DDTHH:mm:ss[Z]` format',
  example: examples.date,
};
