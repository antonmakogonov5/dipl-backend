import { ApiPropertyOptions } from '@nestjs/swagger';

import { examples } from 'src/utils/constants/examples.constant';

export const createdAtDescriptor: ApiPropertyOptions = {
  description: 'Creation date in `YYYY-MM-DDTHH:mm:ss[Z]` format',
  example: examples.date,
};
