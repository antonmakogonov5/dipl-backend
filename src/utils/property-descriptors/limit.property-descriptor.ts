import { ApiPropertyOptions } from '@nestjs/swagger';

import { validationConstraints } from 'src/utils/constants/validation-constraints.constant';
import { defaults } from 'src/utils/constants/defaults.constant';
import { examples } from 'src/utils/constants/examples.constant';

export const limitDescriptor: ApiPropertyOptions = {
  description: 'Number of entities to load (integer)',
  required: false,
  minimum: validationConstraints.limit.min,
  maximum: validationConstraints.limit.max,
  default: defaults.limit,
  example: examples.limit,
};
