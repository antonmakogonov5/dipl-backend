import { ApiPropertyOptions } from '@nestjs/swagger';

import { validationConstraints } from 'src/utils/constants/validation-constraints.constant';
import { examples } from 'src/utils/constants/examples.constant';

export const idDescriptor: ApiPropertyOptions = {
  description: 'Entity identifier (integer)',
  minimum: validationConstraints.id.min,
  example: examples.id,
};
