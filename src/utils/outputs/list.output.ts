import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';

import { pagesCountDescriptor } from 'src/utils/property-descriptors/pages-count.property-descriptor';
import { pageDescriptor } from '../property-descriptors/page.property-descriptor';

export class ListOutput {
  @ApiProperty(pagesCountDescriptor)
  @Expose()
  pagesCount: number;

  @ApiProperty(pageDescriptor)
  @Expose()
  page: number;
}
