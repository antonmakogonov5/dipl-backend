import { Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

import { IdentityOutput } from 'src/utils/outputs/identity.output';
import { createdAtDescriptor } from 'src/utils/property-descriptors/created-at.property-descriptor';
import { updatedAtDescriptor } from 'src/utils/property-descriptors/updated-at.property-descriptor';

export class IdentityWithTimestampsOutput extends IdentityOutput {
  @ApiProperty(createdAtDescriptor)
  @Expose()
  createdAt: Date;

  @ApiProperty(updatedAtDescriptor)
  @Expose()
  updatedAt: Date;
}
