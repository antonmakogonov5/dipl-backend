import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';

import { idDescriptor } from 'src/utils/property-descriptors/id.property-descriptor';

export class IdentityOutput {
  @ApiProperty(idDescriptor)
  @Expose()
  id: number;
}
