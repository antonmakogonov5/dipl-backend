import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { Transaction } from 'sequelize';

export const RequestTransaction = createParamDecorator(
  (data: any, context: ExecutionContext): Transaction => {
    if (context.getType() === 'http') {
      const httpContext = context.switchToHttp();
      const request = httpContext.getRequest();

      return request.transaction as Transaction;
    }
  },
);
