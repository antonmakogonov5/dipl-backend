import { applyDecorators } from '@nestjs/common';
import { Type } from 'class-transformer';
import { IsOptional, IsInt, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

import { pageDescriptor } from 'src/utils/property-descriptors/page.property-descriptor';
import { i18nValidationMessage } from 'nestjs-i18n';
import { type I18nTranslations } from '../../generated/i18n.generated';
import { validationConstraints } from 'src/utils/constants/validation-constraints.constant';

export function IsPage() {
  return applyDecorators(
    ApiProperty(pageDescriptor),
    Min(validationConstraints.page.min, {
      message: i18nValidationMessage<I18nTranslations>('validation.MIN'),
    }),
    IsInt({
      message: i18nValidationMessage<I18nTranslations>('validation.IS_INT'),
    }),
    Type(() => Number),
    IsOptional(),
  );
}
