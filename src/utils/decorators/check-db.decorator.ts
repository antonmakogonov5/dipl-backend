export const CheckDbMetadataKey = 'check-db';

/**
 * Decorator is used to define property metadata for CheckDBValidationPipe
 * @param options.table - table in db to check the record
 * @param options.message - i18n key for the error message
 * @param options.column - column in db to check the record (takes property name if not provided)
 * @param options.shouldExist - specifies if the record should exist in db (true by default)
 */
export function CheckDB(options: {
  table: string;
  message: string;
  column?: string;
  shouldExist?: boolean;
}) {
  return function (target: any, property: string) {
    options.shouldExist ??= true;
    options.column ??= property;
    const existingMetadata =
      Reflect.getMetadata(CheckDbMetadataKey, target) || [];
    const newMetadata = [
      ...existingMetadata,
      { property: property, ...options },
    ];
    Reflect.defineMetadata(CheckDbMetadataKey, newMetadata, target);
  };
}
