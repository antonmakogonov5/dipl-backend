import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';

dayjs.extend(customParseFormat);

export function IsDateOnly(validationOptions?: ValidationOptions) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      validator: IsDateOnlyConstraint,
    });
  };
}

@ValidatorConstraint({
  name: 'IsDateOnly',
})
class IsDateOnlyConstraint implements ValidatorConstraintInterface {
  validate(value: any, args: ValidationArguments) {
    return dayjs(value, 'DD.MM.YYYY', true).isValid();
  }
}
