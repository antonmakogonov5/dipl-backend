import { SetMetadata } from '@nestjs/common';
import { Transaction } from 'sequelize';

export const IsolationLevel = (isolationLevel: Transaction.ISOLATION_LEVELS) => {
  return SetMetadata('isolationLevel', isolationLevel);
};
