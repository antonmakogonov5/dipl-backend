import { applyDecorators } from '@nestjs/common';
import { Type } from 'class-transformer';
import { IsOptional, IsInt, Min, Max } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

import { limitDescriptor } from 'src/utils/property-descriptors/limit.property-descriptor';
import { i18nValidationMessage } from 'nestjs-i18n';
import { type I18nTranslations } from '../../generated/i18n.generated';
import { validationConstraints } from 'src/utils/constants/validation-constraints.constant';

export function IsLimit() {
  return applyDecorators(
    ApiProperty(limitDescriptor),
    Max(validationConstraints.limit.max, {
      message: i18nValidationMessage<I18nTranslations>('validation.MAX'),
    }),
    Min(validationConstraints.limit.min, {
      message: i18nValidationMessage<I18nTranslations>('validation.MIN'),
    }),
    IsInt({
      message: i18nValidationMessage<I18nTranslations>('validation.IS_INT'),
    }),
    IsOptional(),
    Type(() => Number),
  );
}
