import { applyDecorators } from '@nestjs/common';
import { Type } from 'class-transformer';
import { IsInt, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

import { i18nValidationMessage } from 'nestjs-i18n';
import { I18nTranslations } from '../../generated/i18n.generated';

export function IsIntIdentifier() {
  return applyDecorators(
    Type(() => Number),
    IsInt({
      message: i18nValidationMessage<I18nTranslations>('validation.IS_INT'),
    }),
    Min(1, {
      message: i18nValidationMessage<I18nTranslations>('validation.MIN'),
    }),
    ApiProperty({
      description: 'Identifier of entity',
      example: 1,
    }),
  );
}
