import { applyDecorators } from '@nestjs/common';
import { Type } from 'class-transformer';
import { IsOptional, IsInt, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

import { i18nValidationMessage } from 'nestjs-i18n';
import { I18nTranslations } from '../../generated/i18n.generated';

export function IsOffset() {
  return applyDecorators(
    IsOptional(),
    Type(() => Number),
    IsInt({
      message: i18nValidationMessage<I18nTranslations>('validation.IS_INT'),
    }),
    Min(0, {
      message: i18nValidationMessage<I18nTranslations>('validation.MIN'),
    }),
    ApiProperty({
      description: 'Number of entities to skip from start',
      default: 0,
      minimum: 0,
      required: false,
    }),
  );
}
