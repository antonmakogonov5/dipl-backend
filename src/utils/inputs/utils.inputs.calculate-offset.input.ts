export interface CalculateOffsetInput {
  page: number;
  limit: number;
}
