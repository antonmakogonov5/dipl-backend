import { Transaction } from 'sequelize';

export interface TransactionableInput {
  transaction?: Transaction;
}
