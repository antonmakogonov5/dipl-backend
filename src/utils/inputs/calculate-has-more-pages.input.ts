export interface CalculateHasMorePagesInput {
  page: number;
  pagesCount: number;
}
