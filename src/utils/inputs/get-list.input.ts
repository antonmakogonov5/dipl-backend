import { TransactionableInput } from 'src/utils/inputs/transactionable.input';

export interface GetListInput extends TransactionableInput {
  page: number;
  limit: number;
}
