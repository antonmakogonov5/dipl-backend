import { TransactionableInput } from 'src/utils/inputs/transactionable.input';

export interface IdentifierInput extends TransactionableInput {
  id: number;
}
