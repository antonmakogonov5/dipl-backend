export interface CalculatePagesCountInput {
  count: number;
  limit: number;
}
