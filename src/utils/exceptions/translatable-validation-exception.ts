import { UnprocessableEntityException } from '@nestjs/common';
export type TranslatableError = {
  fieldName: string;
  translationKey: string;
  translationOptions?: Record<string, unknown>;
};
export class TranslatableValidationException extends UnprocessableEntityException {
  constructor(...errors: TranslatableError[]) {
    super({
      statusCode: 422,
      message: 'Bad Request',
      errors,
    });
  }
}
