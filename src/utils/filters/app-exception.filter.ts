import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  ValidationError,
} from '@nestjs/common';
import {
  I18nContext,
  I18nValidationException,
  I18nValidationExceptionFilter,
} from 'nestjs-i18n';
import { AppLogger } from '../logger/app.logger';
import { HttpAdapterHost } from '@nestjs/core';
import { handleValidationError } from '../helpers/utils.helpers.validation-error-handler';
import { unlinkSync } from 'fs';
import {
  TranslatableValidationException,
  TranslatableError,
} from '../exceptions/translatable-validation-exception';

@Catch()
export class AppExceptionFilter implements ExceptionFilter {
  private validationFilter: I18nValidationExceptionFilter;
  constructor(
    private readonly httpAdapterHost: HttpAdapterHost,
    private readonly appLogger: AppLogger,
  ) {
    this.validationFilter = new I18nValidationExceptionFilter({
      errorHttpStatusCode: 422,
      errorFormatter(validationErrors: ValidationError[]) {
        const responseErrors: any = {};

        validationErrors.forEach(function (validationError: ValidationError) {
          handleValidationError(validationError, responseErrors);
        });

        return responseErrors;
      },
    });
  }

  translateErrors(errors: TranslatableError[], i18n: I18nContext) {
    const response: Record<string, string[]> = {};
    errors.forEach((error) => {
      response[error.fieldName] = [
        i18n.t(error.translationKey, {
          lang: I18nContext.current().lang,
          ...error.translationOptions,
        }),
      ];
    });
    return response;
  }

  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();
    const i18n = I18nContext.current();
    if (request.file) {
      const file = request.file as Express.Multer.File;
      try {
        unlinkSync(file.path);
      } catch (error) {
        this.appLogger.error(error, 'AppExceptionFilter');
      }
    }
    if (exception instanceof I18nValidationException) {
      return this.validationFilter.catch(exception, host);
    }
    const error = {
      statusCode: 500,
      timestamp: new Date().toISOString(),
      path: request.url,
      method: request.method,
      message: 'InternalError',
      errors: undefined as unknown,
    };
    if (exception instanceof TranslatableValidationException) {
      error.statusCode = exception.getStatus();
      error.message = exception.message;
      const exceptionResponse = exception.getResponse();
      if (
        typeof exceptionResponse === 'object' &&
        'errors' in exceptionResponse
      ) {
        error.errors = this.translateErrors(
          exceptionResponse.errors as TranslatableError[],
          i18n,
        );
      } else {
        error.errors = exceptionResponse;
      }
    } else if (exception instanceof HttpException) {
      if (exception.message === 'File too large') {
        exception.message = 'FILE_TOO_LARGE';
      }
      error.statusCode = exception.getStatus();
      if (i18n && /[A-Z_]+/.test(exception.message)) {
        error.message = i18n?.t(`general.ERRORS.${exception.message}`, {
          lang: i18n.lang,
        });
      } else {
        error.message = exception.message;
      }
    }
    if (error.statusCode === 500) {
      this.appLogger.error(exception, 'InternalError');
    }

    if (
      typeof exception === 'object' &&
      'code' in exception &&
      exception.code === 'ENOENT' &&
      request.url.startsWith('/public')
    ) {
      error.statusCode = 404;
      error.message = 'NOT_FOUND';
    }

    const { httpAdapter } = this.httpAdapterHost;
    const isHttpApp = ['http', 'express'].includes(httpAdapter.getType());
    if (isHttpApp) {
      response.status(error.statusCode).json(error);
    }
  }
}
