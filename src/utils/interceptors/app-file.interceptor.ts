import { FileInterceptor } from '@nestjs/platform-express';
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { diskStorage } from 'multer';
import { extname, join } from 'path';
import { User } from '../../user/user.model';
import 'dotenv/config';
import { TranslatableValidationException } from '../exceptions/translatable-validation-exception';
import { I18nContext } from 'nestjs-i18n';
export function AppFileInterceptor(
  i18n: I18nContext,
  folderName: string,
  fieldName: string,
  mimeTypes: string[],
  fileSize = 5000000,
  privateAccess: boolean = false,
) {
  const localOptions: MulterOptions = {
    storage: diskStorage({
      destination: join(
        process.cwd(),
        privateAccess
          ? `${process.env.STORAGE_FOLDER}/private/${folderName}`
          : `${process.env.STORAGE_FOLDER}/public/${folderName}`,
      ),
      filename(request: any, file: Express.Multer.File, callback) {
        if (fieldName === 'avatar') {
          const user: User = request.user;
          callback(null, `avatar-${user.id}${extname(file.originalname)}`);
        } else {
          callback(null, `${Date.now()}${extname(file.originalname)}`);
        }
      },
    }),
    fileFilter(request: any, file: Express.Multer.File, callback) {
      if (!mimeTypes.includes(file.mimetype)) {
        callback(
          new TranslatableValidationException({
            fieldName,
            translationKey: 'validation.INVALID_FILE_TYPE',
            translationOptions: { args: { types: mimeTypes.join(', ') } },
          }),
          false,
        );
        return;
      }
      callback(null, true);
    },
    limits: { fileSize },
  };
  return FileInterceptor(fieldName, localOptions);
}
