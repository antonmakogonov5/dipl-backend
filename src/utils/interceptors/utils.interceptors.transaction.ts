import { Injectable, NestInterceptor, CallHandler, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Sequelize } from 'sequelize-typescript';
import { Observable, tap, catchError } from 'rxjs';
import { Transaction } from 'sequelize';

@Injectable()
export class TransactionInterceptor implements NestInterceptor {
  constructor(
    private reflector: Reflector,
    private sequelize: Sequelize,
  ) {}

  async intercept(context: ExecutionContext, next: CallHandler<any>): Promise<Observable<any>> {
    const isolationLevel: Transaction.ISOLATION_LEVELS =
      this.reflector.getAllAndOverride<Transaction.ISOLATION_LEVELS>('isolationLevel', [
        context.getHandler(),
        context.getClass(),
      ]);
    const transaction: Transaction = await this.sequelize.transaction({
      isolationLevel: isolationLevel || Transaction.ISOLATION_LEVELS.READ_COMMITTED,
    });

    if (context.getType() === 'http') {
      const httpContext = context.switchToHttp();
      const request = httpContext.getRequest();

      request.transaction = transaction;
    }

    return next.handle().pipe(
      tap(async () => {
        await transaction.commit();
      }),
      catchError(async (error) => {
        await transaction.rollback();

        throw error;
      }),
    );
  }
}
