import { IdentityPlain } from 'src/utils/plains/identity.plain';

export interface IdentityWithTimestampsPlain extends IdentityPlain {
  createdAt: Date;
  updatedAt: Date;
}
