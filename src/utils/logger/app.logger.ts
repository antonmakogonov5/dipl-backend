import { Injectable, LoggerService } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { createWriteStream, existsSync, mkdirSync } from 'fs';
import { AppConfig } from '../../app/app.config';
import { ConfigNames } from '../../config/enums/config.enums.config-names';
import { join } from 'path';

@Injectable()
export class AppLogger implements LoggerService {
  constructor(private readonly configService: ConfigService) {
    const config = this.configService.get<AppConfig>(ConfigNames.app);
    if (!existsSync(`${config.storage}/logs`)) {
      try {
        mkdirSync(join(process.cwd(), config.storage, 'logs'));
      } catch (err) {
        console.error(err);
      }
    }
  }

  log(message: any, ...optionalParams: any[]) {
    this.write('Log', message, optionalParams);
  }

  /**
   * Write an 'error' level log.
   */
  error(message: any, ...optionalParams: any[]) {
    if (message instanceof Error) {
      optionalParams = [optionalParams[1], message.stack.replace(/\n/g, '')];
      message = message.message;
    }
    this.write('Error', message, optionalParams);
  }

  /**
   * Write a 'warn' level log.
   */
  warn(message: any, ...optionalParams: any[]) {
    this.write('Warn', message, optionalParams);
  }

  /**
   * Write a 'debug' level log.
   */
  debug?(message: any, ...optionalParams: any[]) {
    this.write('Debug', message, optionalParams);
  }

  /**
   * Write a 'verbose' level log.
   */
  verbose?(message: any, ...optionalParams: any[]) {
    this.write('Verbose', message, optionalParams);
  }

  private write(type: string, message: unknown, optionalParams: any[]) {
    const config = this.configService.get<AppConfig>(ConfigNames.app);
    const logFile = createWriteStream(
      `${config.storage}/logs/${new Date().toISOString().slice(0, 10)}.app.log`,
      { flags: 'a' },
    );
    let context = 'App';
    if (typeof optionalParams?.[0] === 'string') {
      context = optionalParams.shift();
    }
    const logData =
      typeof message === 'object' ? JSON.stringify(message) : message;
    const params = optionalParams?.length
      ? `[${JSON.stringify(optionalParams)}]`
      : optionalParams;
    const logLone = `[${new Date().toISOString()}][${context}][${type}]: ${logData} ${params}`;
    logFile.write(logLone + '\n');
  }
}
