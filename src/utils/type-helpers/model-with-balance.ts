export type ModelWithBalance<Model> = Model & { balance: string };
