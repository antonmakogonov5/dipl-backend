export interface MailerConfig {
  options: {
    host: string;
    port: number;
    secure: boolean;
    auth: {
      user: string;
      pass: string;
    };
    pool: boolean;
  };
  defaults: {
    from: string;
  };
}
