export interface CipherConfig {
  algorithm: string;
  vector: string;
  secret: string;
}
