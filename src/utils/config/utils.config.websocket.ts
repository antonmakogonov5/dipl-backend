export interface WebsocketConfig {
  port: number;
  path: string;
  origin: string;
}
