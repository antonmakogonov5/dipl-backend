import { Injectable } from '@nestjs/common';
import { hash, compare } from 'bcryptjs';

@Injectable()
export class PasswordService {
  async encrypt(password: string): Promise<string> {
    return hash(password, 7);
  }

  async check(password: string, hash: string): Promise<boolean> {
    return compare(password, hash);
  }
}
