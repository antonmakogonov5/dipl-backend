import { Injectable } from '@nestjs/common';

import { CalculateOffsetInput } from 'src/utils/inputs/utils.inputs.calculate-offset.input';
import { CalculatePagesCountInput } from 'src/utils/inputs/utils.inputs.calculate-pages-count.input';
import { CalculateHasMorePagesInput } from 'src/utils/inputs/calculate-has-more-pages.input';

@Injectable()
export class PaginationService {
  calculateOffset(input: CalculateOffsetInput): number {
    return (input.page - 1) * input.limit;
  }

  calculatePagesCount(input: CalculatePagesCountInput): number {
    if (input.count === 0) {
      return 0;
    }

    return Math.ceil(input.count / input.limit);
  }

  calculateHasMorePages(input: CalculateHasMorePagesInput): boolean {
    return input.page < input.pagesCount;
  }
}
