import { Injectable } from '@nestjs/common';
import { Chance } from 'chance';

@Injectable()
export class RandomService {
  private chance: Chance.Chance;

  constructor() {
    this.chance = new Chance.Chance();
  }

  generatePassword(length: number = 10): string {
    return this.chance.string({
      length,
      alpha: true,
      numeric: true,
    });
  }
}
