import { Global, Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { APP_INTERCEPTOR } from '@nestjs/core';

import { User } from 'src/user/user.model';
import { PasswordService } from 'src/utils/services/utils.services.password';
import { RandomService } from 'src/utils/services/utils.services.random';
import { TransactionInterceptor } from 'src/utils/interceptors/utils.interceptors.transaction';
import { HttpModule } from '@nestjs/axios';
import { PaginationService } from 'src/utils/services/pagination.service';
import { CheckDBValidationPipe } from './pipes/check-db-validation.pipe';

@Global()
@Module({
  imports: [SequelizeModule.forFeature([User]), HttpModule],
  exports: [
    CheckDBValidationPipe,
    PasswordService,
    RandomService,
    PaginationService,
  ],
  controllers: [],
  providers: [
    CheckDBValidationPipe,
    PasswordService,
    RandomService,
    PaginationService,
    {
      provide: APP_INTERCEPTOR,
      useClass: TransactionInterceptor,
    },
  ],
})
export class UtilsModule {}
