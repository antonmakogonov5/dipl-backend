import { ArgumentMetadata, Inject, ValidationPipe } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectConnection } from '@nestjs/sequelize';
import { Sequelize } from 'sequelize-typescript';
import {
  TranslatableError,
  TranslatableValidationException,
} from '../exceptions/translatable-validation-exception';
import { QueryTypes, Transaction } from 'sequelize';
import { CheckDbMetadataKey } from '../decorators/check-db.decorator';
import { User } from '../../user/user.model';

type DBMetadata = {
  column: string;
  table: string;
  shouldExist: boolean;
  message: string;
  property: string;
};

export class CheckDBValidationPipe extends ValidationPipe {
  constructor(
    @Inject(REQUEST)
    private readonly request: Request & {
      transaction: Transaction;
      user: User;
      params: { id: number };
    },
    @InjectConnection() private readonly sequelize: Sequelize,
  ) {
    super({
      whitelist: true,
      transform: true,
      stopAtFirstError: true,
      skipMissingProperties: false,
    });
  }

  async validateDB(value: any, meta: DBMetadata[]) {
    const errors: TranslatableError[] = [];
    for (const item of meta) {
      const { column, table, shouldExist, property } = item;
      const [dbRecord]: { id: number }[] = await this.sequelize.query(
        `SELECT "id" FROM "${table}" WHERE "${column}" = :value`,
        {
          replacements: { value: value[property] },
          type: QueryTypes.SELECT,
          transaction: this.request.transaction,
        },
      );
      const method = this.request.method;
      if ((method === 'PUT' || method === 'PATCH') && shouldExist === false) {
        if (property === 'email' && this.request.user.id === dbRecord.id) {
          continue;
        } else if (Number(this.request.params.id) === dbRecord?.id) {
          continue;
        }
      }
      if (!!dbRecord !== shouldExist) {
        errors.push({ fieldName: property, translationKey: item.message });
      }
    }
    if (errors.length) throw new TranslatableValidationException(...errors);
  }

  async transform(value: any, metadata: ArgumentMetadata) {
    const meta: DBMetadata[] =
      Reflect.getMetadata(CheckDbMetadataKey, value) || [];
    if (!meta?.length) {
      return super.transform(value, metadata);
    }
    await this.validateDB(value, meta);
    return super.transform(value, metadata);
  }
}
