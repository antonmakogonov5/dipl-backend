import { ValidationError } from '@nestjs/common';

export function handleValidationError(
  validationError: ValidationError,
  responseErrors: any,
) {
  if (Object.keys(validationError.constraints).length) {
    responseErrors[validationError.property] = Object.values(
      validationError.constraints,
    );
  } else {
    responseErrors[validationError.property] = {} as any;

    validationError.children.forEach(
      (childValidationError: ValidationError) => {
        handleValidationError(
          childValidationError,
          responseErrors[validationError.property],
        );
      },
    );
  }
}
