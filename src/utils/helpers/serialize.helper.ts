import { ClassConstructor, plainToInstance } from 'class-transformer';

export function serialize<T>(target: any, outputClass: ClassConstructor<T>): T {
  return plainToInstance(outputClass, target, {
    excludeExtraneousValues: true,
    enableImplicitConversion: true,
  });
}
