import { CreatedAt, Column, DataType, UpdatedAt } from 'sequelize-typescript';

import { BaseModel } from 'src/utils/models/utils.models.base';

export class BaseModelWithTimestamps extends BaseModel {
  @CreatedAt
  @Column({
    type: DataType.DATE,
    allowNull: false,
    defaultValue: DataType.NOW,
  })
  createdAt: Date;

  @UpdatedAt
  @Column({
    type: DataType.DATE,
  })
  updatedAt: Date;
}
