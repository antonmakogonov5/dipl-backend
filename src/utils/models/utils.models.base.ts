import { Model, PrimaryKey, Column, DataType } from 'sequelize-typescript';

export class BaseModel extends Model {
  @PrimaryKey
  @Column({
    type: DataType.INTEGER,
    autoIncrement: true,
  })
  id: number;
}
