import { IsPage } from 'src/utils/decorators/is-page.decorator';
import { defaults } from 'src/utils/constants/defaults.constant';
import { IsLimit } from 'src/utils/decorators/is-limit.decorator';

export class GetListDto {
  @IsPage()
  page: number = defaults.page;

  @IsLimit()
  limit: number = defaults.limit;
}
