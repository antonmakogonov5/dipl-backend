import { ApiProperty } from '@nestjs/swagger';

import { IsIntIdentifier } from 'src/utils/decorators/utils.decorators.is-int-identifier';

export class IdentifierDto {
  @ApiProperty({
    minimum: 1,
    example: 1,
  })
  @IsIntIdentifier()
  id: number;
}
