export const validationConstraints = {
  page: {
    min: 1,
  },
  limit: {
    min: 1,
    max: 100,
  },
  id: {
    min: 1,
  },
  pagesCount: {
    min: 0,
  },
};
