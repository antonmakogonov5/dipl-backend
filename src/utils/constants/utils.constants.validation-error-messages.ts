export const validationErrorMessages = {
  id: {
    int: 'VALIDATION_ID_INT',
    min: 'VALIDATION_ID_MIN',
  },
  email: {
    format: 'VALIDATION_EMAIL_FORMAT',
    length: {
      min: 'VALIDATION_EMAIL_LENGTH_MIN',
      max: 'VALIDATION_EMAIL_LENGTH_MAX',
    },
  },
  password: {
    length: {
      min: 'VALIDATION_PASSWORD_LENGTH_MIN',
      max: 'VALIDATION_PASSWORD_LENGTH_MAX',
    },
    strength: 'VALIDATION_PASSWORD_STRENGTH',
    empty: 'VALIDATION_PASSWORD_EMPTY',
  },
  passwordRepeat: {
    match: 'VALIDATION_PASSWORD_REPEAT_MATCH',
    missing: 'VALIDATION_PASSWORD_REPEAT_MISSING',
  },
  name: {
    length: {
      min: 'VALIDATION_NAME_LENGTH_MIN',
      max: 'VALIDATION_NAME_LENGTH_MAX',
    },
  },
  telegram: {
    length: {
      min: 'VALIDATION_TELEGRAM_LENGTH_MIN',
      max: 'VALIDATION_TELEGRAM_LENGTH_MAX',
    },
  },
  accessToken: {
    empty: 'VALIDATION_ACCESS_TOKEN_EMPTY',
    invalid: 'VALIDATION_ACCESS_TOKEN_INVALID',
    expired: 'VALIDATION_ACCESS_TOKEN_EXPIRED',
  },
  refreshToken: {
    invalid: 'VALIDATION_REFRESH_TOKEN_INVALID',
    expired: 'VALIDATION_REFRESH_TOKEN_EXPIRED',
    length: {
      min: 'VALIDATION_REFRESH_TOKEN_LENGTH_MIN',
      max: 'VALIDATION_REFRESH_TOKEN_LENGTH_MAX',
    },
  },
  cardNumber: {
    format: 'VALIDATION_CARD_NUMBER_FORMAT',
  },
  active: {
    bool: 'VALIDATION_ACTIVE_BOOL',
  },
  limit: {
    int: 'VALIDATION_LIMIT_INT',
    min: 'VALIDATION_LIMIT_MIN',
    max: 'VALIDATION_LIMIT_MAX',
  },
  offset: {
    int: 'VALIDATION_OFFSET_INT',
    min: 'VALIDATION_OFFSET_MAX',
  },
  avatar: {
    type: 'VALIDATION_AVATAR_TYPE',
    size: 'VALIDATION_AVATAR_SIZE',
  },
  phone: {
    format: 'VALIDATION_PHONE_FORMAT',
  },
  birthDate: {
    format: 'VALIDATION_BIRTH_DATE_FORMAT',
  },
  currentPassword: {
    missing: 'VALIDATION_CURRENT_PASSWORD_MISSING',
    missmatch: 'VALIDATION_CURRENT_PASSWORD_MISSMATCH',
  },
  resetPasswordConfirmToken: {
    length: {
      min: 'VALIDATION_PASSWORD_RESET_CONFIRM_TOKEN_LENGTH_MIN',
      max: 'VALIDATION_PASSWORD_RESET_CONFIRM_TOKEN_LENGTH_MAX',
    },
    invalid: 'VALIDATION_PASSWORD_RESET_CONFIRM_TOKEN_INVALID',
    expired: 'VALIDATION_PASSWORD_RESET_CONFIRM_TOKEN_EXPIRED',
  },
  currency: 'VALIDATION_CURRENCY',
  paymentStatus: 'VALIDATION_PAYMENT_STATUS',
  paymentWay: 'VALIDATION_PAYMENT_WAY',
  dateFrom: {
    format: 'VALIDATION_DATE_FROM_FORMAT',
  },
  dateTo: {
    format: 'VALIDATION_DATE_TO_FORMAT',
  },
};
