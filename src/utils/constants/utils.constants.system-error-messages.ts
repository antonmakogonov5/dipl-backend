export const systemErrorMessages = {
  wrongAddressCurrency: 'WRONG_ADDRESS_CURRENCY',
  insufficientFunds: 'INSUFFICIENT_FUNDS',
  insufficientFundsFee: 'INSUFFICIENT_FUNDS_FEE',
  notWithdraw: 'NOT_WITHDRAW',
  notWaiting: 'NOT_WAITING',
  paydeskNotFound: 'PAYDESK_NOT_FOUND',
  walletNotFound: 'WALLET_NOT_FOUND',
};
