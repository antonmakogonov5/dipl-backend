export const defaultErrorMessages = {
  notFound: 'RESOURCE_NOT_FOUND',
  forbidden: 'ACCESS_FORBIDDEN',
  internalError: 'INTERNAL_ERROR',
};
