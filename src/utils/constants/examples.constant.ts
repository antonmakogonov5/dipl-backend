import dayjs from 'dayjs';

import { SortOrders } from 'src/utils/enums/utils.enums.sort-orders';

const now: Date = new Date();
const dateOutputFormat: string = 'YYYY-MM-DDTHH:mm:ss.SSS[Z]';

export const examples = {
  page: 1,
  limit: 10,
  id: 1,
  pagesCount: 10,
  hasMorePages: true,
  date: dayjs(now).format(dateOutputFormat),
  order: SortOrders.asc,
};