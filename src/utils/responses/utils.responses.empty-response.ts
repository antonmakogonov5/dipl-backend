import { ApiProperty } from '@nestjs/swagger';

export class EmptyResponse {
  @ApiProperty({
    description: 'Status of operation',
    example: true,
  })
  status: boolean = true;
}
