import { ApiProperty } from '@nestjs/swagger';

export class ForbiddenResponse {
  @ApiProperty({
    example: 'Доступ заборонено',
  })
  message: string;
}
