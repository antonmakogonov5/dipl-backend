import { ApiProperty } from '@nestjs/swagger';

import { defaultErrorMessages } from 'src/utils/constants/utils.constants.default-error-messages';

export class InternalServerErrorResponse {
  @ApiProperty({
    example: [defaultErrorMessages.internalError],
  })
  messages: string[];
}
