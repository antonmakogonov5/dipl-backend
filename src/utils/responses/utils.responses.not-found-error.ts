import { ApiProperty } from '@nestjs/swagger';

export class ResourceNotFoundErrorResponse {
  @ApiProperty({
    example: 'Ресурс не знайдено',
  })
  message: string[];
}
