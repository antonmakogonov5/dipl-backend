import path from 'path';

import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { SequelizeModule, SequelizeModuleOptions } from '@nestjs/sequelize';
import { JwtModule } from '@nestjs/jwt';
import { ServeStaticModule } from '@nestjs/serve-static';

import { validateConfig } from 'src/config/validator/config.validator';
import { loadConfig } from 'src/config/loader/config.loader';
import { ConfigNames } from 'src/config/enums/config.enums.config-names';
import { AuthConfig } from 'src/auth/auth.config';
import { UtilsModule } from 'src/utils/utils.module';
import { AuthModule } from 'src/auth/auth.module';
import { UserModule } from 'src/user/user.module';
import {
  AcceptLanguageResolver,
  HeaderResolver,
  I18nModule,
  QueryResolver,
} from 'nestjs-i18n';
import { SettingsModule } from '../setting/settings.module';
import {ProductModule} from "../product/product.module";
import {CartModule} from "../cart/cart.module";
import {OrderModule} from "../order/order.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      validate: validateConfig,
      load: [loadConfig],
    }),
    SequelizeModule.forRootAsync({
      inject: [ConfigService],
      useFactory(configService: ConfigService): SequelizeModuleOptions {
        const dbConfig: SequelizeModuleOptions =
          configService.get<SequelizeModuleOptions>(ConfigNames.db);

        return dbConfig;
      },
    }),
    JwtModule.registerAsync({
      global: true,
      inject: [ConfigService],
      async useFactory(configService: ConfigService) {
        const authConfig: AuthConfig = configService.get<AuthConfig>(
          ConfigNames.auth,
        );

        return authConfig.jwt;
      },
    }),
    ServeStaticModule.forRootAsync({
      useFactory: (configService: ConfigService) => {
        const appConfig = configService.get(ConfigNames.app);
        return [
          {
            rootPath: path.join(process.cwd(), `${appConfig.storage}/public`),
            serveRoot: '/public',
            serveStaticOptions: {
              index: false,
            },
          },
        ];
      },
      inject: [ConfigService],
    }),
    I18nModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        fallbackLanguage: configService.getOrThrow('FALLBACK_LANGUAGE'),
        loaderOptions: {
          path: path.join(__dirname, '../i18n'),
          watch: true,
        },
        typesOutputPath: path.join(
          __dirname,
          '../../src/generated/i18n.generated.ts',
        ),
      }),
      resolvers: [
        { use: QueryResolver, options: ['lang'] },
        AcceptLanguageResolver,
        new HeaderResolver(['x-lang']),
      ],
      inject: [ConfigService],
    }),
    UtilsModule,
    AuthModule,
    UserModule,
    SettingsModule,
    ProductModule,
    CartModule,
    OrderModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
