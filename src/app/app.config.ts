export interface AppConfig {
  port: number;
  url: string;
  storage: string;
}
