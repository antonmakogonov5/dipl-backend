import { Table, Model, PrimaryKey, Column, DataType, ForeignKey, BelongsTo, CreatedAt } from 'sequelize-typescript';

import { User } from 'src/user/user.model';

@Table({
  tableName: 'resetPasswordTokens',
  timestamps: true,
  updatedAt: false,
})
export class ResetPasswordToken extends Model {
  @PrimaryKey
  @Column({
    type: DataType.INTEGER,
    autoIncrement: true,
  })
  id: number;

  @ForeignKey(() => User)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
    unique: true,
  })
  userId: number;

  @BelongsTo(() => User)
  user: User;

  @CreatedAt
  @Column({
    type: DataType.DATE,
    allowNull: false,
    defaultValue: DataType.NOW,
  })
  createdAt: Date;

  @Column({
    type: DataType.DATE,
  })
  expiredAt: Date;
}
