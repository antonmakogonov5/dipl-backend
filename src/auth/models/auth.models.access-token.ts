import {
  Table,
  Model,
  PrimaryKey,
  Column,
  DataType,
  ForeignKey,
  BelongsTo,
  CreatedAt,
} from 'sequelize-typescript';

import { User } from 'src/user/user.model';
import { RefreshToken } from 'src/auth/models/auth.models.refresh-token';

@Table({
  tableName: 'accessTokens',
  timestamps: true,
  updatedAt: false,
})
export class AccessToken extends Model {
  @PrimaryKey
  @Column({
    type: DataType.INTEGER,
    autoIncrement: true,
  })
  id: number;

  @ForeignKey(() => User)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  userId: number;

  @BelongsTo(() => User)
  user: User;

  @ForeignKey(() => RefreshToken)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  refreshTokenId: number;

  @BelongsTo(() => RefreshToken)
  refreshToken: RefreshToken;

  @CreatedAt
  @Column({
    type: DataType.DATE,
    allowNull: false,
    defaultValue: DataType.NOW,
  })
  createdAt: Date;

  @Column({
    type: DataType.DATE,
  })
  expiredAt?: Date;
}
