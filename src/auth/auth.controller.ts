import {Controller, Post, Body, Get, UseInterceptors, Put, Param, Query} from '@nestjs/common';
import { Transaction } from 'sequelize';
import {
  ApiTags,
  ApiInternalServerErrorResponse,
  ApiCreatedResponse,
  ApiUnprocessableEntityResponse,
  ApiOkResponse,
  ApiUnauthorizedResponse,
  ApiBearerAuth,
} from '@nestjs/swagger';

import { AuthService } from 'src/auth/auth.service';
import { RequestTransaction } from 'src/utils/decorators/utils.decorators.request-transaction';
import { SignInOutput } from 'src/auth/outputs/auth.outputs.sign-in';
import { SignInBodyDto } from 'src/auth/dto/auth.dto.sign-in.body';
import { JwtInterceptor } from 'src/auth/interceptors/auth.interceptors.jwt';
import { CurrentAccessToken } from 'src/auth/decorators/auth.decorators.current-access-token';
import { AccessToken } from 'src/auth/models/auth.models.access-token';
import { ReclaimAuthTokensBodyDto } from 'src/auth/dto/auth.dto.reclaim-auth-tokens.body';
import { InternalServerErrorResponse } from 'src/utils/responses/utils.responses.internal-error';
import { UnauthorizedErrorResponse } from 'src/auth/responses/auth.responses.unauthorized-error';
import { EmptyResponse } from 'src/utils/responses/utils.responses.empty-response';
import { SignInValidationErrorResponse } from 'src/auth/responses/auth.responses.validation-error.sign-in';
import { ReclaimAuthTokensValidationError } from 'src/auth/responses/auth.responses.validation-error.reclaim-auth-tokens';
import {GetOneSettingDto} from "../setting/dto/get-one-setting.dto";
import {AuthDtoGetNonce} from "./dto/auth.dto.get-nonce";

@Controller('auth')
@ApiTags('auth')
@ApiInternalServerErrorResponse({
  type: InternalServerErrorResponse,
})
export class AuthController {
  constructor(private authService: AuthService) {}


  @Get('sign/in')
  async getNonce(
      @Query() params: AuthDtoGetNonce,
  ): Promise<{nonce: string}> {
    return this.authService.getNonce(params);
  }


  @Post('sign/in')
  async signIn(
    @RequestTransaction() transaction: Transaction,
    @Body() body: SignInBodyDto,
  ): Promise<SignInOutput> {
    return this.authService.signIn({
      data: {
        ...body,
      },
      transaction,
    });
  }

  @Get('sign/out')
  @ApiBearerAuth()
  @ApiOkResponse({
    description: 'Sign out was successfull',
    type: EmptyResponse,
  })
  @ApiUnauthorizedResponse({
    description: 'Access token validation failed - empty, invalid or expired',
    type: UnauthorizedErrorResponse,
  })
  @UseInterceptors(JwtInterceptor)
  async signOut(
    @RequestTransaction() transaction: Transaction,
    @CurrentAccessToken() accessToken: AccessToken,
  ): Promise<void> {
    await this.authService.signOut({
      accessToken,
      transaction,
    });
  }

  @Post('tokens/reclaim')
  @ApiCreatedResponse({
    description: 'Access and refresh tokens pair',
    type: SignInOutput,
  })
  @ApiUnprocessableEntityResponse({
    description:
      'Input validation failed - refresh token incorrect length, invalid or expired',
    type: ReclaimAuthTokensValidationError,
  })
  @ApiUnauthorizedResponse({
    description: 'Refresh token validation failed - empty, invalid or expired',
    type: UnauthorizedErrorResponse,
  })
  async reclaimAuthTokens(
    @RequestTransaction() transaction: Transaction,
    @Body() body: ReclaimAuthTokensBodyDto,
  ): Promise<SignInOutput> {
    return this.authService.reclaimAuthTokens({
      ...body,
      transaction,
    });
  }
}
