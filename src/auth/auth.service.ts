import {
  Injectable,
  ForbiddenException,
  UnauthorizedException,
  UnprocessableEntityException,
} from '@nestjs/common';
import {InjectModel} from '@nestjs/sequelize';
import {ConfigService} from '@nestjs/config';
import {JwtService} from '@nestjs/jwt';
import {Transaction, Op} from 'sequelize';

import {User} from 'src/user/user.model';
import {RefreshToken} from 'src/auth/models/auth.models.refresh-token';
import {AccessToken} from 'src/auth/models/auth.models.access-token';
import {ResetPasswordToken} from 'src/auth/models/auth.models.reset-password-token';
import {PasswordService} from 'src/utils/services/utils.services.password';
import {
  SignInOutput,
  AuthTokens,
} from 'src/auth/outputs/auth.outputs.sign-in';
import {Roles} from 'src/auth/enums/auth.enums.roles';
import {userErrorMessages} from 'src/user/constants/user.constants.error-messages';
import {SignInServiceInput} from 'src/auth/inputs/auth.inputs.sign-in';
import {authErrorMessages} from 'src/auth/constants/auth.constants.error-messages';
import {AuthConfig} from 'src/auth/auth.config';
import {ConfigNames} from 'src/config/enums/config.enums.config-names';
import {SignOutInput} from 'src/auth/inputs/auth.inputs.sign-out';
import {ReclaimAuthTokensInput} from 'src/auth/inputs/auth.inputs.reclaim-auth-tokens';
import {validationErrorMessages} from 'src/utils/constants/utils.constants.validation-error-messages';
import {AuthDtoGetNonce} from "./dto/auth.dto.get-nonce";
import {createId} from "@paralleldrive/cuid2";
import {recoverPersonalSignature} from "@metamask/eth-sig-util";


@Injectable()
export class AuthService {
  private authConfig: AuthConfig;

  constructor(
    @InjectModel(User)
    private userModel: typeof User,
    @InjectModel(RefreshToken)
    private refreshTokenModel: typeof RefreshToken,
    @InjectModel(AccessToken)
    private accessTokenModel: typeof AccessToken,
    @InjectModel(ResetPasswordToken)
    private resetPasswordTokenModel: typeof ResetPasswordToken,
    private configService: ConfigService,
    private jwtService: JwtService,
  ) {
    this.authConfig = configService.get<AuthConfig>(ConfigNames.auth);
  }

  async getNonce(params: AuthDtoGetNonce): Promise<{nonce: string}> {
    const user: User = await this.userModel.findOne({
      where: {
        address: params.address,
      }
    });

    if (!user) {
      const newUser = await this.userModel.create({
        address: params.address,
        role: Roles.user,
        nonce: createId()
      })
      return {nonce: newUser.nonce};
    }

    return {nonce: user.nonce};
  }

  async signIn(input: SignInServiceInput): Promise<SignInOutput> {
    const user: User = await this.userModel.findOne({
      where: {
        address: input.data.address
      },
      transaction: input.transaction,
    });

    if (!user) {
      throw new UnprocessableEntityException(
        authErrorMessages.signIn.addressNotMatch,
      );
    }
    const message = 'I agree to the terms of use and confirm my identity to VineApp.';
    const fullMessage = `${message}\nNonce: ${user.nonce}`;

    const recoveredAddress = recoverPersonalSignature({data: fullMessage, signature: input.data.signature});

    if (recoveredAddress !== input.data.address) {
      throw new UnprocessableEntityException(
        authErrorMessages.signIn.addressNotMatch,
      );
    }
    const tokens = await this.createAuthTokens(user, input.transaction);
    await user
      .update(
        {
          nonce: createId(),
        },
        {
          transaction: input.transaction,
        },
      );


    return {
      tokens
    };
  }

  async signOut(input: SignOutInput): Promise<void> {
    const now: Date = new Date();
    const user: User = input.accessToken.user;

    await input.accessToken.refreshToken.destroy({
      transaction: input.transaction,
    });
    await this.refreshTokenModel.destroy({
      where: {
        userId: user.id,
        expiredAt: {
          [Op.lt]: now,
        },
      },
      transaction: input.transaction,
    });
    await this.accessTokenModel.destroy({
      where: {
        userId: user.id,
        expiredAt: {
          [Op.lt]: now,
        },
      },
      transaction: input.transaction,
    });
  }

  async reclaimAuthTokens(
    input: ReclaimAuthTokensInput,
  ): Promise<SignInOutput> {
    const payload: any = this.jwtService.decode(input.refreshToken);

    if (
      !payload ||
      isNaN(parseInt(payload.id)) ||
      isNaN(parseInt(payload.exp))
    ) {
      throw new UnauthorizedException(
        validationErrorMessages.refreshToken.invalid,
      );
    }

    if (payload.exp * 1000 < Date.now()) {
      await this.refreshTokenModel.destroy({
        where: {
          id: payload.id,
        },
      });

      throw new UnauthorizedException(
        validationErrorMessages.refreshToken.expired,
      );
    }

    const refreshToken: RefreshToken = await this.refreshTokenModel.findByPk(
      payload.id,
      {
        transaction: input.transaction,
        include: [
          {
            model: User,
          },
        ],
      },
    );

    if (!refreshToken) {
      throw new UnauthorizedException(
        validationErrorMessages.refreshToken.invalid,
      );
    }

    const user: User = refreshToken.user;

    await refreshToken.destroy({
      transaction: input.transaction,
    });

    const tokens = await this.createAuthTokens(user, input.transaction);

    return {
      tokens
    };
  }


  private async createAuthTokens(
    user: User,
    transaction?: Transaction,
  ): Promise<AuthTokens> {
    const refreshToken: RefreshToken = await user.$create(
      'refreshToken',
      {},
      {
        transaction: transaction,
      },
    );
    const accessToken: AccessToken = await user.$create(
      'accessToken',
      {
        refreshTokenId: refreshToken.id,
      },
      {
        transaction: transaction,
      },
    );
    const tokenValues: AuthTokens = {
      accessToken: await this.jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: this.authConfig.ttl.access,
        },
      ),
      refreshToken: await this.jwtService.signAsync(
        {
          id: refreshToken.id,
        },
        {
          expiresIn: this.authConfig.ttl.refresh,
        },
      ),
    };
    const tokenPayloads: any = {
      accessToken: this.jwtService.decode(tokenValues.accessToken),
      refreshToken: this.jwtService.decode(tokenValues.refreshToken),
    };

    await Promise.all([
      refreshToken.update(
        {
          expiredAt: new Date(tokenPayloads.refreshToken.exp * 1000),
        },
        {
          transaction,
        },
      ),
      accessToken.update(
        {
          expiredAt: new Date(tokenPayloads.accessToken.exp * 1000),
        },
        {
          transaction,
        },
      ),
    ]);

    return tokenValues;
  }
}
