export const UserRoles = {
  trader: 'trader',
  merchant: 'merchant',
} as const;
export type UserRoles = (typeof UserRoles)[keyof typeof UserRoles];
