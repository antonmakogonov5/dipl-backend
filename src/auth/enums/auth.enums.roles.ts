export const Roles = {
  admin: 'admin',
  user: 'user',
} as const;

export type Roles = (typeof Roles)[keyof typeof Roles];
