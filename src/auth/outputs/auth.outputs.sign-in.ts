import { ApiProperty } from '@nestjs/swagger';

export class SignInOutput {
  @ApiProperty({
    example: {
      accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTIsImlhdCI6MTY5MzY1MzQ4NywiZXhwIjoxNjkzNzM5ODg3fQ.1djiX5MnRj6ThHYva7UGsGW3dVQn5R8I5WYkKcJ4gx4',
      refreshToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTIsImlhdCI6MTY5MzY1MzQ4NywiZXhwIjoxNjk2MjQ1NDg3fQ.TZDtMUFMkXNriTrYVaiX-n3B5pD2wvZJfo-eVmACtfk',
    },
  })
  tokens: AuthTokens;
}

export interface AuthTokens {
  accessToken: string;
  refreshToken: string;
}
