import { JwtModuleOptions } from '@nestjs/jwt';

export interface AuthConfig {
  jwt: JwtModuleOptions;
  ttl: {
    access: string;
    refresh: string;
    resetPassword: string;
  };
}
