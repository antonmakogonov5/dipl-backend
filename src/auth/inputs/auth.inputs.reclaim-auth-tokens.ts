import { Transaction } from 'sequelize';

export interface ReclaimAuthTokensInput {
  refreshToken: string;
  transaction?: Transaction;
}
