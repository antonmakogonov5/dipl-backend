import { Transaction } from 'sequelize';

import { AccessToken } from 'src/auth/models/auth.models.access-token';

export interface SignOutInput {
  accessToken: AccessToken;
  transaction?: Transaction;
}
