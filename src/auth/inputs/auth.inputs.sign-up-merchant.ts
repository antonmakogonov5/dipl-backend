import { Transaction } from 'sequelize';

export interface SignUpMerchantInput {
  data: {
    email: string;
    name: string;
    telegram: string;
    url: string;
  };
  transaction?: Transaction;
}
