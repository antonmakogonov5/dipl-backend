import { TransactionableInput } from 'src/utils/inputs/transactionable.input';

import { User } from 'src/user/user.model';

export interface ChangePasswordInput extends TransactionableInput {
  data: {
    password: string;
    newPassword: string;
  };
  user: User;
}
