import { Transaction } from 'sequelize';

export interface SignInServiceInput {
  data: {
    address: string;
    signature: string;
  };
  transaction?: Transaction;
}
