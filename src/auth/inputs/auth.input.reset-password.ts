import { Transaction } from 'sequelize';

export interface ResetPasswordInput {
  data: {
    email: string;
  };
  transaction?: Transaction;
}
