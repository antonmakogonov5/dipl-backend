import { Transaction } from 'sequelize';

export interface ResetPasswordConfirmInput {
  data: {
    token: string;
    newPassword: string;
  };
  transaction?: Transaction;
}
