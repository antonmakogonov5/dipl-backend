import { Transaction } from 'sequelize';

export interface SignUpTraderServiceInput {
  data: {
    email: string;
    password: string;
    name: string;
    telegram: string;
  };
  transaction?: Transaction;
}
