export const authErrorMessages = {
  signIn: {
    addressNotMatch: 'ADDRESS_AND_SIGNATURE_MISMATCH',
  },
  restorePassword: {
    requestExists: 'AUTH_RESET_PASSWORD_REQUEST_EXISTS',
  },
};
