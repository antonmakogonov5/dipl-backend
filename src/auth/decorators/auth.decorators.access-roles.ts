import { SetMetadata } from '@nestjs/common';

import { Roles } from 'src/auth/enums/auth.enums.roles';

export const AccessRoles = (...roles: Roles[]) => {
  return SetMetadata('roles', roles);
};
