import { createParamDecorator, ExecutionContext } from '@nestjs/common';

import { AccessToken } from 'src/auth/models/auth.models.access-token';

export const CurrentAccessToken = createParamDecorator(
  (data: unknown, context: ExecutionContext): AccessToken => {
    if (context.getType() === 'http') {
      const httpContext = context.switchToHttp();
      const request = httpContext.getRequest();

      return request.accessToken as AccessToken;
    }
  },
);
