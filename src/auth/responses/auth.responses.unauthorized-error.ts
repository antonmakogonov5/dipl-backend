import { ApiProperty } from '@nestjs/swagger';

import { validationErrorMessages } from 'src/utils/constants/utils.constants.validation-error-messages';

export class UnauthorizedErrorResponse {
  @ApiProperty({
    example: [
      validationErrorMessages.accessToken.empty,
      validationErrorMessages.accessToken.invalid,
      validationErrorMessages.accessToken.expired,
    ],
  })
  messages: string[];
}
