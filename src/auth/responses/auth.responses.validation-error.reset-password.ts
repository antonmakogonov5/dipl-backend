import { ApiProperty } from '@nestjs/swagger';

import { authErrorMessages } from 'src/auth/constants/auth.constants.error-messages';
import { userErrorMessages } from 'src/user/constants/user.constants.error-messages';

export class ResetPasswordValidationErrorResponse {
  @ApiProperty({
    example: [
      authErrorMessages.restorePassword.requestExists,
      userErrorMessages.confirmation.notConfirmed,
    ],
  })
  messages: string[];
  @ApiProperty({
    example: {
      email: ['Мінімальна довжина: 1'],
    },
  })
  errors: any;
}
