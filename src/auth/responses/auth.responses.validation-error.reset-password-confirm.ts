import { ApiProperty } from '@nestjs/swagger';

export class ResetPasswordConfirmValidationErrorResponse {
  @ApiProperty({
    example: {
      token: ['Максимальна довжина: 255'],
      newPassword: ['Пароль повинен містити принаймні 1 цифру'],
      newPasswordRepeat: ['Паролі мають співпадати'],
    },
  })
  errors: any;
}
