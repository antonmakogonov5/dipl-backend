import { ApiProperty } from '@nestjs/swagger';

export class SignUpTraderValidationErrorResponse {
  @ApiProperty({
    example: {
      email: ['Користувач з такою електронною поштою вже існує'],
      password: ['Пароль повинен містити принаймні 1 цифру'],
      passwordRepeat: ['Паролі мають співпадати'],
      name: ['Максимальна довжина: 255'],
      telegram: ['Максимальна довжина: 255'],
    },
  })
  errors: any;
}
