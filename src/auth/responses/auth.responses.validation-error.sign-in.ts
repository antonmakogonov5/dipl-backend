import { ApiProperty } from '@nestjs/swagger';

export class SignInValidationErrorResponse {
  @ApiProperty({
    example: {
      email: ['Користувач з такою електронною поштою вже існує'],
      password: ['Пароль повинен містити принаймні 1 цифру'],
    },
  })
  errors: any;
}
