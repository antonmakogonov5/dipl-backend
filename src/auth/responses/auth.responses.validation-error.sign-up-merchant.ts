import { ApiProperty } from '@nestjs/swagger';

export class SignUpMerchantValidationErrorResponse {
  @ApiProperty({
    description: 'Input validation failed',
    example: {
      email: [
        'Invalid email format',
      ],
      name: [
        'Minimum length: 1',
        'Maximum length: 255',
      ],
      telegram: [
        'Minimum length: 1',
        'Maximum length: 255',
      ],
      url: [
        'Minimum length: 1',
        'Maximum length: 255',
      ],
    },
  })
  errors: any;
}
