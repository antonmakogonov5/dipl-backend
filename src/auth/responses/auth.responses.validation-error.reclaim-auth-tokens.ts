import { ApiProperty } from '@nestjs/swagger';

export class ReclaimAuthTokensValidationError {
  @ApiProperty({
    example: {
      refreshToken: ['Максимальна довжина: 255'],
    },
  })
  errors: any;
}
