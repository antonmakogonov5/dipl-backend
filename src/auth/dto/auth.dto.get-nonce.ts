import {ApiProperty} from "@nestjs/swagger";
import {IsString} from "class-validator";

export class AuthDtoGetNonce {
    @ApiProperty({
        example: 'key',
    })
    @IsString()
    address: string;
}