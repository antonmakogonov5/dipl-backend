import { MinLength, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { i18nValidationMessage } from 'nestjs-i18n';

import { type I18nTranslations } from '../../generated/i18n.generated';

export class ReclaimAuthTokensBodyDto {
  @MinLength(1, {
    message: i18nValidationMessage<I18nTranslations>('validation.MIN_LENGTH'),
  })
  @MaxLength(255, {
    message: i18nValidationMessage<I18nTranslations>('validation.MAX_LENGTH'),
  })
  @ApiProperty({
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTIsImlhdCI6MTY5MzY1MzQ4NywiZXhwIjoxNjk2MjQ1NDg3fQ.TZDtMUFMkXNriTrYVaiX-n3B5pD2wvZJfo-eVmACtfk',
    minLength: 1,
    maxLength: 255,
  })
  refreshToken: string;
}
