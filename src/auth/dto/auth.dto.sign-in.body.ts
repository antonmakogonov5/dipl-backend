import {IsEmail, MinLength, MaxLength, IsString} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

import { i18nValidationMessage } from 'nestjs-i18n';
import { type I18nTranslations } from '../../generated/i18n.generated';

export class SignInBodyDto {
  @IsString()
  @ApiProperty({
    example: '0xDd0aAf9FA038015825eaC50D431416C133558eC2',
  })
  address: string;

  @ApiProperty({
    example: '0x98cfc800c6c9e763461d00cb16f4dc077349779386134db38fcdc5b05599261d499f6756ff119323b53cddd75e00084fd89964f91b59f702150dbc21ef3d43e11c',
  })
  @IsString()
  signature: string;
}
