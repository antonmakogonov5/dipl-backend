import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import { User } from 'src/user/user.model';
import { RefreshToken } from 'src/auth/models/auth.models.refresh-token';
import { AccessToken } from 'src/auth/models/auth.models.access-token';
import { ResetPasswordToken } from 'src/auth/models/auth.models.reset-password-token';
import { AuthController } from 'src/auth/auth.controller';
import { AuthService } from 'src/auth/auth.service';

@Module({
  imports: [
    SequelizeModule.forFeature([
      User,
      RefreshToken,
      AccessToken,
      ResetPasswordToken,
    ]),
  ],
  exports: [],
  controllers: [AuthController],
  providers: [AuthService],
})
export class AuthModule {}
