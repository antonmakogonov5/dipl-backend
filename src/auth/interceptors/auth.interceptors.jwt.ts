import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/sequelize';
import { Observable } from 'rxjs';
import { Transaction } from 'sequelize';

import { AccessToken } from 'src/auth/models/auth.models.access-token';
import { validationErrorMessages } from 'src/utils/constants/utils.constants.validation-error-messages';
import { User } from 'src/user/user.model';
import { RefreshToken } from 'src/auth/models/auth.models.refresh-token';

@Injectable()
export class JwtInterceptor implements NestInterceptor {
  constructor(
    private jwtService: JwtService,
    @InjectModel(AccessToken)
    private accessTokenModel: typeof AccessToken,
  ) {}

  async intercept(
    context: ExecutionContext,
    next: CallHandler<any>,
  ): Promise<Observable<any>> {
    let accessTokenValue: string;
    let transaction: Transaction;

    if (context.getType() === 'http') {
      const httpContext = context.switchToHttp();
      const request = httpContext.getRequest();
      const authHeader: string = request.get('Authorization') + '';

      accessTokenValue = authHeader.split(' ')[1];
      transaction = request.transaction;
    }

    if (!accessTokenValue) {
      throw new UnauthorizedException(
        validationErrorMessages.accessToken.empty,
      );
    }

    const payload: any = this.jwtService.decode(accessTokenValue);

    if (
      !payload ||
      isNaN(parseInt(payload.id)) ||
      isNaN(parseInt(payload.exp))
    ) {
      throw new UnauthorizedException(
        validationErrorMessages.accessToken.invalid,
      );
    }

    const accessToken: AccessToken = await this.accessTokenModel.findByPk(
      payload.id,
      {
        transaction,
        include: [
          {
            model: User,
          },
          {
            model: RefreshToken,
          },
        ],
      },
    );

    if (!accessToken) {
      throw new UnauthorizedException(
        validationErrorMessages.accessToken.invalid,
      );
    }

    if (payload.exp * 1000 < Date.now()) {
      await accessToken.refreshToken.destroy();

      throw new UnauthorizedException(
        validationErrorMessages.accessToken.expired,
      );
    }

    if (context.getType() === 'http') {
      const httpContext = context.switchToHttp();
      const request = httpContext.getRequest();

      request.accessToken = accessToken;
      request.user = accessToken.user;
    }

    return next.handle();
  }
}
