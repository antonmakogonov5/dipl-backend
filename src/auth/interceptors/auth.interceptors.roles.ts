import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  ForbiddenException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';

import { Roles } from 'src/auth/enums/auth.enums.roles';
import { User } from 'src/user/user.model';
import { defaultErrorMessages } from 'src/utils/constants/utils.constants.default-error-messages';

@Injectable()
export class RolesInterceptor implements NestInterceptor {
  constructor(private reflector: Reflector) {}

  async intercept(
    context: ExecutionContext,
    next: CallHandler<any>,
  ): Promise<Observable<any>> {
    const roles: Roles[] = this.reflector.getAllAndOverride<Roles[]>('roles', [
      context.getHandler(),
      context.getClass(),
    ]);

    let user: User;

    if (context.getType() === 'http') {
      const httpContext = context.switchToHttp();
      const request = httpContext.getRequest();

      user = request.user;
    }

    if (!roles.includes(user?.role)) {
      throw new ForbiddenException(defaultErrorMessages.forbidden);
    }

    return next.handle();
  }
}
