import {
  Controller,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiTags,
  ApiInternalServerErrorResponse,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import { UserService } from 'src/user/user.service';
import { JwtInterceptor } from 'src/auth/interceptors/auth.interceptors.jwt';
import { InternalServerErrorResponse } from 'src/utils/responses/utils.responses.internal-error';
import { UnauthorizedErrorResponse } from 'src/auth/responses/auth.responses.unauthorized-error';

@Controller('users')
@ApiTags('users')
@ApiUnauthorizedResponse({
  description: 'Access token validation failed',
  type: UnauthorizedErrorResponse,
})
@ApiInternalServerErrorResponse({
  description: 'Internal error',
  type: InternalServerErrorResponse,
})
@UseInterceptors(JwtInterceptor)
export class UserController {
  constructor(private userService: UserService) {}

}
