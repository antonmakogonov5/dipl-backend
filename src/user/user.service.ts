import {
  Injectable,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { PasswordService } from 'src/utils/services/utils.services.password';
import { RandomService } from 'src/utils/services/utils.services.random';
import { User } from 'src/user/user.model';


@Injectable()
export class UserService {
  constructor(
    private passwordService: PasswordService,
    private randomService: RandomService,
    @InjectModel(User)
    private userModel: typeof User,
  ) {}

}
