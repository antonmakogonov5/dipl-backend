export const userNames = {
  search: {
    email: 'search[email]',
    name: 'search[name]',
    telegram: 'search[telegram]',
  },
};
