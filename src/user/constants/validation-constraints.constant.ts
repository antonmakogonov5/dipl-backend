export const userValidationConstraints = {
  name: {
    length: {
      min: 1,
      max: 255,
    },
  },
  telegram: {
    length: {
      min: 1,
      max: 255,
    },
  },
};
