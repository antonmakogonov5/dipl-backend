export const userDescriptions = {
  tradeActive: 'Trading status',
  search: 'Search options',
  email: 'User\'s email',
  name: 'User\'s name',
  telegram: 'User\'s telegram',
};
