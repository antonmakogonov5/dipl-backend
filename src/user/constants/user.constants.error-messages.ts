export const userErrorMessages = {
  existence: {
    exists: 'USER_EXISTS',
    notExists: 'USER_NOT_FOUND',
  },
  confirmation: {
    notConfirmed: 'USER_NOT_CONFIRMED',
    confirmed: 'USER_CONFIRMED',
  },
};
