import { GetUsersListSortFields } from 'src/user/enums/user.enums.get-list-sort-fields';

export const userExamples = {
  tradeActive: true,
  sort: {
    field: GetUsersListSortFields.name,
  },
  email: 'name@mail.com',
  name: 'name',
  telegram: 'telegram',
};
