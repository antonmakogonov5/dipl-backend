import { ApiProperty } from '@nestjs/swagger';

import { userErrorMessages } from 'src/user/constants/user.constants.error-messages';

export class UserNotConfirmedErrorResponse {
  @ApiProperty({
    example: [userErrorMessages.confirmation.notConfirmed],
  })
  messages: string[];
}
