import { ApiProperty } from '@nestjs/swagger';

export class EditUserProfileValidationErrorResponse {
  @ApiProperty({
    example: {
      email: ['Користувач з такою електронною поштою вже існує'],
      password: ['Пароль повинен містити принаймні 1 цифру'],
      passwordRepeat: ['Паролі мають співпадати'],
      name: ['Максимальна довжина: 255'],
      telegram: ['Максимальна довжина: 255'],
      birthDate: ['Поле повинно бути датою'],
      phone: ['Невірний формат номеру телефону'],
      currentPassword: ['Поле не може бути порожнім'],
      newPassword: ['Пароль повинен містити принаймні 1 цифру'],
      newPasswordRepeat: ['Паролі мають співпадати'],
    },
  })
  errors: any;
}
