import { ApiProperty } from '@nestjs/swagger';

export class UploadAvatarFileTooLargeError {
  @ApiProperty({
    example: {
      avatar: ['Файл повинен бути зображенням (png або jpeg)'],
    },
  })
  errors: [];
}
