import { ApiProperty } from '@nestjs/swagger';

export class UploadAvatarInvalidFileTypeError {
  @ApiProperty({
    example: {
      avatar: ['Файл повинен бути зображенням (png або jpeg)'],
    },
  })
  errors: [];
}
