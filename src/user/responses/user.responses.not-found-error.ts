import { ApiProperty } from '@nestjs/swagger';

import { userErrorMessages } from 'src/user/constants/user.constants.error-messages';

export class UserNotFoundErrorResponse {
  @ApiProperty({
    example: [userErrorMessages.existence.notExists],
  })
  messages: string[];
}
