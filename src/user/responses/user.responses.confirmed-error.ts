import { ApiProperty } from '@nestjs/swagger';

import { userErrorMessages } from 'src/user/constants/user.constants.error-messages';

export class UserConfirmedErrorResponse {
  @ApiProperty({
    example: [userErrorMessages.confirmation.confirmed],
  })
  messages: string[];
}
