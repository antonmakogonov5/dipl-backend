export enum GetUserStatsSortFields {
  confirmed = 'confirmed',
  name = 'name',
  paydeskCount = 'paydeskCount',
}
