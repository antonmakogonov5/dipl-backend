import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import { User } from 'src/user/user.model';
import { AccessToken } from 'src/auth/models/auth.models.access-token';
import { UserController } from 'src/user/user.controller';
import { UserService } from 'src/user/user.service';

@Module({
  imports: [
    SequelizeModule.forFeature([User, AccessToken]),
  ],
  exports: [],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
