import {
  Injectable,
  Scope,
  PipeTransform,
  Inject,
  ArgumentMetadata,
  NotFoundException,
} from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/sequelize';
import { Transaction } from 'sequelize';

import { User } from 'src/user/user.model';
import { userErrorMessages } from 'src/user/constants/user.constants.error-messages';

@Injectable({
  scope: Scope.REQUEST,
})
export class GetUserByIdPipe implements PipeTransform {
  constructor(
    @Inject(REQUEST)
    private request: any,

    @InjectModel(User)
    private userModel: typeof User,
  ) {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async transform(id: number, metadata: ArgumentMetadata): Promise<User> {
    const transaction: Transaction = this.request.transaction;
    const user: User = await this.userModel.findByPk(id, {
      transaction,
    });

    if (!user) {
      throw new NotFoundException(userErrorMessages.existence.notExists);
    }

    return user;
  }
}
