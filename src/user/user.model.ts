import {
  Table,
  Model,
  PrimaryKey,
  Column,
  DataType,
  HasMany,
  CreatedAt,
  UpdatedAt,
  HasOne,
} from 'sequelize-typescript';
import { ClassConstructor, plainToInstance } from 'class-transformer';

import { Roles } from 'src/auth/enums/auth.enums.roles';
import { RefreshToken } from 'src/auth/models/auth.models.refresh-token';
import { AccessToken } from 'src/auth/models/auth.models.access-token';
import { ResetPasswordToken } from 'src/auth/models/auth.models.reset-password-token';

@Table({
  tableName: 'users',
  timestamps: true,
})
export class User extends Model {
  @PrimaryKey
  @Column({
    type: DataType.INTEGER,
    autoIncrement: true,
  })
  id: number;

  @Column({
    type: DataType.STRING(255),
    allowNull: false,
    unique: true,
  })
  address: string;

  @Column({
    type: DataType.STRING(255),
    allowNull: false,
    unique: true,
  })
  nonce: string;

  @Column({
    type: DataType.STRING(255),
    allowNull: false,
    field: 'roleName',
  })
  role: Roles;

  @HasMany(() => RefreshToken)
  refreshTokens: RefreshToken[];

  @HasMany(() => AccessToken)
  accessTokens: AccessToken[];

  @HasOne(() => ResetPasswordToken)
  resetPasswordToken: ResetPasswordToken;

  @CreatedAt
  @Column({
    type: DataType.DATE,
    allowNull: false,
    defaultValue: DataType.NOW,
  })
  createdAt: Date;

  @UpdatedAt
  @Column({
    type: DataType.DATE,
  })
  updatedAt: Date;
}
