export class ProductCartDto {
  id: number;
  name: string;
  price: number;
  image: string;
  blockchainId: number;
}