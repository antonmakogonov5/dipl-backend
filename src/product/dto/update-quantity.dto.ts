import {ApiProperty} from "@nestjs/swagger";
import {IsDefined, IsNumber} from "class-validator";
import {Transform} from "class-transformer";

export class UpdateQuantityDto {
    @ApiProperty({
        minimum: 0,
        example: 0,
    })
    @IsNumber()
    @IsDefined()
    @Transform(({ value }) => parseInt(value))
    quantity: number;
}