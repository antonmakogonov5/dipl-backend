import {GetListDto} from "../../utils/dto/get-list.dto";
import {ApiProperty} from "@nestjs/swagger";
import {IsOptional, MaxLength, MinLength} from "class-validator";
import {i18nValidationMessage} from "nestjs-i18n";
import {I18nTranslations} from "../../generated/i18n.generated";

export class GetProductsListQueryDto extends GetListDto {
  @ApiProperty({
    required: false,
  })
  @MaxLength(255, {
    message: i18nValidationMessage<I18nTranslations>('validation.MAX_LENGTH'),
  })
  @MinLength(1, {
    message: i18nValidationMessage<I18nTranslations>('validation.MIN_LENGTH'),
  })
  @IsOptional()
  search?: string;
}