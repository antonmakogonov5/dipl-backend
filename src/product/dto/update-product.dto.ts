import {IsIntIdentifier} from "../../utils/decorators/utils.decorators.is-int-identifier";
import {ApiProperty} from "@nestjs/swagger";
import {IsDefined, IsNumber, IsString} from "class-validator";
import {Transform} from "class-transformer";

export class UpdateProductDto {
  @ApiProperty({
    example: 'Product name',
  })
  @IsString()
  @IsDefined()
  name: string;

  @ApiProperty({
    minimum: 0,
    example: 0,
  })
  @IsNumber()
  @IsDefined()
  @Transform(({ value }) => parseInt(value))
  price: number;

  @ApiProperty({
    minimum: 0,
    example: 0,
  })
  @IsNumber()
  @IsDefined()
  @Transform(({ value }) => parseInt(value))
  categoryId: number;
}