import {IsDefined, IsInt, IsString} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";
import {Controller} from "@nestjs/common";
import {Transform} from "class-transformer";

@Controller('products')
export class CreateProductDto{
    @IsString()
    @IsDefined()
    @ApiProperty({
      example: 'Product name',
    })
    name: string;
    @IsString()
    @IsDefined()
    @ApiProperty({
      example: 'Product description',
    })
    description: string;
    @IsInt()
    @IsDefined()
    @ApiProperty({
      example: 100,
    })
    @Transform(({ value }) => parseInt(value))
    price: number;
    @IsInt()
    @IsDefined()
    @ApiProperty({
      example: 10,
    })
    @Transform(({ value }) => parseInt(value))
    quantity: number;
    @IsInt()
    @IsDefined()
    @ApiProperty({
      example: 1,
    })
    @Transform(({ value }) => parseInt(value))
    categoryId: number;
    @IsInt()
    @IsDefined()
    @ApiProperty({
        example: 1,
    })
    @Transform(({ value }) => parseInt(value))
    blockchainId: number;
}