import {IsIntIdentifier} from "../../utils/decorators/utils.decorators.is-int-identifier";

export class IdDto {
    @IsIntIdentifier()
    id: number;
}