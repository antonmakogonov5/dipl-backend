import {ApiProperty} from "@nestjs/swagger";
import {IsDefined, IsNumber, IsString} from "class-validator";

export class UpdateProductMetadataDto {
  @ApiProperty({
    example: 'Product description',
  })
  @IsString()
  @IsDefined()
  description: string;
}