import {Body, Controller, Delete, Get, Param, Post, Put, Query, UploadedFile, UseInterceptors} from "@nestjs/common";
import {ProductService} from "./product.service";
import {Product} from "./models/product.model";
import {CreateProductDto} from "./dto/create-product.dto";
import {RequestTransaction} from "../utils/decorators/utils.decorators.request-transaction";
import {Transaction} from "sequelize";
import {AccessRoles} from "../auth/decorators/auth.decorators.access-roles";
import {Roles} from "../auth/enums/auth.enums.roles";
import {JwtInterceptor} from "../auth/interceptors/auth.interceptors.jwt";
import {RolesInterceptor} from "../auth/interceptors/auth.interceptors.roles";
import {AppFileInterceptor} from "../utils/interceptors/app-file.interceptor";
import { I18nContext } from 'nestjs-i18n';
import {GetProductsListQueryDto} from "./dto/products.list.query.dto";
import {IdDto} from "./dto/id.dto";
import {UpdateProductDto} from "./dto/update-product.dto";
import {UpdateProductMetadataDto} from "./dto/update-product-metadata";
import {UpdateQuantityDto} from "./dto/update-quantity.dto";

@Controller('products')
export class ProductController {
  constructor(
      private readonly productService: ProductService,
  ) {
  }

  @Post()
  @AccessRoles(Roles.admin)
  @UseInterceptors(
    JwtInterceptor,
    RolesInterceptor,
    AppFileInterceptor(I18nContext.current(), 'products', 'image', [
      'image/jpeg',
      'image/png',
      'image/svg+xml',
    ]),
  )
  async create(
      @Body() createProductDto: CreateProductDto,
      @UploadedFile() image: Express.Multer.File,
      @RequestTransaction() transaction: Transaction,
  ): Promise<Product> {
    return this.productService.create(
      createProductDto,
      image,
      transaction,
    );
  }

  @Get()
  async findAll(
    @Query() query: GetProductsListQueryDto,
  ): Promise<Product[]> {
    return this.productService.findAll(query);
  }

  @Get('/:id')
  async findOne(
   @Param() params: IdDto,
  ): Promise<Product> {
    return this.productService.findOne(params.id);
  }

  @Post(':id')
  @AccessRoles(Roles.admin)
  @UseInterceptors(
    JwtInterceptor,
    RolesInterceptor,
    AppFileInterceptor(I18nContext.current(), 'products', 'image', [
      'image/jpeg',
      'image/png',
      'image/svg+xml',
    ]),
  )
  async update(
    @Param() params: IdDto,
    @Body() updateProductDto: UpdateProductDto,
    @UploadedFile() image: Express.Multer.File,
    @RequestTransaction() transaction: Transaction,
  ): Promise<Product> {
    return this.productService.update(
      params.id,
      image,
      updateProductDto,
      transaction,
    );
  }

  @Post('/metadata/:id')
  @AccessRoles(Roles.admin)
  @UseInterceptors(
    JwtInterceptor,
    RolesInterceptor,
    AppFileInterceptor(I18nContext.current(), 'products', 'image', [
      'image/jpeg',
      'image/png',
      'image/svg+xml',
    ]),
  )
  async updateMetadata(
    @Param() params: IdDto,
    @Body() updateProductDto: UpdateProductMetadataDto,
    @UploadedFile() image: Express.Multer.File,
    @RequestTransaction() transaction: Transaction,
  ): Promise<Product> {
    return this.productService.updateMetadata(
      params.id,
      image,
      updateProductDto,
      transaction,
    );
  }

  @Put('/quantity/:id')
  @AccessRoles(Roles.admin)
  @UseInterceptors(
    JwtInterceptor,
    RolesInterceptor,
  )
  async updateQuantity(
    @Param() params: IdDto,
    @Body() updateProductDto: UpdateQuantityDto,
    @RequestTransaction() transaction: Transaction,
  ): Promise<Product> {
    return this.productService.updateQuantity(
      params.id,
      updateProductDto,
      transaction,
    );
  }

  @Delete(':id')
  @AccessRoles(Roles.admin)
  @UseInterceptors(
    JwtInterceptor,
    RolesInterceptor,
  )
  async remove(
    @Param() params: IdDto,
    @RequestTransaction() transaction: Transaction,
  ): Promise<{status: string}> {
    return this.productService.delete(
      params.id,
      transaction,
    );
  }
}