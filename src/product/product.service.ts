import {Product} from "./models/product.model";
import {InjectModel} from "@nestjs/sequelize";
import {CreateProductDto} from "./dto/create-product.dto";
import {UnprocessableEntityException} from "@nestjs/common";
import {I18nContext, I18nService} from 'nestjs-i18n';
import {Transaction} from "sequelize";
import {GetProductsListQueryDto} from "./dto/products.list.query.dto";
import {AppConfig} from "../app/app.config";
import {ConfigNames} from "../config/enums/config.enums.config-names";
import {ConfigService} from "@nestjs/config";
import {UpdateProductDto} from "./dto/update-product.dto";
import {UpdateProductMetadataDto} from "./dto/update-product-metadata";
import {UpdateQuantityDto} from "./dto/update-quantity.dto";
import { promises as fsPromises } from 'fs';


export class ProductService {
  private appConfig: AppConfig;
  constructor(
    @InjectModel(Product) private readonly productModel: typeof Product,
    private i18nService: I18nService,
    private configService: ConfigService,
  ) {
    this.appConfig = configService.get<AppConfig>(ConfigNames.app);
  }

  async findAll(query: GetProductsListQueryDto): Promise<Product[]> {
    const products = await this.productModel.findAll();
    products.forEach(product => {
      product.image = this.appConfig.url + '/public/' + product.image;
    });
    return products;
  }


  async create(createProductDto: CreateProductDto, image: Express.Multer.File, transaction: Transaction)
    : Promise<Product> {
    if (!image) {
      throw new UnprocessableEntityException(
        this.i18nService.t('validation.FILE_MISSING', {
          lang: I18nContext.current().lang,
        }),
      );
    }

    return await this.productModel.create({
        ...createProductDto,
        image: 'products/' + image.filename,
      },
      {
        transaction: transaction,
        fields: [
          'name',
          'price',
          'image',
          'description',
          'quantity',
          'categoryId',
          'blockchainId',
        ],
      });
  }

  async findOne(id: number): Promise<Product> {
    const product = await this.productModel.findByPk(id);
    product.image = this.appConfig.url + '/public/' + product.image;
    return product;
  }

  async update(id: number,  image: Express.Multer.File, updateProductDto: UpdateProductDto, transaction: Transaction): Promise<Product> {
    const product = await this.productModel.findByPk(id);
    if (!product) {
      throw new UnprocessableEntityException(
        this.i18nService.t('validation.PRODUCT_NOT_FOUND', {
          lang: I18nContext.current().lang,
        }),
      );
    }

    product.name = updateProductDto.name;
    product.price = updateProductDto.price;
    product.categoryId = updateProductDto.categoryId;
    await product.save({transaction: transaction});
    return product;
  }

  async updateMetadata(id: number, image:  Express.Multer.File, updateProductMetadataDto: UpdateProductMetadataDto, transaction: Transaction): Promise<Product> {
    const product = await this.productModel.findByPk(id);
    if (!product) {
      throw new UnprocessableEntityException(
        this.i18nService.t('validation.PRODUCT_NOT_FOUND', {
          lang: I18nContext.current().lang,
        }),
      );
    }

    product.description = updateProductMetadataDto.description;
    if (image) {
      product.image = 'products/' + image.filename;
    }
    await product.save({transaction: transaction});
    return product;
  }

  async updateQuantity(id: number, updateQuantityDto: UpdateQuantityDto, transaction: Transaction): Promise<Product> {
    const product = await this.productModel.findByPk(id);
    if (!product) {
      throw new UnprocessableEntityException(
        this.i18nService.t('validation.PRODUCT_NOT_FOUND', {
          lang: I18nContext.current().lang,
        }),
      );
    }

    product.quantity = product.quantity + updateQuantityDto.quantity;
    await product.save({transaction: transaction});
    return product;
  }

  async delete(id: number, transaction: Transaction): Promise<{ status: string }> {
    const product = await this.productModel.findByPk(id);
    if (!product) {
      throw new UnprocessableEntityException(
        this.i18nService.t('validation.PRODUCT_NOT_FOUND', {
          lang: I18nContext.current().lang,
        }),
      );
    }
    await fsPromises.unlink('./storage/public/' + product.image);

    await product.destroy({transaction: transaction});
    return {status: 'success'};
  }
}