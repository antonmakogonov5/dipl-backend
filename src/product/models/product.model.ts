import {Column, CreatedAt, DataType, Model, PrimaryKey, Table, UpdatedAt} from "sequelize-typescript";
import { InferAttributes, InferCreationAttributes } from 'sequelize';
@Table({
    tableName: 'products',
    timestamps: true,
})
export class Product extends Model<
  InferAttributes<Product>,
  InferCreationAttributes<Product>
> {
    @PrimaryKey
    @Column({
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
        type: DataType.INTEGER,
    })
    id: number;
    @Column({
        allowNull: false,
        type: DataType.STRING,
    })
    name: string;
    @Column({
        allowNull: false,
        type: DataType.INTEGER,
    })
    price: number;
    @Column({
        allowNull: false,
        type: DataType.STRING,
    })
    image: string;
    @Column({
        allowNull: false,
        type: DataType.TEXT,
    })
    description: string;
    @Column({
        allowNull: false,
        type: DataType.INTEGER,
    })
    categoryId: number;
    @Column({
        allowNull: false,
        type: DataType.INTEGER,
    })
    blockchainId: number;
    @Column({
        allowNull: false,
        type: DataType.INTEGER,
        defaultValue: 0,
    })
    quantity: number;
    @Column({
        allowNull: false,
        type: DataType.INTEGER,
        defaultValue: 0,
    })
    rating: number;
    @CreatedAt
    @Column({
        type: DataType.DATE,
        allowNull: false,
        defaultValue: DataType.NOW,
    })
    createdAt: Date;
    @UpdatedAt
    @Column({
        type: DataType.DATE,
    })
    updatedAt: Date;
}