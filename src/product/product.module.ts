import {Module} from "@nestjs/common";
import {SequelizeModule} from "@nestjs/sequelize";
import {Product} from "./models/product.model";
import {ProductController} from "./product.conroller";
import {ProductService} from "./product.service";
import {AccessToken} from "../auth/models/auth.models.access-token";

@Module({
    imports: [
      SequelizeModule.forFeature([Product,  AccessToken]),
    ],
    controllers: [ProductController],
    providers: [ProductService],
    exports: [ProductService],
})
export class ProductModule {
}