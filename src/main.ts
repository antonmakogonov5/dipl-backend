import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import morgan from 'morgan';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { AppModule } from 'src/app/app.module';
import { AppConfig } from 'src/app/app.config';
import { ConfigNames } from 'src/config/enums/config.enums.config-names';
import { I18nValidationPipe } from 'nestjs-i18n';
import { AppLogger } from './utils/logger/app.logger';
import { AppExceptionFilter } from './utils/filters/app-exception.filter';
import { useContainer } from 'class-validator';

bootstrap();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  process.on('SIGTERM', async () => {
    await app.close();
  });

  const configService: ConfigService = app.get<ConfigService>(ConfigService);
  const appConfig: AppConfig = configService.get<AppConfig>(ConfigNames.app);
  const appLogger = new AppLogger(configService);
  app
    .setGlobalPrefix('api')
    .useGlobalPipes(
      new I18nValidationPipe({
        whitelist: true,
        transform: true,
        stopAtFirstError: true,
        skipMissingProperties: false,
      }),
    )
    .useGlobalFilters(
      new AppExceptionFilter(app.get(HttpAdapterHost), appLogger),
    )
    .use(morgan('dev'))

    .useLogger(appLogger);
  useContainer(app.select(AppModule), { fallbackOnErrors: true });
  app.enableCors();

  const swaggerConfig = new DocumentBuilder()
    .addBearerAuth()
    .addServer(`${appConfig.url}/api`)
    .setTitle('Shark Pay API')
    .setVersion('1.0')
    .addTag('sharkPay')
    .build();
  const swaggerDocument = SwaggerModule.createDocument(app, swaggerConfig, {
    ignoreGlobalPrefix: true,
  });

  SwaggerModule.setup('api', app, swaggerDocument);

  app.enableCors();
  await app.listen(appConfig.port, '0.0.0.0');
}
