import { INestApplication, HttpStatus, ValidationError } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtService, JwtModule } from '@nestjs/jwt';
import { SequelizeModule, SequelizeModuleOptions } from '@nestjs/sequelize';
import { TestingModule, Test } from '@nestjs/testing';
import request from 'supertest';
import { Sequelize } from 'sequelize-typescript';

import { AuthConfig } from '../../src/auth/auth.config';
import { Roles } from '../../src/auth/enums/auth.enums.roles';
import { AccessToken } from '../../src/auth/models/auth.models.access-token';
import { RefreshToken } from '../../src/auth/models/auth.models.refresh-token';
import { ConfigNames } from '../../src/config/enums/config.enums.config-names';
import { loadConfig } from '../../src/config/loader/config.loader';
import { validateConfig } from '../../src/config/validator/config.validator';
import { User } from '../../src/user/user.model';
import { validationErrorMessages } from '../../src/utils/constants/utils.constants.validation-error-messages';
import { UtilsModule } from '../../src/utils/utils.module';
import { CryptoCurrencyCodes } from '../../src/currency/enums/cryptoCurrencyCodes';
import { PaydeskModule } from '../../src/paydesk/paydesk.module';
import {
  AcceptLanguageResolver,
  HeaderResolver,
  I18nModule,
  I18nValidationExceptionFilter,
  I18nValidationPipe,
  QueryResolver,
} from 'nestjs-i18n';
import path from 'path';
import bcrypt from 'bcryptjs';
import { Paydesk } from '../../src/paydesk/models/paydesk.model';
import { Languages } from '../../src/user/enums/user.enums.languages';
import { TimeLimit } from '../../src/paydesk/models/time-limit.model';
import { handleValidationError } from '../../src/utils/helpers/utils.helpers.validation-error-handler';
import { defaultErrorMessages } from '../../src/utils/constants/utils.constants.default-error-messages';

export const paydeskTests = () => {
  let app: INestApplication;
  let merchantToken: string;
  let traderToken: string;
  let timeLimitId: number;
  let paydeskUsdtId: number;
  let paydeskBtcId: number;
  let paydeskToDeleteId: number;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          validate: validateConfig,
          load: [loadConfig],
        }),
        SequelizeModule.forRootAsync({
          inject: [ConfigService],
          useFactory(configService: ConfigService): SequelizeModuleOptions {
            const dbConfig: SequelizeModuleOptions =
              configService.get<SequelizeModuleOptions>(ConfigNames.db);

            dbConfig.logging = false;

            return dbConfig;
          },
        }),
        JwtModule.registerAsync({
          global: true,
          inject: [ConfigService],
          async useFactory(configService: ConfigService) {
            const authConfig: AuthConfig = configService.get<AuthConfig>(
              ConfigNames.auth,
            );

            return authConfig.jwt;
          },
        }),
        I18nModule.forRootAsync({
          useFactory: () => ({
            fallbackLanguage: 'debug',
            loaderOptions: {
              path: path.join(__dirname, '../../src/i18n'),
              watch: true,
            },
            typesOutputPath: path.join(
              __dirname,
              '../../src/generated/i18n.generated.ts',
            ),
          }),
          resolvers: [
            { use: QueryResolver, options: ['lang'] },
            AcceptLanguageResolver,
            new HeaderResolver(['x-lang']),
          ],
          inject: [ConfigService],
        }),
        UtilsModule,
        PaydeskModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();

    app
      .setGlobalPrefix('api')
      .useGlobalPipes(
        new I18nValidationPipe({
          whitelist: true,
          transform: true,
          skipMissingProperties: false,
        }),
      )
      .useGlobalFilters(
        new I18nValidationExceptionFilter({
          errorHttpStatusCode: 422,
          errorFormatter(validationErrors: ValidationError[]) {
            const responseErrors: any = {};
            validationErrors.forEach(function (
              validationError: ValidationError,
            ) {
              handleValidationError(validationError, responseErrors);
            });
            return responseErrors;
          },
        }),
      );

    await app.init();

    const jwtService = app.get<JwtService>(JwtService);
    const sequelize = app.get<Sequelize>(Sequelize);
    const queryInterface = sequelize.getQueryInterface();
    const userModel = sequelize.getRepository(User);
    const refreshTokenModel = sequelize.getRepository(RefreshToken);
    const accessTokenModel = sequelize.getRepository(AccessToken);
    const paydeskModel = sequelize.getRepository(Paydesk);
    const timeLimitModel = sequelize.getRepository(TimeLimit);
    await Promise.all([
      ...Object.values(sequelize.models).map((model) => {
        return model.truncate({
          cascade: true,
          force: true,
          restartIdentity: true,
        });
      }),
    ]);
    await queryInterface.bulkDelete('roles', {});
    await Promise.all([
      queryInterface.bulkInsert('roles', [
        { name: Roles.trader },
        { name: Roles.merchant },
        { name: Roles.admin },
      ]),
      queryInterface.bulkInsert('currencies', [
        { code: CryptoCurrencyCodes.usdt, rate: 36 },
        { code: CryptoCurrencyCodes.btc, rate: 1008000 },
      ]),
      queryInterface.bulkInsert('timeLimits', [
        { time: 20 * 60 },
        { time: 25 * 60 },
        { time: 30 * 60 },
        { time: 35 * 60 },
        { time: 40 * 60 },
      ]),
    ]);
    await queryInterface.bulkInsert('users', [
      {
        email: process.env.ADMIN_EMAIL,
        password: await bcrypt.hash(process.env.ADMIN_PASSWORD, 7),
        confirmed: true,
        roleName: Roles.admin,
      },
      {
        email: 'trader@mail.com',
        password: await bcrypt.hash('password1', 7),
        confirmed: true,
        roleName: Roles.trader,
        name: 'trader',
        telegram: 'traderTelegram',
      },
      {
        email: 'merchant@mail.com',
        password: await bcrypt.hash('password1', 7),
        confirmed: true,
        roleName: Roles.merchant,
        name: 'merchant',
        telegram: 'traderTelegram',
        rate: '6',
      },
    ]);
    await queryInterface.bulkInsert('fees', [
      {
        min: 10_000, //0.0001 BTC
        max: 50_000 - 1,
        currencyCode: 'btc',
        merchantFee: 7_000,
        traderFee: 7_000,
      },
      {
        min: 50_000, //0.0005 BTC
        max: 100_000 - 1,
        currencyCode: 'btc',
        merchantFee: 10_000,
        traderFee: 10_000,
      },
      {
        min: 100_000, //0.001 BTC
        max: 10_000_000 - 1,
        currencyCode: 'btc',
        merchantFee: 20_000,
        traderFee: 20_000,
      },
      {
        min: 10_000_000, //0.1 BTC
        max: 100_000_000,
        currencyCode: 'btc',
        merchantFee: 30_000,
        traderFee: 30_000,
      },
      {
        min: 10_000_000,
        max: 10_000_000_000,
        currencyCode: 'usdt',
        traderFee: 5_000_000,
        merchantFee: 1_000_000,
      },
    ]);
    const [merchant, trader] = await Promise.all([
      userModel.findOne({
        where: { roleName: Roles.merchant },
      }),
      userModel.findOne({
        where: { roleName: Roles.trader },
      }),
    ]);
    const timeLimit = await timeLimitModel.findOne({});
    timeLimitId = timeLimit.id;
    const [paydeskUSDT, paydeskBTC, paydeskToDelete] = await Promise.all([
      paydeskModel.create({
        userId: merchant.id,
        name: 'Store',
        url: 'https://store.com',
        language: Languages.en,
        actualBalance: '100000000000',
        privateKey: 'privateKey',
        publicKey: 'publicKey',
        currencyCode: CryptoCurrencyCodes.usdt,
        timeLimitId,
      }),
      paydeskModel.create({
        userId: merchant.id,
        name: 'StoreBTC',
        url: 'https://storeBTC.com',
        actualBalance: '90000000',
        language: Languages.en,
        privateKey: 'privateKey',
        publicKey: 'publicKey',
        currencyCode: CryptoCurrencyCodes.btc,
        timeLimitId,
      }),
      paydeskModel.create({
        userId: merchant.id,
        name: 'Storedelete',
        url: 'https://storedelete.com',
        actualBalance: '0',
        language: Languages.en,
        privateKey: 'privateKey',
        publicKey: 'publicKey',
        currencyCode: CryptoCurrencyCodes.btc,
        timeLimitId,
      }),
    ]);
    paydeskUsdtId = paydeskUSDT.id;
    paydeskBtcId = paydeskBTC.id;
    paydeskToDeleteId = paydeskToDelete.id;
    const [refreshToken, traderRefreshToken] = await Promise.all([
      refreshTokenModel.create({
        userId: merchant.id,
      }),
      refreshTokenModel.create({
        userId: trader.id,
      }),
    ]);
    const [accessToken, traderAccessToken] = await Promise.all([
      accessTokenModel.create({
        userId: merchant.id,
        refreshTokenId: refreshToken.id,
      }),
      accessTokenModel.create({
        userId: trader.id,
        refreshTokenId: traderRefreshToken.id,
      }),
    ]);
    const authConfig = app.get(ConfigService).get<AuthConfig>(ConfigNames.auth);
    merchantToken = await jwtService.signAsync(
      {
        id: accessToken.id,
      },
      {
        expiresIn: authConfig.ttl.access,
      },
    );
    traderToken = await jwtService.signAsync(
      {
        id: traderAccessToken.id,
      },
      {
        expiresIn: authConfig.ttl.access,
      },
    );
  });

  afterAll(async () => {
    await app.close();
  });

  describe('Get paydesks (GET /api/paydesks)', () => {
    it('Should fail for trader', async () => {
      await request(app.getHttpServer())
        .get('/api/paydesks')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${traderToken}`)
        .expect(HttpStatus.NOT_FOUND);
    });
    it('Should fail without access token', async () => {
      await request(app.getHttpServer())
        .get('/api/paydesks')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect(HttpStatus.UNAUTHORIZED);
    });
    it('Should fail with invalid access token', async () => {
      const { body } = await request(app.getHttpServer())
        .get(`/api/paydesks`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer qwe`)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toMatchObject({
        message: validationErrorMessages.accessToken.invalid,
      });
    });
    it('Should success with valid input', async () => {
      const { body } = await request(app.getHttpServer())
        .get('/api/paydesks')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`);
      expect(body).toEqual({
        paydesks: expect.any(Array),
        currencies: expect.any(Array),
        totalBalance: expect.any(String),
      });
      expect(body.paydesks[0]).toMatchObject({
        id: expect.any(Number),
        name: expect.any(String),
      });
      expect(body.currencies[0]).toEqual({
        code: expect.any(String),
        rate: expect.any(String),
      });
    });
  });
  describe('Create paydesk (POST /api/paydesks)', () => {
    it('Should fail for trader', async () => {
      await request(app.getHttpServer())
        .post('/api/paydesks')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${traderToken}`)
        .expect(HttpStatus.NOT_FOUND);
    });
    it('Should fail without access token', async () => {
      await request(app.getHttpServer())
        .post('/api/paydesks')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect(HttpStatus.UNAUTHORIZED);
    });
    it('Should fail with invalid access token', async () => {
      const invalidmerchantToken: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .post(`/api/paydesks`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${invalidmerchantToken}`)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toMatchObject({
        message: validationErrorMessages.accessToken.invalid,
      });
    });
    it('Should fail with empty input', async () => {
      const { body } = await request(app.getHttpServer())
        .post('/api/paydesks')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .send({})
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toMatchObject({
        errors: {
          name: ['validation.MAX_LENGTH', 'validation.IS_NOT_EMPTY'],
          url: [
            'validation.IS_URL',
            'validation.MAX_LENGTH',
            'validation.IS_NOT_EMPTY',
          ],
          currencyCode: ['validation.IS_ENUM', 'validation.IS_NOT_EMPTY'],
          language: ['validation.IS_ENUM', 'validation.IS_NOT_EMPTY'],
          timeLimitId: ['validation.IS_INT', 'validation.MIN'],
        },
      });
    });

    it('Should fail with invalid input', async () => {
      const { body } = await request(app.getHttpServer())
        .post('/api/paydesks')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .send({
          name: 'store2',
          url: 'wrong url',
          currencyCode: 'invalid',
          language: 'invalid',
          timeLimitId: 1,
        })
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toMatchObject({
        errors: {
          url: ['validation.IS_URL'],
          currencyCode: ['validation.IS_ENUM'],
          language: ['validation.IS_ENUM'],
        },
      });
    });
    it('Should fail with invalid timeLimitId', async () => {
      const { body } = await request(app.getHttpServer())
        .post('/api/paydesks')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .send({
          name: 'store2',
          url: 'https://store2.com',
          currencyCode: CryptoCurrencyCodes.usdt,
          language: Languages.en,
          timeLimitId: 999999,
        })
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toMatchObject({
        errors: {
          timeLimitId: ['validation.EXISTS_TIME_LIMIT'],
        },
      });
    });
    it('Should success with valid input (USDT)', async () => {
      const { body } = await request(app.getHttpServer())
        .post('/api/paydesks')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .send({
          name: 'Store USDT',
          url: 'https://storeusdt.com',
          currencyCode: CryptoCurrencyCodes.usdt,
          language: Languages.en,
          timeLimitId,
        })
        .expect(HttpStatus.CREATED);

      expect(body).toMatchObject({
        id: expect.any(Number),
        privateKey: expect.any(String),
        publicKey: expect.any(String),
      });
    });
    it('Should success with valid input (BTC)', async () => {
      const { body } = await request(app.getHttpServer())
        .post('/api/paydesks')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .send({
          name: 'Store BTC',
          url: 'https://storebtc.com',
          currencyCode: CryptoCurrencyCodes.btc,
          language: Languages.en,
          timeLimitId,
        })
        .expect(HttpStatus.CREATED);

      expect(body).toMatchObject({
        id: expect.any(Number),
        privateKey: expect.any(String),
        publicKey: expect.any(String),
      });
    });
  });
  describe('Get paydesk (GET /api/paydesks/:id)', () => {
    it('Should fail for trader', async () => {
      await request(app.getHttpServer())
        .get(`/api/paydesks/${paydeskUsdtId}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${traderToken}`)
        .expect(HttpStatus.NOT_FOUND);
    });
    it('Should fail without access token', async () => {
      await request(app.getHttpServer())
        .get(`/api/paydesks/${paydeskUsdtId}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect(HttpStatus.UNAUTHORIZED);
    });
    it('Should fail with invalid access token', async () => {
      const invalidmerchantToken: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .get(`/api/paydesks/1`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${invalidmerchantToken}`)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toMatchObject({
        message: validationErrorMessages.accessToken.invalid,
      });
    });
    it('Should fail with invalid id', async () => {
      const { body } = await request(app.getHttpServer())
        .get('/api/paydesks/qwe')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toMatchObject({
        errors: {
          paydeskId: ['validation.IS_INT', 'validation.MIN'],
        },
      });
    });
    it('Should fail with invalid id', async () => {
      const { body } = await request(app.getHttpServer())
        .get('/api/paydesks/999999')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toMatchObject({
        message: defaultErrorMessages.notFound,
      });
    });
    it('Should success with valid input', async () => {
      const { body } = await request(app.getHttpServer())
        .get(`/api/paydesks/${paydeskUsdtId}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`);

      expect(body).toMatchObject({
        id: expect.any(Number),
        name: expect.any(String),
      });
    });
  });
  describe('Update paydesk (PUT /api/paydesks/:id)', () => {
    it('Should fail for trader', async () => {
      await request(app.getHttpServer())
        .put(`/api/paydesks/${paydeskUsdtId}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${traderToken}`)
        .expect(HttpStatus.NOT_FOUND);
    });
    it('Should fail without access token', async () => {
      await request(app.getHttpServer())
        .put(`/api/paydesks/${paydeskUsdtId}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect(HttpStatus.UNAUTHORIZED);
    });
    it('Should fail with invalid access token', async () => {
      const invalidmerchantToken: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .put(`/api/paydesks/1`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${invalidmerchantToken}`)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toMatchObject({
        message: validationErrorMessages.accessToken.invalid,
      });
    });
    it('Should fail with invalid id', async () => {
      const { body } = await request(app.getHttpServer())
        .put('/api/paydesks/qwe')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .send({
          name: 'store2',
          url: 'https://store2.com',
          language: Languages.en,
          timeLimitId,
        })
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toMatchObject({
        errors: {
          paydeskId: ['validation.IS_INT', 'validation.MIN'],
        },
      });
    });
    it('Should fail with not existing id', async () => {
      const { body } = await request(app.getHttpServer())
        .put('/api/paydesks/999999')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .send({
          name: 'store2',
          url: 'https://store2.com',
          language: Languages.en,
          timeLimitId,
        })
        .expect(HttpStatus.NOT_FOUND);
      expect(body).toMatchObject({
        message: 'RESOURCE_NOT_FOUND',
      });
    });
    it('Should fail with invalid input', async () => {
      const { body } = await request(app.getHttpServer())
        .put(`/api/paydesks/${paydeskUsdtId}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .send({
          name: 'store2',
          url: 'wrong url',
          language: 'invalid',
          timeLimitId: 1,
        })
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toMatchObject({
        errors: {
          url: ['validation.IS_URL'],
          language: ['validation.IS_ENUM'],
        },
      });
    });
    it('Should fail with invalid timeLimitId', async () => {
      const { body } = await request(app.getHttpServer())
        .put(`/api/paydesks/${paydeskUsdtId}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)

        .send({
          name: 'store2',
          url: 'https://store2.com',
          language: Languages.en,
          timeLimitId: 999999,
        })
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toMatchObject({
        errors: {
          timeLimitId: ['validation.EXISTS_TIME_LIMIT'],
        },
      });
    });
    it('Should success with valid input', async () => {
      const { body } = await request(app.getHttpServer())
        .put(`/api/paydesks/${paydeskUsdtId}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)

        .send({
          name: 'store2',
          url: 'https://store2.com',
          language: Languages.en,
          timeLimitId,
        })
        .expect(HttpStatus.OK);

      expect(body).toMatchObject({
        id: expect.any(Number),
        name: expect.any(String),
      });
    });
  });

  describe('Withdraw from paydesk (POST /api/paydesks/:id/withdraw)', () => {
    it('Should fail for trader', async () => {
      await request(app.getHttpServer())
        .post(`/api/paydesks/${paydeskUsdtId}/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${traderToken}`)
        .expect(HttpStatus.NOT_FOUND);
    });
    it('Should fail without access token', async () => {
      await request(app.getHttpServer())
        .post(`/api/paydesks/${paydeskUsdtId}/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect(HttpStatus.UNAUTHORIZED);
    });
    it('Should fail with invalid access token', async () => {
      const invalidmerchantToken: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .post(`/api/paydesks/1/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${invalidmerchantToken}`)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toMatchObject({
        message: validationErrorMessages.accessToken.invalid,
      });
    });
    it('Should fail with invalid id', async () => {
      const { body } = await request(app.getHttpServer())
        .post('/api/paydesks/qwe/withdraw')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .send({
          address: 'TRq8kCGFfghGavHS21XoQ9QPeJUbZN23vw',
          amount: 10,
        })
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toMatchObject({
        errors: {
          paydeskId: ['validation.IS_INT', 'validation.MIN'],
        },
      });
    });
    it('Should fail with not existing id', async () => {
      const { body } = await request(app.getHttpServer())
        .post('/api/paydesks/999999/withdraw')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .send({
          address: 'TRq8kCGFfghGavHS21XoQ9QPeJUbZN23vw',
          amount: 10,
        })
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toMatchObject({
        message: defaultErrorMessages.notFound,
      });
    });
    it('Should fail with empty input', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/paydesks/${paydeskUsdtId}/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .send()
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toMatchObject({
        errors: {
          amount: expect.arrayContaining([
            'validation.IS_NOT_EMPTY',
            'validation.CRYPTO_AMOUNT',
            'validation.IS_POSITIVE',
          ]),
          address: expect.arrayContaining([
            'validation.IS_CRYPTO_ADDRESS',
            'validation.IS_NOT_EMPTY',
          ]),
        },
      });
    });
    it('Should fail with invalid amount (too small)', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/paydesks/${paydeskUsdtId}/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .send({
          amount: 1,
          address: 'TRq8kCGFfghGavHS21XoQ9QPeJUbZN23vw',
        })
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);
      expect(body).toMatchObject({
        errors: {
          amount: ['validation.MIN'],
        },
      });
    });
    it('Should fail with invalid amount (too big)', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/paydesks/${paydeskUsdtId}/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .send({
          amount: 100_000,
          address: 'TRq8kCGFfghGavHS21XoQ9QPeJUbZN23vw',
        })
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toMatchObject({
        errors: {
          amount: ['validation.MAX'],
        },
      });
    });
    it('Should fail with invalid address', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/paydesks/${paydeskUsdtId}/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)

        .send({
          amount: 10,
          address: 'mpsZzzpy5tE9G4VA6UXfSomftiSZJxiitg',
        })
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toMatchObject({
        errors: {
          address: ['validation.IS_CRYPTO_ADDRESS'],
        },
      });
    });
    it('Should success with valid input (USDT)', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/paydesks/${paydeskUsdtId}/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)

        .send({
          amount: 10,
          address: 'TRq8kCGFfghGavHS21XoQ9QPeJUbZN23vw',
        })
        .expect(HttpStatus.CREATED);

      expect(body).toMatchObject({
        id: expect.any(Number),
        status: expect.any(String),
        currencyCode: expect.any(String),
      });
    });
    it('Should success with valid input (BTC)', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/paydesks/${paydeskBtcId}/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)

        .send({
          amount: 0.0001,
          address: 'mpsZzzpy5tE9G4VA6UXfSomftiSZJxiitg',
        })
        .expect(HttpStatus.CREATED);

      expect(body).toMatchObject({
        id: expect.any(Number),
        status: expect.any(String),
        currencyCode: expect.any(String),
      });
    });
  });

  describe('Delete paydesk (DELETE /api/paydesks/:id)', () => {
    it('Should fail for trader', async () => {
      await request(app.getHttpServer())
        .delete(`/api/paydesks/${paydeskUsdtId}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${traderToken}`)
        .expect(HttpStatus.NOT_FOUND);
    });
    it('Should fail without access token', async () => {
      await request(app.getHttpServer())
        .delete(`/api/paydesks/${paydeskUsdtId}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect(HttpStatus.UNAUTHORIZED);
    });
    it('Should fail with invalid access token', async () => {
      const invalidmerchantToken: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .delete(`/api/paydesks/1`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${invalidmerchantToken}`)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toMatchObject({
        message: validationErrorMessages.accessToken.invalid,
      });
    });
    it('Should fail with invalid id', async () => {
      const { body } = await request(app.getHttpServer())
        .delete('/api/paydesks/qwe')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toMatchObject({
        errors: {
          paydeskId: ['validation.IS_INT', 'validation.MIN'],
        },
      });
    });
    it('Should fail with not existing id', async () => {
      const { body } = await request(app.getHttpServer())
        .delete('/api/paydesks/999999')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toMatchObject({
        message: defaultErrorMessages.notFound,
      });
    });
    it('Should success with valid input', async () => {
      await request(app.getHttpServer())
        .delete(`/api/paydesks/${paydeskToDeleteId}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .expect(HttpStatus.OK);
    });
  });
};
