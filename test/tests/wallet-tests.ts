import { INestApplication, HttpStatus, ValidationError } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtService, JwtModule } from '@nestjs/jwt';
import { SequelizeModule, SequelizeModuleOptions } from '@nestjs/sequelize';
import { TestingModule, Test } from '@nestjs/testing';
import request from 'supertest';
import { AuthConfig } from '../../src/auth/auth.config';
import { Roles } from '../../src/auth/enums/auth.enums.roles';
import { AccessToken } from '../../src/auth/models/auth.models.access-token';
import { RefreshToken } from '../../src/auth/models/auth.models.refresh-token';
import { ConfigNames } from '../../src/config/enums/config.enums.config-names';
import { loadConfig } from '../../src/config/loader/config.loader';
import { validateConfig } from '../../src/config/validator/config.validator';
import { User } from '../../src/user/user.model';
import { UserModule } from '../../src/user/user.module';
import { validationErrorMessages } from '../../src/utils/constants/utils.constants.validation-error-messages';
import { UtilsModule } from '../../src/utils/utils.module';
import { CryptoCurrencyCodes } from '../../src/currency/enums/cryptoCurrencyCodes';
import { Sequelize } from 'sequelize-typescript';
import {
  AcceptLanguageResolver,
  HeaderResolver,
  I18nModule,
  I18nValidationExceptionFilter,
  I18nValidationPipe,
  QueryResolver,
} from 'nestjs-i18n';
import { join } from 'path';
import { handleValidationError } from '../../src/utils/helpers/utils.helpers.validation-error-handler';
import { Wallet } from '../../src/wallet/models/wallet.model';

export const walletTests = () => {
  let app: INestApplication;
  let accessToken: string;
  let merchantToken: string;
  let usdtWalletId: number;
  let btcWalletId: number;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          validate: validateConfig,
          load: [loadConfig],
        }),
        SequelizeModule.forRootAsync({
          inject: [ConfigService],
          useFactory(configService: ConfigService): SequelizeModuleOptions {
            const dbConfig: SequelizeModuleOptions =
              configService.get<SequelizeModuleOptions>(ConfigNames.db);

            dbConfig.logging = false;

            return dbConfig;
          },
        }),
        JwtModule.registerAsync({
          global: true,
          inject: [ConfigService],
          async useFactory(configService: ConfigService) {
            const authConfig: AuthConfig = configService.get<AuthConfig>(
              ConfigNames.auth,
            );

            return authConfig.jwt;
          },
        }),
        I18nModule.forRootAsync({
          useFactory: () => ({
            fallbackLanguage: 'debug',
            loaderOptions: {
              path: join(__dirname, '../../src/i18n'),
              watch: true,
            },
            typesOutputPath: join(
              __dirname,
              '../../src/generated/i18n.generated.ts',
            ),
          }),
          inject: [ConfigService],
          resolvers: [
            { use: QueryResolver, options: ['lang'] },
            AcceptLanguageResolver,
            new HeaderResolver(['x-lang']),
          ],
        }),
        UtilsModule,
        UserModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();

    app
      .setGlobalPrefix('api')
      .useGlobalPipes(
        new I18nValidationPipe({
          whitelist: true,
          transform: true,
          skipMissingProperties: false,
        }),
      )
      .useGlobalFilters(
        new I18nValidationExceptionFilter({
          errorHttpStatusCode: 422,
          errorFormatter(validationErrors: ValidationError[]) {
            const responseErrors: any = {};
            validationErrors.forEach(function (
              validationError: ValidationError,
            ) {
              handleValidationError(validationError, responseErrors);
            });
            return responseErrors;
          },
        }),
      );

    await app.init();

    const jwtService = app.get<JwtService>(JwtService);
    const sequelize = app.get<Sequelize>(Sequelize);
    const queryInterface = sequelize.getQueryInterface();
    const userModel = sequelize.getRepository(User);
    const refreshTokenModel = sequelize.getRepository(RefreshToken);
    const accessTokenModel = sequelize.getRepository(AccessToken);
    const walletModel = sequelize.getRepository(Wallet);

    await Promise.all([
      ...Object.values(sequelize.models).map((model) => {
        return model.truncate({
          cascade: true,
          force: true,
          restartIdentity: true,
        });
      }),
    ]);
    await queryInterface.bulkDelete('roles', {});

    await Promise.all([
      queryInterface.bulkInsert('roles', [
        { name: Roles.trader },
        { name: Roles.merchant },
        { name: Roles.admin },
      ]),
      queryInterface.bulkInsert('currencies', [
        { code: CryptoCurrencyCodes.usdt, rate: 36 },
        { code: CryptoCurrencyCodes.btc, rate: 1000000 },
      ]),
    ]);
    const configService = app.get(ConfigService);
    const authConfig: AuthConfig = configService.get<AuthConfig>(
      ConfigNames.auth,
    );

    const [trader, merchant] = await Promise.all([
      userModel.create({
        email: 'trader@mail.com',
        role: Roles.trader,
        rate: 3,
      }),
      userModel.create({
        email: 'merchant@mail.com',
        role: Roles.merchant,
        rate: 6,
      }),
    ]);
    await queryInterface.bulkInsert('fees', [
      {
        min: 10_000, //0.0001 BTC
        max: 50_000 - 1,
        currencyCode: 'btc',
        merchantFee: 7_000,
        traderFee: 7_000,
      },
      {
        min: 50_000, //0.0005 BTC
        max: 100_000 - 1,
        currencyCode: 'btc',
        merchantFee: 10_000,
        traderFee: 10_000,
      },
      {
        min: 100_000, //0.001 BTC
        max: 10_000_000 - 1,
        currencyCode: 'btc',
        merchantFee: 20_000,
        traderFee: 20_000,
      },
      {
        min: 10_000_000, //0.1 BTC
        max: 100_000_000,
        currencyCode: 'btc',
        merchantFee: 30_000,
        traderFee: 30_000,
      },
      {
        min: 10_000_000,
        max: 10_000_000_000,
        currencyCode: 'usdt',
        traderFee: 5_000_000,
        merchantFee: 1_000_000,
      },
    ]);

    const [usdtWallet, btcWallet] = await Promise.all([
      walletModel.create({
        userId: trader.id,
        currencyCode: CryptoCurrencyCodes.usdt,
        actualBalance: '100000000000',
        address: 'addr',
        privateKey: 'qwe',
        publicKey: 'qwerty',
      }),
      walletModel.create({
        userId: trader.id,
        currencyCode: CryptoCurrencyCodes.btc,
        actualBalance: '90000000',
        address: 'addr',
        privateKey: 'qwe',
        publicKey: 'qwerty',
      }),
    ]);
    btcWalletId = btcWallet.id;
    usdtWalletId = usdtWallet.id;
    const [traderRefresh, merchantRefresh] = await Promise.all([
      refreshTokenModel.create({
        userId: trader.id,
      }),
      refreshTokenModel.create({
        userId: merchant.id,
      }),
    ]);
    const [traderAccess, merchantAccess] = await Promise.all([
      accessTokenModel.create({
        userId: trader.id,
        refreshTokenId: traderRefresh.id,
      }),
      accessTokenModel.create({
        userId: merchant.id,
        refreshTokenId: merchantRefresh.id,
      }),
    ]);
    const [traderTokenValue, merchantTokenValue] = await Promise.all([
      jwtService.signAsync(
        { id: traderAccess.id },
        { expiresIn: authConfig.ttl.access },
      ),
      jwtService.signAsync(
        { id: merchantAccess.id },
        { expiresIn: authConfig.ttl.access },
      ),
    ]);
    accessToken = traderTokenValue;
    merchantToken = merchantTokenValue;
  });

  afterAll(async () => {
    await app.close();
  });

  describe('Get wallet (GET /api/wallets)', () => {
    it('Should fail for merchant', async () => {
      await request(app.getHttpServer())
        .get('/api/wallets')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .expect(HttpStatus.NOT_FOUND);
    });
    it('Should fail without access token', async () => {
      await request(app.getHttpServer())
        .get('/api/wallets')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect(HttpStatus.UNAUTHORIZED);
    });
    it('Should fail with invalid access token', async () => {
      const invalidAccessTokenValue: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .get(`/api/wallets`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${invalidAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toMatchObject({
        message: validationErrorMessages.accessToken.invalid,
      });
    });
    it('Should success with valid input', async () => {
      const { body } = await request(app.getHttpServer())
        .get('/api/wallets')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessToken}`);
      expect(body).toMatchObject({
        wallets: expect.any(Array),
        currencies: expect.any(Array),
        totalBalance: expect.any(String),
      });
    });
  });
  describe('Withdraw (POST /api/wallets/:walletId/withdraw)', () => {
    it('Should fail for merchant', async () => {
      await request(app.getHttpServer())
        .post('/api/wallets/1/withdraw')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${merchantToken}`)
        .expect(HttpStatus.NOT_FOUND);
    });
    it('Should fail without access token', async () => {
      await request(app.getHttpServer())
        .post('/api/wallets/1/withdraw')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect(HttpStatus.UNAUTHORIZED);
    });
    it('Should fail with invalid access token', async () => {
      const invalidAccessTokenValue: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .post(`/api/wallets/1/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${invalidAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toMatchObject({
        message: validationErrorMessages.accessToken.invalid,
      });
    });
    it('Should fail with invalid wallet id', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/wallets/qwe/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          address: 'TRq8kCGFfghGavHS21XoQ9QPeJUbZN23vw',
          amount: '10',
        })
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);
      expect(body).toMatchObject({
        errors: {
          walletId: expect.arrayContaining([
            'validation.IS_INT',
            'validation.MIN',
          ]),
        },
      });
    });
    it('Should fail with empty body', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/wallets/1/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          address: 'qwe',
        })
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toMatchObject({
        errors: {
          address: ['validation.IS_CRYPTO_ADDRESS'],
          amount: expect.arrayContaining([
            'validation.CRYPTO_AMOUNT',
            'validation.IS_POSITIVE',
          ]),
        },
      });
    });
    it('Should fail with invalid address', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/wallets/${usdtWalletId}/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          address: 'TRq8kCGFfghGavHS21XoQ9QPeJUbZN23v1',
          amount: 10,
        })
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toMatchObject({
        errors: {
          address: ['validation.IS_CRYPTO_ADDRESS'],
        },
      });
    });
    it('Should fail with invalid amount', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/wallets/${usdtWalletId}/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          address: 'TRq8kCGFfghGavHS21XoQ9QPeJUbZN23vw',
          amount: 'qwe',
        })
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toMatchObject({
        errors: {
          amount: expect.arrayContaining([
            'validation.CRYPTO_AMOUNT',
            'validation.IS_POSITIVE',
          ]),
        },
      });
    });
    it('Should fail with invalid amount (too big)', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/wallets/${usdtWalletId}/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          address: 'TRq8kCGFfghGavHS21XoQ9QPeJUbZN23vw',
          amount: '10001',
        })
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toMatchObject({
        errors: {
          amount: ['validation.MAX'],
        },
      });
    });
    it('Should fail with invalid amount (too small)', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/wallets/${usdtWalletId}/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          address: 'TRq8kCGFfghGavHS21XoQ9QPeJUbZN23vw',
          amount: 1,
        });
      expect(body).toMatchObject({
        errors: {
          amount: ['validation.MIN'],
        },
      });
    });
    it('Should fail with invalid amount (not enough balance)', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/wallets/${btcWalletId}/withdraw`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          address: 'mpsZzzpy5tE9G4VA6UXfSomftiSZJxiitg',
          amount: '1',
        })
        .expect(HttpStatus.FORBIDDEN);
      expect(body).toMatchObject({
        error: 'Forbidden',
        message: 'INSUFFICIENT_FUNDS',
        statusCode: 403,
      });
    });
  });
};
