import path from 'path';
import fsPromise from 'fs/promises';
import fs from 'fs';

import { INestApplication, ValidationPipe, HttpStatus } from '@nestjs/common';
import { Sequelize } from 'sequelize-typescript';
import { QueryInterface } from 'sequelize';
import { TestingModule, Test } from '@nestjs/testing';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { SequelizeModule, SequelizeModuleOptions } from '@nestjs/sequelize';
import { JwtModule, JwtService } from '@nestjs/jwt';
import request from 'supertest';
import bcrypt from 'bcryptjs';

import { validateConfig } from 'src/config/validator/config.validator';
import { loadConfig } from 'src/config/loader/config.loader';
import { ConfigNames } from 'src/config/enums/config.enums.config-names';
import { AuthConfig } from 'src/auth/auth.config';
import { UtilsModule } from 'src/utils/utils.module';
import { AuthModule } from 'src/auth/auth.module';
import { Roles } from 'src/auth/enums/auth.enums.roles';
import { validationErrorMessages } from 'src/utils/constants/utils.constants.validation-error-messages';
import { User } from 'src/user/user.model';
import { RefreshToken } from 'src/auth/models/auth.models.refresh-token';
import { AccessToken } from 'src/auth/models/auth.models.access-token';
import { ResetPasswordToken } from 'src/auth/models/auth.models.reset-password-token';
import { userErrorMessages } from 'src/user/constants/user.constants.error-messages';
import { authErrorMessages } from 'src/auth/constants/auth.constants.error-messages';
import { defaultErrorMessages } from 'src/utils/constants/utils.constants.default-error-messages';
import { UserModule } from 'src/user/user.module';
import { PaymentOfferModule } from 'src/payment-offer/payment-offer.module';
import { PaymentType } from 'src/payment-type/payment-type.model';
import { PaymentOffer } from 'src/payment-offer/payment-offer.model';
import { paymentTypeErrorMessages } from 'src/payment-type/constants/payment-type.constants.error-messages';
import { paymentOfferErrorMessages } from 'src/payment-offer/constants/payment-offer.constants.error-messages';
import { PaymentModule } from 'src/payment/payment.module';
import { Payment } from 'src/payment/models/payment.model';
import { CryptoCurrencyCodes } from 'src/currency/enums/cryptoCurrencyCodes';
import { PaymentStatuses } from 'src/payment/enums/payment.enums.payment-statuses';
import { PaymentWays } from 'src/payment/enums/payment.enums.payment-ways';
import { paydeskTests } from './tests/paydesk-tests';
import { walletTests } from './tests/wallet-tests';

/*describe('Auth (e2e)', () => {
  let app: INestApplication;
  let jwtService: JwtService;
  let sequelize: Sequelize;
  let queryInterface: QueryInterface;
  let userModel: typeof User;
  let refreshTokenModel: typeof RefreshToken;
  let accessTokenModel: typeof AccessToken;
  let resetPasswordTokenModel: typeof ResetPasswordToken;
  let configService: ConfigService;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          validate: validateConfig,
          load: [loadConfig],
        }),
        SequelizeModule.forRootAsync({
          inject: [ConfigService],
          useFactory(configService: ConfigService): SequelizeModuleOptions {
            const dbConfig: SequelizeModuleOptions =
              configService.get<SequelizeModuleOptions>(ConfigNames.db);

            dbConfig.logging = false;

            return dbConfig;
          },
        }),
        JwtModule.registerAsync({
          global: true,
          inject: [ConfigService],
          async useFactory(configService: ConfigService) {
            const authConfig: AuthConfig = configService.get<AuthConfig>(
              ConfigNames.auth,
            );

            return authConfig.jwt;
          },
        }),
        UtilsModule,
        AuthModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();

    app.setGlobalPrefix('api').useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
        transform: true,
      }),
    );

    await app.init();

    jwtService = app.get<JwtService>(JwtService);
    sequelize = app.get<Sequelize>(Sequelize);
    queryInterface = sequelize.getQueryInterface();
    userModel = sequelize.getRepository(User);
    refreshTokenModel = sequelize.getRepository(RefreshToken);
    accessTokenModel = sequelize.getRepository(AccessToken);
    resetPasswordTokenModel = sequelize.getRepository(ResetPasswordToken);
    configService = app.get<ConfigService>(ConfigService);
  });

  describe('Sign up trader (POST /api/auth/sign/up/trader)', () => {
    const validInput = {
      email: 'name@mail.com',
      password: 'password1',
      passwordRepeat: 'password1',
      name: 'name',
      telegram: 'telegram',
    };

    beforeEach(async () => {
      await queryInterface.bulkDelete('users', {});
      await queryInterface.bulkDelete('roles', {});
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.trader,
        },
      ]);
    });

    it('Should fail with invalid input', async () => {
      const invalidInput = {
        email: '',
        password: '',
        passwordRepeat: 'q',
        name: '',
        telegram: '',
      };
      const { body } = await request(app.getHttpServer())
        .post('/api/auth/sign/up/trader')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(invalidInput)
        .expect(HttpStatus.BAD_REQUEST);
      const createdUser: User = await userModel.findOne({
        where: {
          email: invalidInput.email,
        },
      });

      expect(body).toEqual({
        messages: [
          validationErrorMessages.email.format,
          validationErrorMessages.email.length.min,
          validationErrorMessages.password.strength,
          validationErrorMessages.password.length.min,
          validationErrorMessages.passwordRepeat.match,
          validationErrorMessages.name.length.min,
          validationErrorMessages.telegram.length.min,
        ],
      });
      expect(createdUser).toBe(null);
    });

    it('Should fail for existing user', async () => {
      await userModel.create({
        email: validInput.email,
        role: Roles.trader,
      });

      const { body } = await request(app.getHttpServer())
        .post('/api/auth/sign/up/trader')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toEqual({
        messages: [userErrorMessages.existence.exists],
      });
    });

    it('Should success with valid input', async () => {
      const { body } = await request(app.getHttpServer())
        .post('/api/auth/sign/up/trader')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .expect(HttpStatus.CREATED);

      expect(typeof body.accessToken === 'string');
      expect(typeof body.refreshToken === 'string');

      const createdUser: User = await userModel.findOne({
        where: {
          email: validInput.email,
        },
      });
      const accessTokenPayload: any = jwtService.decode(body.accessToken);
      const refreshTokenPayload: any = jwtService.decode(body.refreshToken);
      const createdAccessToken: AccessToken = await accessTokenModel.findByPk(
        accessTokenPayload.id,
      );
      const createdRefreshToken: RefreshToken =
        await refreshTokenModel.findByPk(refreshTokenPayload.id);

      expect(createdUser).not.toBe(null);
      expect(createdAccessToken).not.toBe(null);
      expect(createdRefreshToken).not.toBe(null);
    });
  });

  describe('Sign in (POST /api/auth/sign/in)', () => {
    const validInput = {
      email: 'name@mail.com',
      password: 'password1',
    };

    let user: User;

    beforeEach(async () => {
      await queryInterface.bulkDelete('users', {});
      await queryInterface.bulkDelete('roles', {});
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.merchant,
        },
      ]);

      user = await userModel.create({
        email: validInput.email,
        password: await bcrypt.hash(validInput.password, 7),
        role: Roles.merchant,
        confirmed: true,
      });
    });

    it('Should fail with invalid input', async () => {
      const invalidInput = {
        email: '',
        password: '',
      };
      const { body } = await request(app.getHttpServer())
        .post('/api/auth/sign/in')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(invalidInput)
        .expect(HttpStatus.BAD_REQUEST);
      const refreshTokensCount: number = await refreshTokenModel.count({
        where: {
          userId: user.id,
        },
      });
      const accessTokensCount: number = await accessTokenModel.count({
        where: {
          userId: user.id,
        },
      });

      expect(body).toEqual({
        messages: [
          validationErrorMessages.email.format,
          validationErrorMessages.email.length.min,
          validationErrorMessages.password.length.min,
        ],
      });
      expect(refreshTokensCount).toBe(0);
      expect(accessTokensCount).toBe(0);
    });

    it('Should fail for not confirmed merchant', async () => {
      await user.update({
        confirmed: false,
      });

      const { body } = await request(app.getHttpServer())
        .post('/api/auth/sign/in')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .expect(HttpStatus.BAD_REQUEST);
      const refreshTokensCount: number = await refreshTokenModel.count({
        where: {
          userId: user.id,
        },
      });
      const accessTokensCount: number = await accessTokenModel.count({
        where: {
          userId: user.id,
        },
      });

      expect(body).toEqual({
        messages: [userErrorMessages.confirmation.notConfirmed],
      });
      expect(refreshTokensCount).toBe(0);
      expect(accessTokensCount).toBe(0);
    });

    it('Should fail for not existing user', async () => {
      await user.destroy();

      const { body } = await request(app.getHttpServer())
        .post('/api/auth/sign/in')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .expect(HttpStatus.BAD_REQUEST);
      const refreshTokensCount: number = await refreshTokenModel.count({
        where: {
          userId: user.id,
        },
      });
      const accessTokensCount: number = await accessTokenModel.count({
        where: {
          userId: user.id,
        },
      });

      expect(body).toEqual({
        messages: [authErrorMessages.signIn.emailOrPasswordMissmatch],
      });
      expect(refreshTokensCount).toBe(0);
      expect(accessTokensCount).toBe(0);
    });

    it('Should fail with invalid password', async () => {
      validInput.password = 'password2';

      const { body } = await request(app.getHttpServer())
        .post('/api/auth/sign/in')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .expect(HttpStatus.BAD_REQUEST);
      const refreshTokensCount: number = await refreshTokenModel.count({
        where: {
          userId: user.id,
        },
      });
      const accessTokensCount: number = await accessTokenModel.count({
        where: {
          userId: user.id,
        },
      });

      expect(body).toEqual({
        messages: [authErrorMessages.signIn.emailOrPasswordMissmatch],
      });
      expect(refreshTokensCount).toBe(0);
      expect(accessTokensCount).toBe(0);
    });

    it('Should success with valid input', async () => {
      const { body } = await request(app.getHttpServer())
        .post('/api/auth/sign/in')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .expect(HttpStatus.CREATED);

      expect(typeof body.accessToken === 'string');
      expect(typeof body.refreshToken === 'string');

      const accessTokenPayload: any = jwtService.decode(body.accessToken);
      const refreshTokenPayload: any = jwtService.decode(body.refreshToken);
      const createdAccessToken: AccessToken = await accessTokenModel.findByPk(
        accessTokenPayload.id,
      );
      const createdRefreshToken: RefreshToken =
        await refreshTokenModel.findByPk(refreshTokenPayload.id);

      expect(createdAccessToken).not.toBe(null);
      expect(createdRefreshToken).not.toBe(null);
    });
  });

  describe('Sign out (GET /api/auth/sign/out)', () => {
    let user: User;
    let refreshToken: RefreshToken;
    let accessToken: AccessToken;
    let accessTokenValue: string;

    beforeEach(async () => {
      await queryInterface.bulkDelete('users', {});
      await queryInterface.bulkDelete('roles', {});
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.trader,
        },
      ]);

      const configService: ConfigService =
        app.get<ConfigService>(ConfigService);
      const authConfig: AuthConfig = configService.get<AuthConfig>(
        ConfigNames.auth,
      );

      user = await userModel.create({
        email: 'name@mail.com',
        password: 'password1',
        role: Roles.trader,
      });
      refreshToken = await refreshTokenModel.create({
        userId: user.id,
      });
      accessToken = await accessTokenModel.create({
        userId: user.id,
        refreshTokenId: refreshToken.id,
      });
      accessTokenValue = await jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: authConfig.ttl.access,
        },
      );
    });

    it('Should fail without access token', async () => {
      const { body } = await request(app.getHttpServer())
        .get('/api/auth/sign/out')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect(HttpStatus.UNAUTHORIZED);
      const currentRefreshToken: RefreshToken =
        await refreshTokenModel.findByPk(refreshToken.id);
      const currentAccessToken: AccessToken = await accessTokenModel.findByPk(
        accessToken.id,
      );

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.empty],
      });
      expect(currentRefreshToken).not.toBe(null);
      expect(currentAccessToken).not.toBe(null);
    });

    it('Should fail with invalid access token', async () => {
      const invalidAccessTokenValue: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .get('/api/auth/sign/out')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${invalidAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);
      const currentRefreshToken: RefreshToken =
        await refreshTokenModel.findByPk(refreshToken.id);
      const currentAccessToken: AccessToken = await accessTokenModel.findByPk(
        accessToken.id,
      );

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.invalid],
      });
      expect(currentRefreshToken).not.toBe(null);
      expect(currentAccessToken).not.toBe(null);
    });

    it('Should fail with expired access token', async () => {
      const expiredAccessTokenValue: string = await jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: 0,
        },
      );
      const { body } = await request(app.getHttpServer())
        .get('/api/auth/sign/out')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${expiredAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);
      const currentRefreshToken: RefreshToken =
        await refreshTokenModel.findByPk(refreshToken.id);
      const currentAccessToken: AccessToken = await accessTokenModel.findByPk(
        accessToken.id,
      );

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.expired],
      });
      expect(currentRefreshToken).toBe(null);
      expect(currentAccessToken).toBe(null);
    });

    it('Should success with valid access token', async () => {
      const { body } = await request(app.getHttpServer())
        .get('/api/auth/sign/out')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.OK);
      const currentRefreshToken: RefreshToken =
        await refreshTokenModel.findByPk(refreshToken.id);
      const currentAccessToken: AccessToken = await accessTokenModel.findByPk(
        accessToken.id,
      );

      expect(body).toEqual({});
      expect(currentRefreshToken).toBe(null);
      expect(currentAccessToken).toBe(null);
    });
  });

  describe('Reclaim auth tokens (POST /api/auth/tokens/reclaim)', () => {
    let user: User;
    let refreshToken: RefreshToken;
    let accessToken: AccessToken;
    let refreshTokenValue: string;

    beforeEach(async () => {
      await queryInterface.bulkDelete('users', {});
      await queryInterface.bulkDelete('roles', {});
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.trader,
        },
      ]);

      const configService: ConfigService =
        app.get<ConfigService>(ConfigService);
      const authConfig: AuthConfig = configService.get<AuthConfig>(
        ConfigNames.auth,
      );

      user = await userModel.create({
        email: 'name@mail.com',
        password: 'password1',
        role: Roles.trader,
      });
      refreshToken = await refreshTokenModel.create({
        userId: user.id,
      });
      accessToken = await accessTokenModel.create({
        userId: user.id,
        refreshTokenId: refreshToken.id,
      });
      refreshTokenValue = await jwtService.signAsync(
        {
          id: refreshToken.id,
        },
        {
          expiresIn: authConfig.ttl.refresh,
        },
      );
    });

    it('Should fail without refresh token', async () => {
      const { body } = await request(app.getHttpServer())
        .post('/api/auth/tokens/reclaim')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect(HttpStatus.BAD_REQUEST);
      const currentRefreshToken: RefreshToken =
        await refreshTokenModel.findByPk(refreshToken.id);
      const currentAccessToken: AccessToken = await accessTokenModel.findByPk(
        accessToken.id,
      );

      expect(body).toEqual({
        messages: [
          validationErrorMessages.refreshToken.length.max,
          validationErrorMessages.refreshToken.length.min,
        ],
      });
      expect(currentRefreshToken).not.toBe(null);
      expect(currentAccessToken).not.toBe(null);
    });

    it('Should fail with invalid refresh token', async () => {
      const invalidRefreshTokenValue: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .post('/api/auth/tokens/reclaim')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send({
          refreshToken: invalidRefreshTokenValue,
        })
        .expect(HttpStatus.BAD_REQUEST);
      const currentRefreshToken: RefreshToken =
        await refreshTokenModel.findByPk(refreshToken.id);
      const currentAccessToken: AccessToken = await accessTokenModel.findByPk(
        accessToken.id,
      );

      expect(body).toEqual({
        messages: [validationErrorMessages.refreshToken.invalid],
      });
      expect(currentRefreshToken).not.toBe(null);
      expect(currentAccessToken).not.toBe(null);
    });

    it('Should fail with expired refresh token', async () => {
      const expiredRefreshTokenValue: string = await jwtService.signAsync(
        {
          id: refreshToken.id,
        },
        {
          expiresIn: 0,
        },
      );
      const { body } = await request(app.getHttpServer())
        .post('/api/auth/tokens/reclaim')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send({
          refreshToken: expiredRefreshTokenValue,
        })
        .expect(HttpStatus.BAD_REQUEST);
      const currentRefreshToken: RefreshToken =
        await refreshTokenModel.findByPk(refreshToken.id);
      const currentAccessToken: AccessToken = await accessTokenModel.findByPk(
        accessToken.id,
      );

      expect(body).toEqual({
        messages: [validationErrorMessages.refreshToken.expired],
      });
      expect(currentRefreshToken).toBe(null);
      expect(currentAccessToken).toBe(null);
    });

    it('Should success with valid refresh token', async () => {
      const { body } = await request(app.getHttpServer())
        .post('/api/auth/tokens/reclaim')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send({
          refreshToken: refreshTokenValue,
        })
        .expect(HttpStatus.CREATED);

      expect(typeof body.accessToken === 'string');
      expect(typeof body.refreshToken === 'string');

      const currentRefreshToken: RefreshToken =
        await refreshTokenModel.findByPk(refreshToken.id);
      const currentAccessToken: AccessToken = await accessTokenModel.findByPk(
        accessToken.id,
      );

      expect(currentRefreshToken).toBe(null);
      expect(currentAccessToken).toBe(null);

      const accessTokenPayload: any = jwtService.decode(body.accessToken);
      const refreshTokenPayload: any = jwtService.decode(body.refreshToken);
      const createdAccessToken: AccessToken = await accessTokenModel.findByPk(
        accessTokenPayload.id,
      );
      const createdRefreshToken: RefreshToken =
        await refreshTokenModel.findByPk(refreshTokenPayload.id);

      expect(createdAccessToken).not.toBe(null);
      expect(createdRefreshToken).not.toBe(null);
    });
  });

  describe('Restore password (POST /api/auth/password/restore)', () => {
    const validInput: any = {
      email: 'trader@mail.com',
    };

    let user: User;

    beforeEach(async () => {
      await queryInterface.bulkDelete('users', {});
      await queryInterface.bulkDelete('roles', {});
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.trader,
        },
      ]);

      user = await userModel.create({
        email: validInput.email,
        role: Roles.trader,
      });
    });

    it('Should fail with invalid input', async () => {
      const invalidInput: any = {};
      const { body } = await request(app.getHttpServer())
        .post('/api/auth/password/reset')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(invalidInput)
        .expect(HttpStatus.BAD_REQUEST);
      const createdResetPasswordToken: ResetPasswordToken =
        await user.$get('resetPasswordToken');

      expect(body).toEqual({
        messages: [
          validationErrorMessages.email.format,
          validationErrorMessages.email.length.max,
          validationErrorMessages.email.length.min,
        ],
      });
      expect(createdResetPasswordToken).toBe(null);
    });

    it('Should fail for not confirmed merchant', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.merchant,
        },
      ]);
      await user.update({
        role: Roles.merchant,
      });

      const { body } = await request(app.getHttpServer())
        .post('/api/auth/password/reset')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .expect(HttpStatus.BAD_REQUEST);
      const createdResetPasswordToken: ResetPasswordToken =
        await user.$get('resetPasswordToken');

      expect(body).toEqual({
        messages: [userErrorMessages.confirmation.notConfirmed],
      });
      expect(createdResetPasswordToken).toBe(null);
    });

    it('Should fail with existing request', async () => {
      await user.$create('resetPasswordToken', {});

      const { body } = await request(app.getHttpServer())
        .post('/api/auth/password/reset')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .expect(HttpStatus.BAD_REQUEST);

      expect(body).toEqual({
        messages: [authErrorMessages.restorePassword.requestExists],
      });
    });

    it('Should success with valid input', async () => {
      const { body } = await request(app.getHttpServer())
        .post('/api/auth/password/reset')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .expect(HttpStatus.CREATED);
      const createdResetPasswordToken: ResetPasswordToken =
        await user.$get('resetPasswordToken');

      expect(body).toEqual({});
      expect(createdResetPasswordToken).not.toBe(null);
    });
  });

  describe('Restore password confirm (POST /api/auth/password/confirm)', () => {
    let user: User;
    let resetPasswordToken: ResetPasswordToken;
    let resetPasswordTokenValue: string;
    let validInput: any;

    beforeEach(async () => {
      await queryInterface.bulkDelete('users', {});
      await queryInterface.bulkDelete('roles', {});
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.trader,
        },
      ]);

      const authConfig: AuthConfig = configService.get<AuthConfig>(
        ConfigNames.auth,
      );

      user = await userModel.create({
        email: 'trader@mail.com',
        role: Roles.trader,
      });
      resetPasswordToken = await user.$create('resetPasswordToken', {});
      resetPasswordTokenValue = await jwtService.signAsync(
        {
          id: resetPasswordToken.id,
        },
        {
          expiresIn: authConfig.ttl.resetPassword,
        },
      );
      validInput = {
        token: resetPasswordTokenValue,
        newPassword: 'password1',
        newPasswordRepeat: 'password1',
      };
    });

    it('Should fail with invalid input', async () => {
      const invalidInput: any = {
        newPasswordRepeat: 'qwe',
      };
      const { body } = await request(app.getHttpServer())
        .post('/api/auth/password/reset/confirm')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(invalidInput)
        .expect(HttpStatus.BAD_REQUEST);
      const updatedUser: User = await userModel.findByPk(user.id);
      const currentResetPasswordToken: ResetPasswordToken =
        await resetPasswordTokenModel.findByPk(resetPasswordToken.id);

      expect(body).toEqual({
        messages: [
          validationErrorMessages.resetPasswordConfirmToken.length.min,
          validationErrorMessages.resetPasswordConfirmToken.length.max,
          validationErrorMessages.password.strength,
          validationErrorMessages.password.length.min,
          validationErrorMessages.password.length.max,
          validationErrorMessages.passwordRepeat.match,
        ],
      });
      expect(updatedUser.password).toBe(user.password);
      expect(currentResetPasswordToken).not.toBe(null);
    });

    it('Should fail with invalid token', async () => {
      validInput.token = 'qwe';

      const { body } = await request(app.getHttpServer())
        .post('/api/auth/password/reset/confirm')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .expect(HttpStatus.BAD_REQUEST);
      const updatedUser: User = await userModel.findByPk(user.id);
      const currentResetPasswordToken: ResetPasswordToken =
        await resetPasswordTokenModel.findByPk(resetPasswordToken.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.resetPasswordConfirmToken.invalid],
      });
      expect(updatedUser.password).toBe(user.password);
      expect(currentResetPasswordToken).not.toBe(null);
    });

    it('Should fail with expired token', async () => {
      validInput.token = await jwtService.signAsync(
        {
          id: resetPasswordToken.id,
        },
        {
          expiresIn: 0,
        },
      );

      const { body } = await request(app.getHttpServer())
        .post('/api/auth/password/reset/confirm')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .expect(HttpStatus.BAD_REQUEST);
      const updatedUser: User = await userModel.findByPk(user.id);
      const currentResetPasswordToken: ResetPasswordToken =
        await resetPasswordTokenModel.findByPk(resetPasswordToken.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.resetPasswordConfirmToken.expired],
      });
      expect(updatedUser.password).toBe(user.password);
      expect(currentResetPasswordToken).toBe(null);
    });

    it('Should fail for not confirmed merchant', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.merchant,
        },
      ]);
      await user.update({
        role: Roles.merchant,
      });

      const { body } = await request(app.getHttpServer())
        .post('/api/auth/password/reset/confirm')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .expect(HttpStatus.BAD_REQUEST);
      const updatedUser: User = await userModel.findByPk(user.id);
      const currentResetPasswordToken: ResetPasswordToken =
        await resetPasswordTokenModel.findByPk(resetPasswordToken.id);

      expect(body).toEqual({
        messages: [userErrorMessages.confirmation.notConfirmed],
      });
      expect(updatedUser.password).toBe(user.password);
      expect(currentResetPasswordToken).not.toBe(null);
    });

    it('Should success with valid input', async () => {
      const { body } = await request(app.getHttpServer())
        .post('/api/auth/password/reset/confirm')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .expect(HttpStatus.CREATED);
      const updatedUser: User = await userModel.findByPk(user.id);
      const currentResetPasswordToken: ResetPasswordToken =
        await resetPasswordTokenModel.findByPk(resetPasswordToken.id);

      expect(typeof body.accessToken).toBe('string');
      expect(typeof body.refreshToken).toBe('string');
      expect(updatedUser.password).not.toBe(user.password);
      expect(currentResetPasswordToken).toBe(null);
      expect(bcrypt.compare(validInput.newPassword, updatedUser.password));
    });
  });

  afterAll(async () => {
    await queryInterface.bulkDelete('users', {});
    await queryInterface.bulkDelete('roles', {});
    await app.close();
  });
});

describe('User (e2e)', () => {
  let app: INestApplication;
  let jwtService: JwtService;
  let sequelize: Sequelize;
  let queryInterface: QueryInterface;
  let userModel: typeof User;
  let refreshTokenModel: typeof RefreshToken;
  let accessTokenModel: typeof AccessToken;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          validate: validateConfig,
          load: [loadConfig],
        }),
        SequelizeModule.forRootAsync({
          inject: [ConfigService],
          useFactory(configService: ConfigService): SequelizeModuleOptions {
            const dbConfig: SequelizeModuleOptions =
              configService.get<SequelizeModuleOptions>(ConfigNames.db);

            dbConfig.logging = false;

            return dbConfig;
          },
        }),
        JwtModule.registerAsync({
          global: true,
          inject: [ConfigService],
          async useFactory(configService: ConfigService) {
            const authConfig: AuthConfig = configService.get<AuthConfig>(
              ConfigNames.auth,
            );

            return authConfig.jwt;
          },
        }),
        UtilsModule,
        UserModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();

    app.setGlobalPrefix('api').useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
        transform: true,
      }),
    );

    await app.init();

    jwtService = app.get<JwtService>(JwtService);
    sequelize = app.get<Sequelize>(Sequelize);
    queryInterface = sequelize.getQueryInterface();
    userModel = sequelize.getRepository(User);
    refreshTokenModel = sequelize.getRepository(RefreshToken);
    accessTokenModel = sequelize.getRepository(AccessToken);
  });

  describe('Confirm user (POST /api/users/:id/confirm)', () => {
    let admin: User;
    let merchant: User;
    let refreshToken: RefreshToken;
    let accessToken: AccessToken;
    let accessTokenValue: string;

    beforeEach(async () => {
      await queryInterface.bulkDelete('users', {});
      await queryInterface.bulkDelete('roles', {});
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.admin,
        },
        {
          name: Roles.merchant,
        },
      ]);

      const configService: ConfigService =
        app.get<ConfigService>(ConfigService);
      const authConfig: AuthConfig = configService.get<AuthConfig>(
        ConfigNames.auth,
      );

      admin = await userModel.create({
        email: 'admin@mail.com',
        role: Roles.admin,
      });
      merchant = await userModel.create({
        email: 'merchant@mail.com',
        role: Roles.merchant,
      });
      refreshToken = await refreshTokenModel.create({
        userId: admin.id,
      });
      accessToken = await accessTokenModel.create({
        userId: admin.id,
        refreshTokenId: refreshToken.id,
      });
      accessTokenValue = await jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: authConfig.ttl.access,
        },
      );
    });

    it('Should fail without access token', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/users/${merchant.id}/confirm`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect(HttpStatus.UNAUTHORIZED);
      const updatedMerchant: User = await merchant.reload();

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.empty],
      });
      expect(updatedMerchant.confirmed).toBe(false);
    });

    it('Should fail with invalid access token', async () => {
      const invalidAccessTokenValue: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .post(`/api/users/${merchant.id}/confirm`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${invalidAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);
      const updatedMerchant: User = await merchant.reload();

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.invalid],
      });
      expect(updatedMerchant.confirmed).toBe(false);
    });

    it('Should fail with expired access token', async () => {
      const expiredAccessTokenValue: string = await jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: 0,
        },
      );
      const { body } = await request(app.getHttpServer())
        .post(`/api/users/${merchant.id}/confirm`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${expiredAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);
      const updatedMerchant: User = await merchant.reload();

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.expired],
      });
      expect(updatedMerchant.confirmed).toBe(false);
    });

    it('Should fail for moderator', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.moderator,
        },
      ]);
      await admin.update({
        role: Roles.moderator,
      });

      const { body } = await request(app.getHttpServer())
        .post(`/api/users/${merchant.id}/confirm`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);
      const updatedMerchant: User = await merchant.reload();

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
      expect(updatedMerchant.confirmed).toBe(false);
    });

    it('Should fail for trader', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.trader,
        },
      ]);
      await admin.update({
        role: Roles.trader,
      });

      const { body } = await request(app.getHttpServer())
        .post(`/api/users/${merchant.id}/confirm`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);
      const updatedMerchant: User = await merchant.reload();

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
      expect(updatedMerchant.confirmed).toBe(false);
    });

    it('Should fail for merchant', async () => {
      await admin.update({
        role: Roles.merchant,
      });

      const { body } = await request(app.getHttpServer())
        .post(`/api/users/${merchant.id}/confirm`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);
      const updatedMerchant: User = await merchant.reload();

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
      expect(updatedMerchant.confirmed).toBe(false);
    });

    it('Should fail for not existing user', async () => {
      await merchant.destroy();

      const { body } = await request(app.getHttpServer())
        .post(`/api/users/${merchant.id}/confirm`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toEqual({
        messages: [userErrorMessages.existence.notExists],
      });
    });

    it('Should fail to confirmed user', async () => {
      await merchant.update({
        confirmed: true,
      });

      const { body } = await request(app.getHttpServer())
        .post(`/api/users/${merchant.id}/confirm`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.BAD_REQUEST);
      const updatedMerchant: User = await merchant.reload();

      expect(body).toEqual({
        messages: [userErrorMessages.confirmation.confirmed],
      });
      expect(updatedMerchant.confirmed).toBe(true);
    });

    it('Should fail with invalid id', async () => {
      const invalidId: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .post(`/api/users/${invalidId}/confirm`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.BAD_REQUEST);

      expect(body).toEqual({
        messages: [
          validationErrorMessages.id.int,
          validationErrorMessages.id.min,
        ],
      });
    });

    it('Should succes with valid input', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/users/${merchant.id}/confirm`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.CREATED);
      const updatedMerchant: User = await userModel.findByPk(merchant.id);

      expect(body).toEqual({});
      expect(updatedMerchant.confirmed).toBe(true);
    });
  });

  describe('Get current user details (GET /api/users/me)', () => {
    let user: User;
    let refreshToken: RefreshToken;
    let accessToken: AccessToken;
    let accessTokenValue: string;

    beforeEach(async () => {
      await queryInterface.bulkDelete('users', {});
      await queryInterface.bulkDelete('roles', {});
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.trader,
        },
      ]);

      const configService: ConfigService =
        app.get<ConfigService>(ConfigService);
      const authConfig: AuthConfig = configService.get<AuthConfig>(
        ConfigNames.auth,
      );

      user = await userModel.create({
        email: 'trader@mail.com',
        role: Roles.trader,
      });
      refreshToken = await refreshTokenModel.create({
        userId: user.id,
      });
      accessToken = await accessTokenModel.create({
        userId: user.id,
        refreshTokenId: refreshToken.id,
      });
      accessTokenValue = await jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: authConfig.ttl.access,
        },
      );
    });

    it('Should fail without access token', async () => {
      const { body } = await request(app.getHttpServer())
        .get(`/api/users/me`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.empty],
      });
    });

    it('Should fail with invalid access token', async () => {
      const invalidAccessTokenValue: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .get(`/api/users/me`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${invalidAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.invalid],
      });
    });

    it('Should fail with expired access token', async () => {
      const expiredAccessTokenValue: string = await jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: 0,
        },
      );
      const { body } = await request(app.getHttpServer())
        .get(`/api/users/me`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${expiredAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.expired],
      });
    });

    it('Should success with valid access token', async () => {
      const { body } = await request(app.getHttpServer())
        .get(`/api/users/me`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.OK);

      expect(body).toEqual(user.serialize());
    });
  });

  describe('Edit profile (PUT /api/users/me)', () => {
    const validInput: any = {
      email: 'trader2@mail.com',
      name: 'name',
      telegram: 'telegram',
      phone: '+380971590323',
      birthDate: '07.11.1994',
    };

    let user: User;
    let refreshToken: RefreshToken;
    let accessToken: AccessToken;
    let accessTokenValue: string;

    beforeEach(async () => {
      await queryInterface.bulkDelete('users', {});
      await queryInterface.bulkDelete('roles', {});
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.trader,
        },
      ]);

      const configService: ConfigService =
        app.get<ConfigService>(ConfigService);
      const authConfig: AuthConfig = configService.get<AuthConfig>(
        ConfigNames.auth,
      );

      user = await userModel.create({
        email: 'admin@mail.com',
        role: Roles.trader,
      });
      refreshToken = await refreshTokenModel.create({
        userId: user.id,
      });
      accessToken = await accessTokenModel.create({
        userId: user.id,
        refreshTokenId: refreshToken.id,
      });
      accessTokenValue = await jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: authConfig.ttl.access,
        },
      );
    });

    it('Should fail without access token', async () => {
      const { body } = await request(app.getHttpServer())
        .put(`/api/users/me`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .expect(HttpStatus.UNAUTHORIZED);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.empty],
      });
      expect(
        updatedUser.get({
          plain: true,
        }),
      ).toEqual(
        user.get({
          plain: true,
        }),
      );
    });

    it('Should fail with invalid access token', async () => {
      const invalidAccessTokenValue: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .put(`/api/users/me`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .set('Authorization', `Bearer ${invalidAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.invalid],
      });
      expect(
        updatedUser.get({
          plain: true,
        }),
      ).toEqual(
        user.get({
          plain: true,
        }),
      );
    });

    it('Should fail with expired access token', async () => {
      const expiredAccessTokenValue: string = await jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: 0,
        },
      );
      const { body } = await request(app.getHttpServer())
        .put(`/api/users/me`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .set('Authorization', `Bearer ${expiredAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.expired],
      });
      expect(
        updatedUser.get({
          plain: true,
        }),
      ).toEqual(
        user.get({
          plain: true,
        }),
      );
    });

    it('Should fail for admin', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.admin,
        },
      ]);
      await user.update({
        role: Roles.admin,
      });

      const { body } = await request(app.getHttpServer())
        .put(`/api/users/me`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
      expect(
        updatedUser.get({
          plain: true,
        }),
      ).toEqual(
        user.get({
          plain: true,
        }),
      );
    });

    it('Should fail for moderator', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.moderator,
        },
      ]);
      await user.update({
        role: Roles.moderator,
      });

      const { body } = await request(app.getHttpServer())
        .put(`/api/users/me`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
      expect(
        updatedUser.get({
          plain: true,
        }),
      ).toEqual(
        user.get({
          plain: true,
        }),
      );
    });

    it('Should fail with invalid input', async () => {
      const invalidInput: any = {
        newPassword: '',
      };
      const { body } = await request(app.getHttpServer())
        .put(`/api/users/me`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(invalidInput)
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.BAD_REQUEST);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual({
        messages: [
          validationErrorMessages.email.format,
          validationErrorMessages.name.length.min,
          validationErrorMessages.name.length.max,
          validationErrorMessages.telegram.length.min,
          validationErrorMessages.telegram.length.max,
          'validation.IS_DATE_ONLY',
          'validation.IS_PHONE_NUMBER',
          validationErrorMessages.currentPassword.missing,
          validationErrorMessages.password.length.min,
          validationErrorMessages.password.strength,
          validationErrorMessages.passwordRepeat.missing,
          validationErrorMessages.passwordRepeat.match,
        ],
      });
      expect(
        updatedUser.get({
          plain: true,
        }),
      ).toEqual(
        user.get({
          plain: true,
        }),
      );
    });

    it('Should fail with invalid current password', async () => {
      await user.update({
        password: await bcrypt.hash('password1', 7),
      });

      const { body } = await request(app.getHttpServer())
        .put(`/api/users/me`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send({
          ...validInput,
          currentPassword: 'password2',
          newPassword: 'password2',
          newPasswordRepeat: 'password2',
        })
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.BAD_REQUEST);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.currentPassword.missmatch],
      });
      expect(
        updatedUser.get({
          plain: true,
        }),
      ).toEqual(
        user.get({
          plain: true,
        }),
      );
    });

    it('Should fail for existing email', async () => {
      await userModel.create({
        email: validInput.email,
        role: Roles.trader,
      });

      const { body } = await request(app.getHttpServer())
        .put(`/api/users/me`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual({
        messages: [userErrorMessages.existence.exists],
      });
      expect(
        updatedUser.get({
          plain: true,
        }),
      ).toEqual(
        user.get({
          plain: true,
        }),
      );
    });

    it('Should success with valid input', async () => {
      await user.update({
        password: await bcrypt.hash('password1', 7),
      });

      validInput.currentPassword = 'password1';
      validInput.newPassword = 'password2';
      validInput.newPasswordRepeat = 'password2';

      const { body } = await request(app.getHttpServer())
        .put(`/api/users/me`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.OK);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual(updatedUser.serialize());
      expect(updatedUser.email).toBe(validInput.email);
      expect(updatedUser.name).toBe(validInput.name);
      expect(updatedUser.telegram).toBe(validInput.telegram);
      expect(updatedUser.phone).toBe(validInput.phone);
      expect(updatedUser.birthDate).toBe(validInput.birthDate);
      expect(updatedUser.email).toBe(validInput.email);
      expect(
        await bcrypt.compare(validInput.newPassword, updatedUser.password),
      ).toBe(true);
    });
  });

  describe('Upload avatar (POST /api/users/me/avatar)', () => {
    let user: User;
    let refreshToken: RefreshToken;
    let accessToken: AccessToken;
    let accessTokenValue: string;
    let validFileBuffer: Buffer;

    beforeAll(async () => {
      validFileBuffer = await fsPromise.readFile(
        path.join(process.cwd(), 'test/test-uploads/valid-avatar.jpg'),
      );
    });

    beforeEach(async () => {
      await queryInterface.bulkDelete('users', {});
      await queryInterface.bulkDelete('roles', {});
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.trader,
        },
      ]);

      const configService: ConfigService =
        app.get<ConfigService>(ConfigService);
      const authConfig: AuthConfig = configService.get<AuthConfig>(
        ConfigNames.auth,
      );

      user = await userModel.create({
        email: 'admin@mail.com',
        role: Roles.trader,
      });
      refreshToken = await refreshTokenModel.create({
        userId: user.id,
      });
      accessToken = await accessTokenModel.create({
        userId: user.id,
        refreshTokenId: refreshToken.id,
      });
      accessTokenValue = await jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: authConfig.ttl.access,
        },
      );
    });

    it('Should fail without access token', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/users/me/avatar`)
        .set('Accept', 'application/json')
        .attach('avatar', validFileBuffer, 'valid-avatar.jpg')
        .expect(HttpStatus.UNAUTHORIZED);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.empty],
      });
      expect(updatedUser.avatar).toBe(null);
    });

    it('Should fail with invalid access token', async () => {
      const invalidAccessTokenValue: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .post(`/api/users/me/avatar`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${invalidAccessTokenValue}`)
        .attach('avatar', validFileBuffer, 'valid-avatar.jpg')
        .expect(HttpStatus.UNAUTHORIZED);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.invalid],
      });
      expect(updatedUser.avatar).toBe(null);
      expect(
        fs.existsSync(
          path.join(
            process.cwd(),
            `storage/public/avatars/avatar-${user.id}.jpg`,
          ),
        ),
      ).toBe(false);
    });

    it('Should fail with expired access token', async () => {
      const expiredAccessTokenValue: string = await jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: 0,
        },
      );
      const { body } = await request(app.getHttpServer())
        .post(`/api/users/me/avatar`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${expiredAccessTokenValue}`)
        .attach('avatar', validFileBuffer, 'valid-avatar.jpg')
        .expect(HttpStatus.UNAUTHORIZED);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.expired],
      });
      expect(updatedUser.avatar).toBe(null);
      expect(
        fs.existsSync(
          path.join(
            process.cwd(),
            `storage/public/avatars/avatar-${user.id}.jpg`,
          ),
        ),
      ).toBe(false);
    });

    it('Should fail for admin', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.admin,
        },
      ]);
      await user.update({
        role: Roles.admin,
      });

      const { body } = await request(app.getHttpServer())
        .post(`/api/users/me/avatar`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .attach('avatar', validFileBuffer, 'valid-avatar.jpg')
        .expect(HttpStatus.NOT_FOUND);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
      expect(updatedUser.avatar).toBe(null);
      expect(
        fs.existsSync(
          path.join(
            process.cwd(),
            `storage/public/avatars/avatar-${user.id}.jpg`,
          ),
        ),
      ).toBe(false);
    });

    it('Should fail for moderator', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.moderator,
        },
      ]);
      await user.update({
        role: Roles.moderator,
      });

      const { body } = await request(app.getHttpServer())
        .post(`/api/users/me/avatar`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .attach('avatar', validFileBuffer, 'valid-avatar.jpg')
        .expect(HttpStatus.NOT_FOUND);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
      expect(updatedUser.avatar).toBe(null);
      expect(
        fs.existsSync(
          path.join(
            process.cwd(),
            `storage/public/avatars/avatar-${user.id}.jpg`,
          ),
        ),
      ).toBe(false);
    });

    it('Should fail for too large file', async () => {
      const tooLargeFileBuffer: Buffer = await fsPromise.readFile(
        path.join(process.cwd(), 'test/test-uploads/too-large-avatar.jpg'),
      );
      const { body } = await request(app.getHttpServer())
        .post(`/api/users/me/avatar`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .attach('avatar', tooLargeFileBuffer, 'too-large-avatar.jpg')
        .expect(HttpStatus.PAYLOAD_TOO_LARGE);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.avatar.size],
      });
      expect(updatedUser.avatar).toBe(null);
      expect(
        fs.existsSync(
          path.join(
            process.cwd(),
            `storage/public/avatars/avatar-${user.id}.jpg`,
          ),
        ),
      ).toBe(false);
    });

    it('Should fail for invalid file format', async () => {
      const invalidTypeFileBuffer: Buffer = await fsPromise.readFile(
        path.join(process.cwd(), 'package.json'),
      );
      const { body } = await request(app.getHttpServer())
        .post(`/api/users/me/avatar`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .set('x-lang', 'debug')
        .attach('avatar', invalidTypeFileBuffer, 'package.json')
        .expect(HttpStatus.BAD_REQUEST);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.avatar.type],
      });
      expect(updatedUser.avatar).toBe(null);
      expect(
        fs.existsSync(
          path.join(
            process.cwd(),
            `storage/public/avatars/avatar-${user.id}.json`,
          ),
        ),
      ).toBe(false);
    });

    it('Should success with valid input', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/users/me/avatar`)
        .set('Accept', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .attach('avatar', validFileBuffer, 'valid-avatar.jpg')
        .expect(HttpStatus.CREATED);
      const updatedUser: User = await userModel.findByPk(user.id);

      expect(body).toEqual({});
      expect(updatedUser.avatar).toBe(
        `\\static${path.sep}avatars${path.sep}avatar-${user.id}.jpg`,
      );
      expect(
        fs.existsSync(
          path.join(
            process.cwd(),
            `uploads${path.sep}free${path.sep}avatars${path.sep}avatar-${user.id}.jpg`,
          ),
        ),
      ).toBe(true);
    });
  });

  afterAll(async () => {
    await queryInterface.bulkDelete('users', {});
    await queryInterface.bulkDelete('roles', {});
    await app.close();
  });
});

describe('Payment offer (e2e)', () => {
  let app: INestApplication;
  let jwtService: JwtService;
  let sequelize: Sequelize;
  let queryInterface: QueryInterface;
  let userModel: typeof User;
  let refreshTokenModel: typeof RefreshToken;
  let accessTokenModel: typeof AccessToken;
  let user: User;
  let refreshToken: RefreshToken;
  let accessToken: AccessToken;
  let paymentTypeModel: typeof PaymentType;
  let paymentOfferModel: typeof PaymentOffer;
  let accessTokenValue: string;
  let paymentType: PaymentType;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          validate: validateConfig,
          load: [loadConfig],
        }),
        SequelizeModule.forRootAsync({
          inject: [ConfigService],
          useFactory(configService: ConfigService): SequelizeModuleOptions {
            const dbConfig: SequelizeModuleOptions =
              configService.get<SequelizeModuleOptions>(ConfigNames.db);

            dbConfig.logging = false;

            return dbConfig;
          },
        }),
        JwtModule.registerAsync({
          global: true,
          inject: [ConfigService],
          async useFactory(configService: ConfigService) {
            const authConfig: AuthConfig = configService.get<AuthConfig>(
              ConfigNames.auth,
            );

            return authConfig.jwt;
          },
        }),
        UtilsModule,
        PaymentOfferModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();

    app.setGlobalPrefix('api').useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
        transform: true,
      }),
    );

    await app.init();

    jwtService = app.get<JwtService>(JwtService);
    sequelize = app.get<Sequelize>(Sequelize);
    queryInterface = sequelize.getQueryInterface();
    userModel = sequelize.getRepository(User);
    refreshTokenModel = sequelize.getRepository(RefreshToken);
    accessTokenModel = sequelize.getRepository(AccessToken);
    paymentTypeModel = sequelize.getRepository(PaymentType);
    paymentOfferModel = sequelize.getRepository(PaymentOffer);
  });

  beforeEach(async () => {
    await Promise.all([
      queryInterface.bulkDelete('users', {}),
      queryInterface.bulkDelete('paymentTypes', {}),
    ]);
    await queryInterface.bulkDelete('roles', {});
    await queryInterface.bulkInsert('roles', [
      {
        name: Roles.trader,
      },
    ]);

    const configService: ConfigService = app.get<ConfigService>(ConfigService);
    const authConfig: AuthConfig = configService.get<AuthConfig>(
      ConfigNames.auth,
    );

    user = await userModel.create({
      email: 'admin@mail.com',
      role: Roles.trader,
    });
    refreshToken = await refreshTokenModel.create({
      userId: user.id,
    });
    accessToken = await accessTokenModel.create({
      userId: user.id,
      refreshTokenId: refreshToken.id,
    });
    accessTokenValue = await jwtService.signAsync(
      {
        id: accessToken.id,
      },
      {
        expiresIn: authConfig.ttl.access,
      },
    );
    paymentType = await paymentTypeModel.create({
      name: 'Private',
    });
  });

  describe('Create payment offer (POST /api/payments/offers)', () => {
    let validInput: any;

    beforeEach(async () => {
      validInput = {
        cardNumber: '378282246310005',
        holderName: 'name',
        paymentTypeId: paymentType.id,
      };
    });

    it('Should fail without access token', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/payments/offers`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .expect(HttpStatus.UNAUTHORIZED);
      const createdPaymentOffer: PaymentOffer = await paymentOfferModel.findOne(
        {
          where: {
            cardNumber: validInput.cardNumber,
          },
        },
      );

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.empty],
      });
      expect(createdPaymentOffer).toBe(null);
    });

    it('Should fail with invalid access token', async () => {
      const invalidAccessTokenValue: string = 'qwe';

      const { body } = await request(app.getHttpServer())
        .post(`/api/payments/offers`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${invalidAccessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.UNAUTHORIZED);
      const createdPaymentOffer: PaymentOffer = await paymentOfferModel.findOne(
        {
          where: {
            cardNumber: validInput.cardNumber,
          },
        },
      );

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.invalid],
      });
      expect(createdPaymentOffer).toBe(null);
    });

    it('Should fail with expired access token', async () => {
      const expiredAccessTokenValue: string = await jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: 0,
        },
      );

      const { body } = await request(app.getHttpServer())
        .post(`/api/payments/offers`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${expiredAccessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.UNAUTHORIZED);
      const createdPaymentOffer: PaymentOffer = await paymentOfferModel.findOne(
        {
          where: {
            cardNumber: validInput.cardNumber,
          },
        },
      );

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.expired],
      });
      expect(createdPaymentOffer).toBe(null);
    });

    it('Should fail for admin', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.admin,
        },
      ]);
      await user.update({
        role: Roles.admin,
      });

      const { body } = await request(app.getHttpServer())
        .post(`/api/payments/offers`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.NOT_FOUND);
      const createdPaymentOffer: PaymentOffer = await paymentOfferModel.findOne(
        {
          where: {
            cardNumber: validInput.cardNumber,
          },
        },
      );

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
      expect(createdPaymentOffer).toBe(null);
    });

    it('Should fail for moderator', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.moderator,
        },
      ]);
      await user.update({
        role: Roles.moderator,
      });

      const { body } = await request(app.getHttpServer())
        .post(`/api/payments/offers`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.NOT_FOUND);
      const createdPaymentOffer: PaymentOffer = await paymentOfferModel.findOne(
        {
          where: {
            cardNumber: validInput.cardNumber,
          },
        },
      );

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
      expect(createdPaymentOffer).toBe(null);
    });

    it('Should fail for merchant', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.merchant,
        },
      ]);
      await user.update({
        role: Roles.merchant,
      });

      const { body } = await request(app.getHttpServer())
        .post(`/api/payments/offers`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.NOT_FOUND);
      const createdPaymentOffer: PaymentOffer = await paymentOfferModel.findOne(
        {
          where: {
            cardNumber: validInput.cardNumber,
          },
        },
      );

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
      expect(createdPaymentOffer).toBe(null);
    });

    it('Should fail with invalid input', async () => {
      const invalidInput = {};
      const { body } = await request(app.getHttpServer())
        .post(`/api/payments/offers`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(invalidInput)
        .expect(HttpStatus.BAD_REQUEST);
      const createdPaymentOffer: PaymentOffer = await paymentOfferModel.findOne(
        {
          where: {
            cardNumber: validInput.cardNumber,
          },
        },
      );

      expect(body).toEqual({
        messages: [
          validationErrorMessages.cardNumber.format,
          validationErrorMessages.name.length.max,
          validationErrorMessages.name.length.min,
          validationErrorMessages.id.int,
          validationErrorMessages.id.min,
        ],
      });
      expect(createdPaymentOffer).toBe(null);
    });

    it('Should fail for not existing payment type', async () => {
      await paymentType.destroy();

      const { body } = await request(app.getHttpServer())
        .post(`/api/payments/offers`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.NOT_FOUND);
      const createdPaymentOffer: PaymentOffer = await paymentOfferModel.findOne(
        {
          where: {
            cardNumber: validInput.cardNumber,
          },
        },
      );

      expect(body).toEqual({
        messages: [paymentTypeErrorMessages.existence.notExists],
      });
      expect(createdPaymentOffer).toBe(null);
    });

    it('Should fail with existing card number', async () => {
      await paymentOfferModel.create({
        cardNumber: validInput.cardNumber,
        holderName: validInput.holderName,
        userId: user.id,
        paymentTypeId: validInput.paymentTypeId,
      });

      const { body } = await request(app.getHttpServer())
        .post(`/api/payments/offers`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(body).toEqual({
        messages: [paymentOfferErrorMessages.existence.exists],
      });
    });

    it('Should success with valid input', async () => {
      const { body } = await request(app.getHttpServer())
        .post(`/api/payments/offers`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.CREATED);
      const createdPaymentOffer: PaymentOffer = await paymentOfferModel.findOne(
        {
          where: {
            cardNumber: validInput.cardNumber,
          },
        },
      );

      expect(createdPaymentOffer).not.toBe(null);
      expect(body).toEqual({
        id: createdPaymentOffer.id,
        cardNumber: createdPaymentOffer.cardNumber,
        holderName: createdPaymentOffer.holderName,
        paymentType: {
          id: paymentType.id,
          name: paymentType.name,
        },
        active: createdPaymentOffer.active,
        createdAt: createdPaymentOffer.createdAt.toISOString(),
        updatedAt: createdPaymentOffer.updatedAt.toISOString(),
      });
    });
  });

  describe('Update payment offer (PUT /api/payments/offers/:id)', () => {
    let validInput: any;
    let paymentOffer: PaymentOffer;

    beforeEach(async () => {
      validInput = {
        cardNumber: '371449635398431',
        holderName: 'name2',
        active: true,
        paymentTypeId: paymentType.id,
      };
      paymentOffer = await paymentOfferModel.create({
        cardNumber: '378282246310005',
        holderName: 'name1',
        userId: user.id,
        paymentTypeId: paymentType.id,
      });
    });

    it('Should fail without access token', async () => {
      const { body } = await request(app.getHttpServer())
        .put(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(validInput)
        .expect(HttpStatus.UNAUTHORIZED);
      const updatedPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.empty],
      });
      expect(
        updatedPaymentOffer.get({
          plain: true,
        }),
      ).toEqual(
        paymentOffer.get({
          plain: true,
        }),
      );
    });

    it('Should fail with invalid access token', async () => {
      const invalidAccessTokenValue: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .put(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${invalidAccessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.UNAUTHORIZED);
      const updatedPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.invalid],
      });
      expect(
        updatedPaymentOffer.get({
          plain: true,
        }),
      ).toEqual(
        paymentOffer.get({
          plain: true,
        }),
      );
    });

    it('Should fail with expired access token', async () => {
      const expiredAccessTokenValue: string = await jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: 0,
        },
      );
      const { body } = await request(app.getHttpServer())
        .put(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${expiredAccessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.UNAUTHORIZED);
      const updatedPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.expired],
      });
      expect(
        updatedPaymentOffer.get({
          plain: true,
        }),
      ).toEqual(
        paymentOffer.get({
          plain: true,
        }),
      );
    });

    it('Should fail for admin', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.admin,
        },
      ]);
      await user.update({
        role: Roles.admin,
      });
      const { body } = await request(app.getHttpServer())
        .put(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.NOT_FOUND);
      const updatedPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
      expect(
        updatedPaymentOffer.get({
          plain: true,
        }),
      ).toEqual(
        paymentOffer.get({
          plain: true,
        }),
      );
    });

    it('Should fail for moderator', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.moderator,
        },
      ]);
      await user.update({
        role: Roles.moderator,
      });
      const { body } = await request(app.getHttpServer())
        .put(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.NOT_FOUND);
      const updatedPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
      expect(
        updatedPaymentOffer.get({
          plain: true,
        }),
      ).toEqual(
        paymentOffer.get({
          plain: true,
        }),
      );
    });

    it('Should fail for merchant', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.merchant,
        },
      ]);
      await user.update({
        role: Roles.merchant,
      });
      const { body } = await request(app.getHttpServer())
        .put(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.NOT_FOUND);
      const updatedPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
      expect(
        updatedPaymentOffer.get({
          plain: true,
        }),
      ).toEqual(
        paymentOffer.get({
          plain: true,
        }),
      );
    });

    it('Should fail with invalid input', async () => {
      const invalidInput = {};
      const { body } = await request(app.getHttpServer())
        .put(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(invalidInput)
        .expect(HttpStatus.BAD_REQUEST);
      const updatedPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [
          validationErrorMessages.cardNumber.format,
          validationErrorMessages.name.length.max,
          validationErrorMessages.name.length.min,
          validationErrorMessages.active.bool,
          validationErrorMessages.id.int,
          validationErrorMessages.id.min,
        ],
      });
      expect(
        updatedPaymentOffer.get({
          plain: true,
        }),
      ).toEqual(
        paymentOffer.get({
          plain: true,
        }),
      );
    });

    it('Should fail with invalid id', async () => {
      const invalidId: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .put(`/api/payments/offers/${invalidId}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.BAD_REQUEST);
      const updatedPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [
          validationErrorMessages.id.int,
          validationErrorMessages.id.min,
        ],
      });
      expect(
        updatedPaymentOffer.get({
          plain: true,
        }),
      ).toEqual(
        paymentOffer.get({
          plain: true,
        }),
      );
    });

    it('Should fail for not existing payment type', async () => {
      validInput.paymentTypeId = paymentType.id + 1;

      const { body } = await request(app.getHttpServer())
        .put(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.NOT_FOUND);
      const updatedPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [paymentTypeErrorMessages.existence.notExists],
      });
      expect(
        updatedPaymentOffer.get({
          plain: true,
        }),
      ).toEqual(
        paymentOffer.get({
          plain: true,
        }),
      );
    });

    it('Should fail for payment offer of another trader', async () => {
      const user2: User = await userModel.create({
        email: 'trader2@mail.com',
        role: Roles.trader,
      });

      await paymentOffer.update({
        userId: user2.id,
      });

      const { body } = await request(app.getHttpServer())
        .put(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.NOT_FOUND);
      const updatedPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [paymentOfferErrorMessages.existence.notExists],
      });
      expect(
        updatedPaymentOffer.get({
          plain: true,
        }),
      ).toEqual(
        paymentOffer.get({
          plain: true,
        }),
      );
    });

    it('Should fail for not existing payment offer', async () => {
      await paymentOffer.destroy();

      const { body } = await request(app.getHttpServer())
        .put(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toEqual({
        messages: [paymentOfferErrorMessages.existence.notExists],
      });
    });

    it('Should fail for existing card number', async () => {
      await paymentOfferModel.create({
        cardNumber: validInput.cardNumber,
        holderName: validInput.holderName,
        userId: user.id,
        paymentTypeId: paymentType.id,
      });

      const { body } = await request(app.getHttpServer())
        .put(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);
      const updatedPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [paymentOfferErrorMessages.existence.exists, ,],
      });
      expect(
        updatedPaymentOffer.get({
          plain: true,
        }),
      ).toEqual(
        paymentOffer.get({
          plain: true,
        }),
      );
    });

    it('SHould success with valid input', async () => {
      const { body } = await request(app.getHttpServer())
        .put(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .send(validInput)
        .expect(HttpStatus.OK);
      const updatedPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        id: updatedPaymentOffer.id,
        cardNumber: updatedPaymentOffer.cardNumber,
        holderName: updatedPaymentOffer.holderName,
        active: updatedPaymentOffer.active,
        paymentType: {
          id: paymentType.id,
          name: paymentType.name,
        },
        createdAt: updatedPaymentOffer.createdAt.toISOString(),
        updatedAt: updatedPaymentOffer.updatedAt.toISOString(),
      });
      expect(
        updatedPaymentOffer.get({
          plain: true,
        }),
      ).not.toEqual(
        paymentOffer.get({
          plain: true,
        }),
      );
    });
  });

  describe('Delete payment offer (DELETE /api/payments/offers/:id)', () => {
    let paymentOffer: PaymentOffer;

    beforeEach(async () => {
      paymentOffer = await paymentOfferModel.create({
        cardNumber: '378282246310005',
        holderName: 'name1',
        userId: user.id,
        paymentTypeId: paymentType.id,
      });
    });

    it('Should fail without access token', async () => {
      const { body } = await request(app.getHttpServer())
        .delete(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect(HttpStatus.UNAUTHORIZED);
      const currentPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.empty],
      });
      expect(currentPaymentOffer).not.toBe(null);
    });

    it('Should fail with invalid access token', async () => {
      const invalidAccessTokenValue: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .delete(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${invalidAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);
      const currentPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.invalid],
      });
      expect(currentPaymentOffer).not.toBe(null);
    });

    it('Should fail with expired access token', async () => {
      const expiredAccessTokenValue: string = await jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: 0,
        },
      );
      const { body } = await request(app.getHttpServer())
        .delete(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${expiredAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);
      const currentPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.expired],
      });
      expect(currentPaymentOffer).not.toBe(null);
    });

    it('Should fail for admin', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.admin,
        },
      ]);
      await user.update({
        role: Roles.admin,
      });

      const { body } = await request(app.getHttpServer())
        .delete(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);
      const currentPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
      expect(currentPaymentOffer).not.toBe(null);
    });

    it('Should fail for moderator', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.moderator,
        },
      ]);
      await user.update({
        role: Roles.moderator,
      });

      const { body } = await request(app.getHttpServer())
        .delete(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);
      const currentPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
      expect(currentPaymentOffer).not.toBe(null);
    });

    it('Should fail for merchant', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.merchant,
        },
      ]);
      await user.update({
        role: Roles.merchant,
      });

      const { body } = await request(app.getHttpServer())
        .delete(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);
      const currentPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
      expect(currentPaymentOffer).not.toBe(null);
    });

    it('Should fail with invalid id', async () => {
      const invalidId: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .delete(`/api/payments/offers/${invalidId}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.BAD_REQUEST);
      const currentPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({
        messages: [
          validationErrorMessages.id.int,
          validationErrorMessages.id.min,
        ],
      });
      expect(currentPaymentOffer).not.toBe(null);
    });

    it('Should fail for not existing payment offer', async () => {
      await paymentOffer.destroy();

      const { body } = await request(app.getHttpServer())
        .delete(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toEqual({
        messages: [paymentOfferErrorMessages.existence.notExists],
      });
    });

    it('Should fail for payment offer of another trader', async () => {
      const user2: User = await userModel.create({
        email: 'trader2@mail.com',
        role: Roles.trader,
      });

      await paymentOffer.update({
        userId: user2.id,
      });

      const { body } = await request(app.getHttpServer())
        .delete(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toEqual({
        messages: [paymentOfferErrorMessages.existence.notExists],
      });
    });

    it('Should success with valid input', async () => {
      const { body } = await request(app.getHttpServer())
        .delete(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.OK);
      const currentPaymentOffer: PaymentOffer =
        await paymentOfferModel.findByPk(paymentOffer.id);

      expect(body).toEqual({});
      expect(currentPaymentOffer).toBe(null);
    });
  });

  describe('Get payment offer (GET /api/payments/offers/:id)', () => {
    let paymentOffer: PaymentOffer;

    beforeEach(async () => {
      paymentOffer = await paymentOfferModel.create({
        cardNumber: '378282246310005',
        holderName: 'name1',
        userId: user.id,
        paymentTypeId: paymentType.id,
      });
    });

    it('Should fail without access token', async () => {
      const { body } = await request(app.getHttpServer())
        .get(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.empty],
      });
    });

    it('Should fail with invalid accesst token', async () => {
      const invalidAccessTokenValue: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .get(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${invalidAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.invalid],
      });
    });

    it('Should fail with expired accesst token', async () => {
      const expiredAccessTokenValue: string = await jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: 0,
        },
      );
      const { body } = await request(app.getHttpServer())
        .get(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${expiredAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.expired],
      });
    });

    it('Should fail for admin', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.admin,
        },
      ]);
      await user.update({
        role: Roles.admin,
      });

      const { body } = await request(app.getHttpServer())
        .get(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
    });

    it('Should fail for moderator', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.moderator,
        },
      ]);
      await user.update({
        role: Roles.moderator,
      });

      const { body } = await request(app.getHttpServer())
        .get(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
    });

    it('Should fail for merchant', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.merchant,
        },
      ]);
      await user.update({
        role: Roles.merchant,
      });

      const { body } = await request(app.getHttpServer())
        .get(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
    });

    it('Should fail with invalid id', async () => {
      const invalidId: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .get(`/api/payments/offers/${invalidId}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.BAD_REQUEST);

      expect(body).toEqual({
        messages: [
          validationErrorMessages.id.int,
          validationErrorMessages.id.min,
        ],
      });
    });

    it('Should fail for not existing payment offer', async () => {
      await paymentOffer.destroy();

      const { body } = await request(app.getHttpServer())
        .get(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toEqual({
        messages: [paymentOfferErrorMessages.existence.notExists],
      });
    });

    it('Should fail for payment offer of another trader', async () => {
      const user2: User = await userModel.create({
        email: 'trader2@mail.com',
        role: Roles.trader,
      });

      await paymentOffer.update({
        userId: user2.id,
      });

      const { body } = await request(app.getHttpServer())
        .get(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toEqual({
        messages: [paymentOfferErrorMessages.existence.notExists],
      });
    });

    it('Should success with valid input', async () => {
      const { body } = await request(app.getHttpServer())
        .get(`/api/payments/offers/${paymentOffer.id}`)
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.OK);

      expect(body).toEqual({
        id: paymentOffer.id,
        cardNumber: paymentOffer.cardNumber,
        holderName: paymentOffer.holderName,
        active: paymentOffer.active,
        paymentType: {
          id: paymentType.id,
          name: paymentType.name,
        },
        createdAt: paymentOffer.createdAt.toISOString(),
        updatedAt: paymentOffer.updatedAt.toISOString(),
      });
    });
  });

  describe('Get payment offers list (GET /api/payment/offers)', () => {
    const validInput = {
      limit: 3,
      offset: 0,
    };

    beforeEach(async () => {
      await queryInterface.bulkInsert('paymentOffers', [
        {
          cardNumber: '378282246310005',
          holderName: 'name',
          userId: user.id,
          paymentTypeId: paymentType.id,
        },
        {
          cardNumber: '371449635398431',
          holderName: 'name',
          userId: user.id,
          paymentTypeId: paymentType.id,
        },
        {
          cardNumber: '378734493671000',
          holderName: 'name',
          userId: user.id,
          paymentTypeId: paymentType.id,
        },
        {
          cardNumber: '30569309025904',
          holderName: 'name',
          userId: user.id,
          paymentTypeId: paymentType.id,
        },
        {
          cardNumber: '38520000023237',
          holderName: 'name',
          userId: user.id,
          paymentTypeId: paymentType.id,
        },
      ]);
    });

    it('Should fail without access token', async () => {
      const { body } = await request(app.getHttpServer())
        .get(
          `/api/payments/offers?limit=${validInput.limit}&offset=${validInput.offset}`,
        )
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.empty],
      });
    });

    it('Should fail with invalid access token', async () => {
      const invalidAccessTokenValue: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .get(
          `/api/payments/offers?limit=${validInput.limit}&offset=${validInput.offset}`,
        )
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${invalidAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.invalid],
      });
    });

    it('Should fail with expired access token', async () => {
      const expiredAccessTokenValue: string = await jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: 0,
        },
      );
      const { body } = await request(app.getHttpServer())
        .get(
          `/api/payments/offers?limit=${validInput.limit}&offset=${validInput.offset}`,
        )
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${expiredAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.expired],
      });
    });

    it('Should fail for admin', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.admin,
        },
      ]);
      await user.update({
        role: Roles.admin,
      });

      const { body } = await request(app.getHttpServer())
        .get(
          `/api/payments/offers?limit=${validInput.limit}&offset=${validInput.offset}`,
        )
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
    });

    it('Should fail for moderator', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.moderator,
        },
      ]);
      await user.update({
        role: Roles.moderator,
      });

      const { body } = await request(app.getHttpServer())
        .get(
          `/api/payments/offers?limit=${validInput.limit}&offset=${validInput.offset}`,
        )
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
    });

    it('Should fail for merchant', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.merchant,
        },
      ]);
      await user.update({
        role: Roles.merchant,
      });

      const { body } = await request(app.getHttpServer())
        .get(
          `/api/payments/offers?limit=${validInput.limit}&offset=${validInput.offset}`,
        )
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
    });

    it('Should success with valid input', async () => {
      const { body } = await request(app.getHttpServer())
        .get(
          `/api/payments/offers?limit=${validInput.limit}&offset=${validInput.offset}`,
        )
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.OK);

      expect(body?.paymentOffers?.length).toBe(3);
      expect(body?.hasMore).toBe(true);
    });

    it('Correct `hasMore`', async () => {
      validInput.offset = 3;

      const { body } = await request(app.getHttpServer())
        .get(
          `/api/payments/offers?limit=${validInput.limit}&offset=${validInput.offset}`,
        )
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.OK);

      expect(body?.paymentOffers?.length).toBe(2);
      expect(body?.hasMore).toBe(false);
    });
  });

  afterAll(async () => {
    await Promise.all([
      queryInterface.bulkDelete('users', {}),
      queryInterface.bulkDelete('paymentTypes', {}),
    ]);
    await queryInterface.bulkDelete('roles', {});
    await app.close();
  });
});

describe('Payments (e2e)', () => {
  let app: INestApplication;
  let jwtService: JwtService;
  let sequelize: Sequelize;
  let queryInterface: QueryInterface;
  let userModel: typeof User;
  let refreshTokenModel: typeof RefreshToken;
  let accessTokenModel: typeof AccessToken;
  let paymentModel: typeof Payment;
  let user: User;
  let refreshToken: RefreshToken;
  let accessToken: AccessToken;
  let accessTokenValue: string;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          validate: validateConfig,
          load: [loadConfig],
        }),
        SequelizeModule.forRootAsync({
          inject: [ConfigService],
          useFactory(configService: ConfigService): SequelizeModuleOptions {
            const dbConfig: SequelizeModuleOptions =
              configService.get<SequelizeModuleOptions>(ConfigNames.db);

            dbConfig.logging = false;

            return dbConfig;
          },
        }),
        JwtModule.registerAsync({
          global: true,
          inject: [ConfigService],
          async useFactory(configService: ConfigService) {
            const authConfig: AuthConfig = configService.get<AuthConfig>(
              ConfigNames.auth,
            );

            return authConfig.jwt;
          },
        }),
        UtilsModule,
        PaymentModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();

    app.setGlobalPrefix('api').useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
        transform: true,
      }),
    );

    await app.init();

    jwtService = app.get<JwtService>(JwtService);
    sequelize = app.get<Sequelize>(Sequelize);
    queryInterface = sequelize.getQueryInterface();
    userModel = sequelize.getRepository(User);
    refreshTokenModel = sequelize.getRepository(RefreshToken);
    accessTokenModel = sequelize.getRepository(AccessToken);
    paymentModel = sequelize.getRepository(Payment);
  });

  beforeEach(async () => {
    await queryInterface.bulkDelete('payments', {});
    await queryInterface.bulkDelete('users', {});
    await Promise.all([
      queryInterface.bulkDelete('roles', {}),
      queryInterface.bulkDelete('currencies', {}),
      queryInterface.bulkDelete('paymentStatuses', {}),
      queryInterface.bulkDelete('paymentWays', {}),
    ]);
    await Promise.all([
      queryInterface.bulkInsert('roles', [
        {
          name: Roles.trader,
        },
      ]),
      queryInterface.bulkInsert('currencies', [
        {
          code: 'usdt',
        },
        {
          code: 'btc',
        },
      ]),
      queryInterface.bulkInsert('paymentStatuses', [
        {
          name: 'paid',
        },
        {
          name: 'active',
        },
        {
          name: 'arbitry',
        },
        {
          name: 'completed',
        },
        {
          name: 'canceled',
        },
      ]),
      queryInterface.bulkInsert('paymentWays', [
        {
          name: 'in',
        },
        {
          name: 'out',
        },
        {
          name: 'offer',
        },
      ]),
    ]);

    const configService: ConfigService = app.get<ConfigService>(ConfigService);
    const authConfig: AuthConfig = configService.get<AuthConfig>(
      ConfigNames.auth,
    );

    user = await userModel.create({
      email: 'admin@mail.com',
      role: Roles.trader,
    });
    refreshToken = await refreshTokenModel.create({
      userId: user.id,
    });
    accessToken = await accessTokenModel.create({
      userId: user.id,
      refreshTokenId: refreshToken.id,
    });
    accessTokenValue = await jwtService.signAsync(
      {
        id: accessToken.id,
      },
      {
        expiresIn: authConfig.ttl.access,
      },
    );
  });

  describe('Get payments list (GET /api/payments)', () => {
    const validInput: any = {
      limit: 3,
      offset: 1,
    };

    beforeEach(async () => {
      await queryInterface.bulkInsert('payments', [
        {
          traderId: user.id,
          currency: CryptoCurrencyCodes.usdt,
          status: PaymentStatuses.paid,
          way: PaymentWays.in,
          cryptoAmount: '21.764',
        },
        {
          traderId: user.id,
          currency: CryptoCurrencyCodes.btc,
          status: PaymentStatuses.paid,
          way: PaymentWays.out,
          cryptoAmount: '10',
        },
        {
          traderId: user.id,
          currency: CryptoCurrencyCodes.btc,
          status: PaymentStatuses.active,
          way: PaymentWays.offer,
          cryptoAmount: '23',
          uahAmount: '2154.97',
        },
        {
          traderId: user.id,
          currency: CryptoCurrencyCodes.usdt,
          status: PaymentStatuses.arbitry,
          way: PaymentWays.offer,
          cryptoAmount: '23',
          uahAmount: '2154.97',
        },
        {
          traderId: user.id,
          currency: CryptoCurrencyCodes.btc,
          status: PaymentStatuses.completed,
          way: PaymentWays.offer,
          cryptoAmount: '23',
          uahAmount: '2154.97',
        },
      ]);
    });

    it('Should fail without access token', async () => {
      const { body } = await request(app.getHttpServer())
        .get(
          `/api/payments?limit=${validInput.limit}&offset=${validInput.offset}`,
        )
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.empty],
      });
    });

    it('Should fail with invalid access token', async () => {
      const invalidAccessTokenValue: string = 'qwe';
      const { body } = await request(app.getHttpServer())
        .get(
          `/api/payments?limit=${validInput.limit}&offset=${validInput.offset}`,
        )
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${invalidAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.invalid],
      });
    });

    it('Should fail with expired access token', async () => {
      const expiredAccessTokenValue: string = await jwtService.signAsync(
        {
          id: accessToken.id,
        },
        {
          expiresIn: 0,
        },
      );
      const { body } = await request(app.getHttpServer())
        .get(
          `/api/payments?limit=${validInput.limit}&offset=${validInput.offset}`,
        )
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${expiredAccessTokenValue}`)
        .expect(HttpStatus.UNAUTHORIZED);

      expect(body).toEqual({
        messages: [validationErrorMessages.accessToken.expired],
      });
    });

    it('Should fail for admin', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.admin,
        },
      ]);
      await user.update({
        role: Roles.admin,
      });

      const { body } = await request(app.getHttpServer())
        .get(
          `/api/payments?limit=${validInput.limit}&offset=${validInput.offset}`,
        )
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
    });

    it('Should fail for moderator', async () => {
      await queryInterface.bulkInsert('roles', [
        {
          name: Roles.moderator,
        },
      ]);
      await user.update({
        role: Roles.moderator,
      });

      const { body } = await request(app.getHttpServer())
        .get(
          `/api/payments?limit=${validInput.limit}&offset=${validInput.offset}`,
        )
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.NOT_FOUND);

      expect(body).toEqual({
        messages: [defaultErrorMessages.notFound],
      });
    });

    it('SHould fail with invalid input', async () => {
      const invalidInput: any = {
        limit: 'qwe',
        offset: 'qwe',
        currency: 'qwe',
        status: 'qwe',
        way: 'qwe',
      };
      const { body } = await request(app.getHttpServer())
        .get(
          `/api/payments?limit=${invalidInput.limit}&offset=${invalidInput.offset}&currency=${invalidInput.currency}&status=${invalidInput.status}&way=${invalidInput.way}&dateFrom=${invalidInput.dateFrom}&dateTo=${invalidInput.dateTo}`,
        )
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.BAD_REQUEST);

      expect(body).toEqual({
        messages: [
          validationErrorMessages.limit.int,
          validationErrorMessages.limit.min,
          'validation.MAX',
          validationErrorMessages.offset.int,
          validationErrorMessages.offset.min,
          validationErrorMessages.currency,
          validationErrorMessages.paymentStatus,
          validationErrorMessages.paymentWay,
          validationErrorMessages.dateFrom.format,
          validationErrorMessages.dateTo.format,
        ],
      });
    });

    it('SHould success with valid input', async () => {
      const { body } = await request(app.getHttpServer())
        .get(
          `/api/payments?limit=${validInput.limit}&offset=${validInput.offset}`,
        )
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.OK);

      expect(body.payments?.length).toBe(validInput.limit);
      expect(body.hasMore).toBe(true);
    });

    it('Correct `hasMore`', async () => {
      validInput.offset = 4;

      const { body } = await request(app.getHttpServer())
        .get(
          `/api/payments?limit=${validInput.limit}&offset=${validInput.offset}`,
        )
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${accessTokenValue}`)
        .expect(HttpStatus.OK);

      expect(body.payments?.length).toBe(1);
      expect(body?.hasMore).toBe(false);
    });
  });

  afterAll(async () => {
    await queryInterface.bulkDelete('payments', {});
    await queryInterface.bulkDelete('users', {});
    await Promise.all([
      queryInterface.bulkDelete('roles', {}),
      queryInterface.bulkDelete('currencies', {}),
      queryInterface.bulkDelete('paymentStatuses', {}),
      queryInterface.bulkDelete('paymentWays', {}),
    ]);
    await app.close();
  });
});*/

describe('Wallet (e2e)', walletTests);

describe('Paydesk (e2e)', paydeskTests);
