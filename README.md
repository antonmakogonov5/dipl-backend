# SharkPay API
## Requirements
- Docker 24.0.5 compatible;
- Docker compose 2.20.2 compatible
- NodeJS 18.17.1 compatible (development requirement, don't need in production);

## Steps to start
1. Add `.env` file and fill it by example, provided in `example.env` file.
2. Launch by command <br/>
`$ docker compose  -f "docker.compose.yml" up -d --build api-development` OR<br/>
`$ docker compose  -f "docker.compose.yml" up -d --build api-production` OR<br/>
`$ docker compose  -f "docker.compose.yml" up -d --build api-test-e2e`
3. Generate system wallets <br/>
`npm run console:dev wallet generate` OR<br/>
`npm run console wallet generate`